
package Management;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Lars
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
    Management.Controller.ControllerSuite.class
})
public class ManagementSuite
{

    
}
