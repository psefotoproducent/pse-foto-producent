/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Product.Controller;

import Login.Entity.Photographer;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.LoginService;
import Login.Service.PhotographerService;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import Product.Entity.Product;
import Product.Service.ProductService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Lars
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class ProductManagementControllerTest
{
    private MockMvc mockMvc;
    private Product product;
    private String blob = "http://test.b3-encare.senet.nl/images/logo-header.png";
    private Photographer photographer;
    private PhotoSession photoSession;
    private MockHttpSession session;
    private User user;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private PhotoSessionService photoSessionService;

    @Autowired
    private PhotographerService photographerService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ProductService productService;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() throws MalformedURLException, IOException
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

        this.user = new User("voornaam", "insertion", "lastname", "street", "25", "5555GT", "Eindhoven", "12345678", "username", "unhashed", UserType.EMPLOYEE);
        this.photographer = new Photographer();
        this.photographer.setGroupPrice(1F);
        this.photographer.setPortraitPrice(2F);
        this.photographer.setUser(user);
        this.photographerService.insert(this.photographer);

        this.photoSession = new PhotoSession();

        Date date = new Date(2016, 10, 17);
        this.photoSession.setDate(date);
        this.photoSession.setDescription("Dikke description");
        this.photoSession.setLoginCode("12345");
        this.photoSession.setOrganisation("Fontys Eindhoven Unittest");
        this.photoSession.setOrganisationCity("Eindhoven");
        this.photoSession.setOrganisationHouseNumber("1");
        this.photoSession.setOrganisationStreet("Rachelsmolen");
        this.photoSession.setOrganisationPostalCode("1234BB");
        this.photoSession.setPhotographerId(this.photographer.getPhotographerId());

        this.photoSessionService.insert(this.photoSession);
        this.product = new Product();
        this.product.setDescription(" dikke description ");
        this.product.setDeleted(0);
        this.product.setFilename("C:\\plaatje.jpg");
        this.product.setPrice(2.5F);

        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        InputStream is;
        this.product.setThumbnail(new PhotoData(IOUtils.toByteArray(imageUrl.openStream())));
        this.productService.insert(this.product);

        this.session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());
    }

    @After
    public void tearDown()
    {
        this.productService.remove(this.product.getProductId().intValue());
        this.photographerService.remove(this.photographer);
        this.loginService.remove(this.user);
        this.photoSessionService.remove(this.photoSession.getPhotoSessionId().intValue());
    }

    /**
     * Test of indexAction method, of class ApplicationController.
     */
    @Test
    public void testIndexAction() throws Exception
    {
        this.mockMvc.perform(get("/management/product").accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    /**
     * Test of indexAction method, of class ApplicationController.
     */
    @Test
    public void testEditActionGet() throws Exception
    {
        Long productId = this.product.getProductId();
        this.mockMvc.perform(get("/management/product/edit/" + productId).accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testEditActionGetFail() throws Exception
    {
        this.mockMvc.perform(get("/management/product/edit/NaN").accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testEditActionPostSuccess() throws Exception
    {
        Long productId = this.product.getProductId();

        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        MockMultipartFile firstFile = new MockMultipartFile("data.png", "filename.png", "image/png", imageUrl.openStream());

        this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/management/product/edit/" + productId)
                .file(firstFile)
                .accept("text/html")
                .session(this.session)
                .param("description", "Leipe beschrijving")
                .param("filename", "/een/leipe/pad/naam/super-leuk.jpeg")
                .param("price", "2.5"))
                .andExpect(status().isFound());
    }

    @Test
    public void testEditActionPostFail() throws Exception
    {
        Long productId = this.product.getProductId();

        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        MockMultipartFile firstFile = new MockMultipartFile("data.png", "filename.png", "image/png", imageUrl.openStream());

        this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/management/product/edit/NaN")
                .file(firstFile)
                .accept("text/html")
                .session(this.session)
                .param("description", "Leipe beschrijving")
                .param("filename", "/een/leipe/pad/naam/super-leuk.jpeg")
                .param("price", "aaa"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddActionGetFail() throws Exception
    {
        this.mockMvc.perform(get("/management/product/add").accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddActionPostSuccess() throws Exception
    {
        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        MockMultipartFile firstFile = new MockMultipartFile("data.png", "filename.png", "image/png", imageUrl.openStream());

        this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/management/product/add/")
                .file(firstFile)
                .accept("text/html")
                .session(this.session)
                .param("description", "Leipe beschrijving")
                .param("filename", "/een/leipe/pad/naam/super-leuk.jpeg")
                .param("price", "2.5"))
                .andExpect(status().isFound());
    }

    @Test
    public void testAddActionPostFail() throws Exception
    {
        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        MockMultipartFile firstFile = new MockMultipartFile("data.png", "filename.png", "image/png", imageUrl.openStream());

        this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/management/product/add")
                .file(firstFile)
                .accept("text/html")
                .session(this.session)
                .param("description", "Leipe beschrijving")
                .param("filename", "/een/leipe/pad/naam/super-leuk.jpeg")
                .param("price", "aaa"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteActionPostSuccess() throws Exception
    {
        Long productId = this.product.getProductId();
        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/product")
                .accept("text/html")
                .session(this.session)
                .param("productId", productId.toString()))
                .andExpect(status().isFound());
    }

    @Test
    public void testDeleteActionPostFail() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/product")
                .session(this.session)
                .accept("text/html")
                .param("productId", "NaN"))
                .andExpect(status().isOk());
    }

    @Test
    public void testgetProductPicture() throws Exception
    {
        Long productId = this.product.getProductId();
        
        this.mockMvc.perform(get("/management/product/getPicture/" + productId.toString())
                .session(this.session)
                .accept("text/html"))
                .andExpect(status().isOk());
    }

}
