/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Product.Entity;

import Photo.Entity.PhotoData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class ProductTest
{
    /**
     * Test of getProductId method, of class Product.
     */
    @Test
    public void testGetProductId()
    {
        System.out.println("getProductId");
        Product instance = new Product();
        Long expResult = null;
        Long result = instance.getProductId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setProductId method, of class Product.
     */
    @Test
    public void testSetProductId()
    {
        System.out.println("setProductId");
        Long productId = null;
        Product instance = new Product();
        instance.setProductId(productId);
    }

    /**
     * Test of getFilename method, of class Product.
     */
    @Test
    public void testGetFilename()
    {
        System.out.println("getFilename");
        Product instance = new Product();
        String expResult = null;
        String result = instance.getFilename();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFilename method, of class Product.
     */
    @Test
    public void testSetFilename()
    {
        System.out.println("setFilename");
        String filename = "";
        Product instance = new Product();
        instance.setFilename(filename);
    }

    /**
     * Test of getPrice method, of class Product.
     */
    @Test
    public void testGetPrice()
    {
        System.out.println("getPrice");
        Product instance = new Product();
        Float expResult = null;
        Float result = instance.getPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPrice method, of class Product.
     */
    @Test
    public void testSetPrice()
    {
        System.out.println("setPrice");
        Float price = null;
        Product instance = new Product();
        instance.setPrice(price);
    }

    /**
     * Test of getDescription method, of class Product.
     */
    @Test
    public void testGetDescription()
    {
        System.out.println("getDescription");
        Product instance = new Product();
        String expResult = null;
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescription method, of class Product.
     */
    @Test
    public void testSetDescription()
    {
        System.out.println("setDescription");
        String description = "";
        Product instance = new Product();
        instance.setDescription(description);
    }

    /**
     * Test of getDeleted method, of class Product.
     */
    @Test
    public void testGetDeleted()
    {
        System.out.println("getDeleted");
        Product instance = new Product();
        int expResult = 0;
        int result = instance.getDeleted();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDeleted method, of class Product.
     */
    @Test
    public void testSetDeleted()
    {
        System.out.println("setDeleted");
        int deleted = 0;
        Product instance = new Product();
        instance.setDeleted(deleted);
    }

    /**
     * Test of getThumbnail method, of class Product.
     */
    @Test
    public void testGetThumbnail()
    {
        System.out.println("getThumbnail");
        Product instance = new Product();
        PhotoData expResult = null;
        PhotoData result = instance.getThumbnail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setThumbnail method, of class Product.
     */
    @Test
    public void testSetThumbnail()
    {
        System.out.println("setThumbnail");
        PhotoData thumbnail = null;
        Product instance = new Product();
        instance.setThumbnail(thumbnail);
    }

    /**
     * Test of delete method, of class Product.
     */
    @Test
    public void testDelete()
    {
        System.out.println("delete");
        Product instance = new Product();
        instance.delete();
    }

    /**
     * Test of getPrintPrice method, of class Product.
     */
    @Test
    public void testGetPrintPrice()
    {
        System.out.println("getPrintPrice");
        Product instance = new Product();
        String expResult = "nu";
        String result = instance.getPrintPrice();
        assertEquals(expResult, result);
    }
    
}
