package Login.Controller;

import Login.Entity.Employee;
import Login.Entity.Photographer;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.EmployeeService;
import Login.Service.LoginService;
import Login.Service.PhotographerService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoDataService;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class LoginControllerTest
{

    private MockMvc mockMvc;
    private Employee employee;
    private Employee employee2;
    private Employee employee3;
    private User user;
    private User user2;
    private User user3;
    private User user4;
    private PhotoSession photoSession;
    private Photo photo;
    private Photographer photographer;
    private PhotoData photoData;
    private PhotoType photoType;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private LoginService loginService;
    
    @Autowired
    private PhotoSessionService photoSessionService;
    
    @Autowired
    private PhotographerService photographerService;

    @Autowired
    private PhotoService photoService;
    
    @Autowired
    private PhotoDataService photoDataService;

    public LoginControllerTest()
    {
    }

    @Before
    public void setUp()
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        Date date = new Date(2016, 10, 17);     

        this.user = new User("aa", "aa", "aa", "aa", "aa", "aa", "aa", "aa", "test", "test", UserType.EMPLOYEE);
        this.user2 = new User("bb", "bb", "bb", "bb", "bb", "bb", "bb", "bb", "test2", "test2", UserType.PHOTOGRAPHER);
        this.user3 = new User("cc", "cc", "cc", "cc", "cc", "cc", "cc", "cc", "test3", "test3", UserType.CLIENT);
        this.user4 = new User("dd", "dd", "dd", "dd", "dd", "dd", "dd", "dd", "test4", "test4", UserType.PHOTOGRAPHER);

        this.employee = new Employee();
        this.employee2 = new Employee();
        this.employee3 = new Employee();
        
        this.photographer = new Photographer();
        this.photographer.setGroupPrice(1F);
        this.photographer.setPortraitPrice(2F);
        this.photographer.setUser(this.user4);
        this.photographerService.insert(this.photographer);

        this.employee.setUser(this.user);
        this.employee2.setUser(this.user2);
        this.employee3.setUser(this.user3);
        
        this.employeeService.insert(this.employee);
        this.employeeService.insert(this.employee2);
        this.employeeService.insert(this.employee3);
        
        this.loginService.insert(this.user);
        this.loginService.insert(this.user2);
        this.loginService.insert(this.user3);
        
        this.photoSession = new PhotoSession(this.photographer.getPhotographerId(), "Yo", date, "Yo" , "12345", "Yo", "Yo", "Yo");
        this.photoSessionService.insert(this.photoSession);
        
        this.photoData = new PhotoData(new byte[]{});
        this.photoType = PhotoType.GROUP;
        this.photo = new Photo(this.photoSession, "c:\\dikke\\file\\name.png", 5F, this.photoType, this.photoData);
        this.photo.setActive(true);
        this.photoService.insert(this.photo);
    }

    @After
    public void tearDown()
    {
        this.employeeService.remove(this.employee);
        this.employeeService.remove(this.employee2);
        this.employeeService.remove(this.employee3);
        this.photographerService.remove(this.photographer);
        
        this.loginService.remove(this.user);
        this.loginService.remove(this.user2);
        this.loginService.remove(this.user3);
        this.loginService.remove(this.user4);
      
        this.photoSessionService.remove(this.photoSession.getPhotoSessionId().intValue());
    }

    /**
     * This test will do the following: See if url: / will return on a GET Request
     *
     * Expected result: login/home
     *
     * @throws Exception : If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void testLoginGETAction() throws Exception
    {
        this.mockMvc.perform(get("/")
                .accept("text/html"))
                .andExpect(status().isOk())
                .andExpect(view().name("login/home"));
    }

    /**
     * This function will test if you can sucesfully login.
     *
     * If username and password exist in database than the user will login. This case will return true and view().name would be /login/success if this is not the case it will return false.
     *
     * @throws Exception : If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void loginSubmitEmployee() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/login")
                .accept("text/html")
                .param("username", this.user.getUsername())
                .param("password", "test"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/home"));
    }

    /**
     * This function will test if you can sucesfully login.
     *
     * If username and password exist in database than the user will login. This case will return true and view().name would be /login/success if this is not the case it will return false.
     *
     * @throws Exception : If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void loginSubmitPhotograper() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/login")
                .accept("text/html")
                .param("username", this.user3.getUsername())
                .param("password", "test3"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/home"));
    }
    /**
     * This function will test if you can sucesfully login.
     *
     * If username and password exist in database than the user will login. This case will return true and view().name would be /login/success if this is not the case it will return false.
     *
     * @throws Exception : If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void loginSubmitClient() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/login")
                .accept("text/html")
                .param("username", this.user2.getUsername())
                .param("password", "test2"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/home"));
    }
    
    
    @Test
    public void loginCodeSubmit() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/loginClient")
                .accept("text/html")
                .param("loginCode", "1117113916"))  
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:order/overview/1"));
    }


    @Test
    public void getPictureGET() throws Exception
    {
        this.mockMvc.perform(get("order/getPicture/1")
                .param("photoId", this.photo.getPhotoId().toString()));
    }

    @Test
    public void getPicture() throws Exception
    {
        Photo result = this.photoService.get(this.photo.getPhotoId().intValue());
        PhotoSession ps = new PhotoSession();
        PhotoType tp = PhotoType.GROUP;
        PhotoData pd = new PhotoData();
        Photo expResult = new Photo(ps, "c:\\dikke\\file\\name.png", new Float(5), tp, pd);
        assertEquals(expResult.getFilename(), result.getFilename());
    }

    @Test
    public void logoutGET() throws Exception
    {
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user);

        this.mockMvc.perform(get("/logout")
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/"));
    }

}
