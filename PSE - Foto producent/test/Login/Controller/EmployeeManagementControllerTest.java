package Login.Controller;

import Login.Entity.Employee;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.EmployeeService;
import Login.Service.LoginService;
import Photo.Service.PhotoService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class EmployeeManagementControllerTest
{

    private MockMvc mockMvc;
    private Employee employee;
    private Employee employee2;
    private Employee employee3;
    private User user;
    private User user2;
    private User user3;
    private final List employeeList;
    private int nrOfElements;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private PhotoService photoService;

    public EmployeeManagementControllerTest()
    {
        this.employeeList = new ArrayList();
        employeeList.add(this.employee);
        employeeList.add(this.employee2);
        employeeList.add(this.employee3);
        this.nrOfElements = employeeList.size();
    }

    @Before
    public void setUp()
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

        this.user = new User("aa", "aa", "aa", "aa", "aa", "aa", "aa", "aa", "test", "test", UserType.EMPLOYEE);
        this.user2 = new User("bb", "bb", "bb", "bb", "bb", "bb", "bb", "bb", "test2", "test2", UserType.EMPLOYEE);
        this.user3 = new User("cc", "cc", "cc", "cc", "cc", "cc", "cc", "cc", "test3", "test3", UserType.EMPLOYEE);

        this.employee = new Employee();
        this.employee2 = new Employee();
        this.employee3 = new Employee();

        this.employee.setUser(this.user);
        this.employee2.setUser(this.user2);
        this.employee3.setUser(this.user3);

        this.employeeService.insert(this.employee);
        this.employeeService.insert(this.employee2);
        this.employeeService.insert(this.employee3);

        this.loginService.insert(this.user);
        this.loginService.insert(this.user2);
        this.loginService.insert(this.user3);

    }

    @After
    public void tearDown()
    {
        this.employeeService.remove(this.employee);
        this.employeeService.remove(this.employee2);
        this.employeeService.remove(this.employee3);

        this.loginService.remove(this.user);
        this.loginService.remove(this.user2);
        this.loginService.remove(this.user3);
    }

    /**
     * This test will do the following: - See if url: /employee/management returns view employee/list on a GET Request
     *
     * Expected result: employee/list
     *
     * @throws Exception : If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void testEmployeeManagementGET() throws Exception
    {
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user);

        this.mockMvc.perform(get("/employee/management")
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/list"));
    }

    /**
     * This test will see if a list of employees is returned.
     *
     * Check: - Check if returned value is of Type List - If List is empty assertTrue with message: "Employees are returned but list is empty." - If List is not empty assertTrue with message: "Employees are returned and nr of Employees is: @count ." - Else if returned value is not off type list return false with message: "Returned type is not a list, this cannot be possible."
     */
    @Test
    public void testEmployeeManagementAction()
    {
        if (this.employeeList instanceof List)
        {
            if (this.nrOfElements > 0)
            {
                assertTrue(true);
                System.out.println("testEmployeeManagementAction :: Employees are returned and nr of Employees is: " + this.nrOfElements);
            }
            else if (this.nrOfElements == 0)
            {
                assertTrue(true);
                System.out.println("testEmployeeManagementAction :: Employees are returned but list is empty.");
            }
        }
        else
        {
            assertFalse(true);
            System.out.println("testEmployeeManagementAction :: Returned type is not a list, this cannot be possible.");
        }
    }

    /**
     * This test will do the following: - Get last employee ID in database - See if url: /management/employee/edit/{last_employee_id} returns view employee/edit on a GET Request
     *
     * Expected result: employee/edit
     *
     * @throws Exception : If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void testEmployeeManagementEditActionGET() throws Exception
    {
        long lastEmployeeID = employee3.getEmployeeId();
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user3);

        this.mockMvc.perform(get("/management/employee/edit/" + lastEmployeeID)
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/edit"));
    }

    /**
     * This test will see if a single Employee is selected for editing
     *
     * Check: - Check if returned value is of Type Employee - If Employee is not empty and object of type Employee assertTrue with message: "A single Employee for edit actions is selected." - If Employee is empty and object of type Employee assertFalse with message: "A single Employee for edit actions is null on given ID." - Else if not one of the above give assertFalse: "Selected object is not off type Employee." - Else if NumberFormatException than assertTrue (expected) with message: "A new Employee is made as expected."
     */
    @Test
    public void testEmployeeManagementEditAction() throws NumberFormatException
    {
        try
        {
            Employee e = this.employeeService.get(Integer.parseInt(employee.getEmployeeId().toString()));

            if (e != null && e instanceof Employee)
            {
                assertTrue(true);
                System.out.println("testEmployeeManagementEditAction :: A single Employee for edit actions is selected.");
            }
            else if (e == null && e instanceof Employee)
            {
                assertFalse(true);
                System.out.println("testEmployeeManagementEditAction :: A single Employee for edit actions is null on given ID.");
            }
            else
            {
                assertFalse(true);
                System.out.println("testEmployeeManagementEditAction :: Selected object is not off type Employee.");
            }
        }
        catch (NumberFormatException nfe)
        {
            Employee e = new Employee();
            if (e instanceof Employee)
            {
                assertTrue(true);
                System.out.println("testEmployeeManagementEditAction :: A new Employee is made as expected.");
            }
        }
    }

    /**
     * This is a post request on /management/employee/edit/{employeeId} Simple check if an user got updated from the database on a post Request.
     *
     * returns true if this is the case, returns an exception and / or false if this is not the case.
     *
     * @throws Exception If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void testEmployeeManagementEditUpdatePOSTAction() throws Exception
    {
        long lastEmployeeID = employee3.getEmployeeId();
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user3);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/employee/edit/" + lastEmployeeID)
                .accept("text/html")
                .param("username", "uniekeUserNameAndersDoetDieNiks")
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .sessionAttrs(sessionAttrs))
                .andExpect(view().name("redirect:/employee/management"));
    }

    /**
     * This test will show if there is an employee selected it will update its record values by the given new input values. Else if there is no employee with the given EmployeeID it will make a new employee and set the values of this employee by the given input values. This will however fail because there won't be an user attached to the employee and result in a NullPointer Exception.
     *
     * If the price given in the input is not a number than the employee is not saved and record is not updated.
     *
     * Last thing we check if given input matches data in database
     *
     * @throws NumberFormatException
     */
    @Test
    public void testEmployeeManagementEditUpdateAction() throws NumberFormatException
    {
        Employee updatedEmployee;
        try
        {
            updatedEmployee = this.employeeService.get(Integer.parseInt(this.employee.getEmployeeId().toString()));
        }
        catch (NumberFormatException nfe)
        {
            updatedEmployee = new Employee();
            assertFalse(true);
            System.out.println("testEmployeeManagementEditUpdateAction :: "
                    + "No employee is found, the new made employee won't have an user attached "
                    + "so nullpointer here.");
        }

        try
        {
            String username = "username123";
            String password = "password";
            String firstname = "firstname";
            String insertion = "insertion";
            String surname = "surname";
            String housenumber = "housenumber";
            String postalcode = "postalcode";
            String city = "city";
            String phonenumber = "phonenumber";

            this.user = updatedEmployee.getUser();
            this.user.setUsername(username);
            this.user.setPassword(password);
            this.user.setFirstName(firstname);
            this.user.setInsertion(insertion);
            this.user.setSurName(surname);
            this.user.setHouseNumber(housenumber);
            this.user.setPostalCode(postalcode);
            this.user.setCity(city);
            this.user.setPhoneNumber(phonenumber);

            updatedEmployee.setUser(user);
            this.employeeService.update(updatedEmployee);

            if (updatedEmployee.getUser().getUsername().equals(username)
                    && updatedEmployee.getUser().getPassword().equals(password)
                    && updatedEmployee.getUser().getFirstName().equals(firstname)
                    && updatedEmployee.getUser().getInsertion().equals(insertion)
                    && updatedEmployee.getUser().getSurName().equals(surname)
                    && updatedEmployee.getUser().getHouseNumber().equals(housenumber)
                    && updatedEmployee.getUser().getPostalCode().equals(postalcode)
                    && updatedEmployee.getUser().getCity().equals(city)
                    && updatedEmployee.getUser().getPhoneNumber().equals(phonenumber))
            {
                assertTrue(true);
                System.out.println("testEmployeeManagementEditUpdateAction :: New values are set and do match.");
            }

        }
        catch (NumberFormatException nfe)
        {
            assertFalse(true);
            System.out.println("testEmployeeManagementEditUpdateAction :: Price needs to be a number.");
        }
    }

    /**
     * Check if GET Request for method employeeManagementaddGETAction is working as expected Do a get request on /management/employee/add if this executes well it will return the view: employee/edit
     *
     * If description above is succesfull than return true else return false.
     *
     * @throws Exception If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void employeeManagementaddGETAction() throws Exception
    {
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user);
        this.mockMvc.perform(get("/management/employee/add")
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/edit"));
    }

    /**
     * Check if POST Request for method employeeManagementaddPOSTAction is working as expected Do a post request on /management/employee/add if this executes well it will return a redirect to the view: /employee/management
     *
     * For not getting duplicate results the current time wil be added to the username.
     *
     * If description above is succesfull than return true else return false.
     *
     * @throws Exception If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void employeeManagementAddPOSTAction() throws Exception
    {

        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String current_time = sdf.format(cal.getTime());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/employee/add")
                .accept("text/html")
                .param("username", "test" + current_time)
                .param("password", "test")
                .param("firstname", "test")
                .param("insertion", "test")
                .param("surname", "test")
                .param("housenumber", "1234")
                .param("postalcode", "1234")
                .param("city", "1234")
                .param("phonenumber", "1234")
                .sessionAttrs(sessionAttrs))
                .andExpect(view().name("redirect:/employee/management"));
    }

    /**
     * Check if POST Request for method employeeManagementDeletePOSTAction is working as expected Do a post request on /employee/management if this executes well it will return a redirect to the view: /employee/management
     *
     * For testing purposes we will delete employee3 that is made within the controller of this class.
     *
     * If description above is succesfull than return true else return false.
     *
     * @throws Exception If anything goes wrong with the tested method throw exception on console.
     */
    @Test
    public void employeeManagementDeletePOSTAction() throws Exception
    {
        String lastEmployeeID = employee3.getEmployeeId().toString();
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user3);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/employee/management")
                .accept("text/html")
                .param("employeeId", lastEmployeeID)
                .sessionAttrs(sessionAttrs))
                .andExpect(view().name("redirect:/employee/management"));
    }
    
       @Test
    public void testEmployeeSingleGet() throws Exception
    {
        long lastEmployeeID = employee3.getEmployeeId();
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user3);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/employee/singlee/" + lastEmployeeID)
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(view().name("employee/singlee"));
    }


    @Test
    public void testEmployeeEditActionSingleGET() throws Exception
    {
        long lastEmployeeID = employee3.getEmployeeId();
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user3);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/employee/single/edit/" + lastEmployeeID)
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(view().name("employee/edit"));
    }

    @Test
    public void testEmployeeEditActionSinglePOST() throws Exception
    {
        long lastEmployeeID = employee3.getEmployeeId();
        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("loggedInUser", this.user3);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/employee/single/edit/" + lastEmployeeID)
                .accept("text/html")
                .param("username", "uniekeUserNameAndersDoetDieNiks")
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .sessionAttrs(sessionAttrs))
                .andExpect(view().name("redirect:/employee/singlee/{employeeId}"));
    }

}
