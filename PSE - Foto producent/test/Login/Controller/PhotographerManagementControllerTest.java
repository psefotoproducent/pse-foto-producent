
package Login.Controller;

import Login.Entity.Employee;
import Login.Entity.Photographer;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.EmployeeService;
import Login.Service.LoginService;
import Login.Service.PhotographerService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "/servlet.xml" ,"/dispatcher-servlet.xml"})
public class PhotographerManagementControllerTest
{
    private MockMvc mockMvc;
    private Employee employee;
    private Employee employee2;
    private Employee employee3;
    private User user;
    private User user2;
    private User user3;
    private Photographer photographer;
    private Photographer photographer2;
    private Photographer photographer3;
    
    @Autowired
    private WebApplicationContext wac;
    
    @Autowired
    private EmployeeService employeeService;
    
    @Autowired
    private LoginService loginService;
   
    @Autowired
    private PhotographerService photographerService;
   
    public PhotographerManagementControllerTest(){}

    @Before
    public void setUp()
    {
       this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
       
       this.user = new User("aa", "aa", "aa", "aa", "aa","aa", "aa", "aa", "test", "test", UserType.EMPLOYEE);
       this.user2 = new User("bb", "bb", "bb", "bb", "bb","bb", "bb", "bb", "test2", "test2", UserType.EMPLOYEE);
       this.user3 = new User("cc", "cc", "cc", "cc", "cc","cc", "cc", "cc", "test3", "test3", UserType.PHOTOGRAPHER);
       
       this.photographer = new Photographer();
       this.photographer.setGroupPrice(new Float(1.00));
       this.photographer.setPortraitPrice(new Float(2.00));
       this.photographer.setUser(this.user);
       this.photographerService.insert(this.photographer);
       
       this.photographer2 = new Photographer();
       this.photographer2.setGroupPrice(new Float(1.00));
       this.photographer2.setPortraitPrice(new Float(2.00));
       this.photographer2.setUser(this.user2);
       this.photographerService.insert(this.photographer2);
       
       this.photographer3 = new Photographer();
       this.photographer3.setGroupPrice(new Float(1.00));
       this.photographer3.setPortraitPrice(new Float(2.00));
       this.photographer3.setUser(this.user3);
       this.photographerService.insert(this.photographer3);
       
       this.loginService.insert(this.user);
       this.loginService.insert(this.user2);
       this.loginService.insert(this.user3);
    }
    
    @After
    public void tearDown()
    {
       this.photographerService.remove(this.photographer);
       this.photographerService.remove(this.photographer2);
       this.photographerService.remove(this.photographer3); 

       this.loginService.remove(this.user);
       this.loginService.remove(this.user2);
       this.loginService.remove(this.user3);
    }
    
    @Test 
    public void photographerManagementGETAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(get("/photographer/management")
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("photographer/list"));
    }
    
    @Test 
    public void photographerManagementEditGETAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(get("/management/photographer/edit/1")
                .accept("text/html")
                .param("photographerId", this.user.getUserId().toString())
                .param("userId", this.user.getUserId().toString())
                .param("groupPrice", "2.50")
                .param("portraitPrice", "2.50")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("photographer/edit"));
    }
    
    @Test 
    public void photographerManagementEditFAILGETAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(get("/management/photographer/edit/1")
                .accept("text/html")
                .param("photographerId", "abc")
                .param("userId", this.user.getUserId().toString())
                .param("groupPrice", "2.50")
                .param("portraitPrice", "2.50")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("photographer/edit"));
    }
    
    @Test
    public void photographerManagementEditActionGETSingle() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user3); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.get("/management/photographerSingle/edit/" + this.photographer3.getPhotographerId().toString())
                .accept("text/html")
                .param("photographerId", this.photographer3.getPhotographerId().toString())
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("photographer/edit"));
    }
    
    @Test 
    public void photographerManagementEditActionPOSTSingle() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user3); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photographerSingle/edit/" + this.photographer3.getPhotographerId().toString())
                .accept("text/html")
                .param("photographerId", this.photographer3.getPhotographerId().toString())
                .param("username", "uniekeUserNameAndersDoetDieNiks")
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .param("groupPrice", "2.50")
                .param("portraitPrice", "2.50")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/photographer/singlep/{photographerId}"));
    }
    
    @Test 
    public void photographerManagementEditActionFAILPOSTSingle() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user3); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photographerSingle/edit/" + this.photographer3.getPhotographerId().toString())
                .accept("text/html")
                .param("photographerId", "abc")
                .param("username", "uniekeUserNameAndersDoetDieNiks")
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .param("groupPrice", "2.50")
                .param("portraitPrice", "2.50")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/photographer/singlep/{photographerId}"));
    }
    
    @Test 
    public void photographerManagementEditPOSTAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photographer/edit/" + this.photographer.getPhotographerId().toString())
                .accept("text/html")
                .param("photographerId", this.photographer.getPhotographerId().toString())
                .param("username", "uniekeUserNameAndersDoetDieNiks")
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .param("groupPrice", "2.50")
                .param("portraitPrice", "2.50")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/photographer/management"));
    }
    
     @Test 
    public void photographerManagementEditFAILPOSTAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photographer/edit/" + this.photographer.getPhotographerId().toString())
                .accept("text/html")
                .param("photographerId", "abc")
                .param("username", "uniekeUserNameAndersDoetDieNiks")
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .param("groupPrice", "2.50")
                .param("portraitPrice", "2.50")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/photographer/management"));
    }
    
    @Test 
    public void photographerManagementaddGETAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(get("/management/photographer/add")
                .accept("text/html")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("photographer/edit"));
    }
    
    @Test 
    public void photographerManagementaddPOSTAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
       
       Calendar cal = Calendar.getInstance();
       SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
       String current_time = sdf.format(cal.getTime());
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photographer/add")
                .accept("text/html")
                .param("username", "uniekeUser" + current_time)
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .param("groupPrice", "2.50")
                .param("portraitPrice", "2.50")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/photographer/management"));
    }
    
    @Test 
    public void photographerManagementaddFAILPOSTAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
       
       Calendar cal = Calendar.getInstance();
       SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
       String current_time = sdf.format(cal.getTime());
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photographer/add")
                .accept("text/html")
                .param("username", "uniekeUser" + current_time)
                .param("password", "pass")
                .param("firstname", "first")
                .param("insertion", "insert")
                .param("surname", "last")
                .param("housenumber", "012345353")
                .param("postalcode", "3434")
                .param("city", "EINDHOVEN")
                .param("phonenumber", "232424")
                .param("groupPrice", "abc")
                .param("portraitPrice", "abc")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isBadRequest());
    }
    
    @Test 
    public void photographerManagementDeletePOSTAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/photographer/management")
                .accept("text/html")
                .param("photographerId", this.photographer.getPhotographerId().toString())
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/photographer/management"));
    }
    
    @Test(expected=org.springframework.web.util.NestedServletException.class) 
    public void photographerManagementDeleteFAILPOSTAction() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.post("/photographer/management")
                .accept("text/html")
                .param("photographerId", "34243242334")
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isFound())
                .andExpect(view().name("photographer/list"));
    }
    
    @Test
    public void photographerSingleGET() throws Exception
    {
       Map<String, Object> sessionAttrs = new HashMap<>();
       sessionAttrs.put("loggedInUser", this.user3); 
        
       this.mockMvc.perform(MockMvcRequestBuilders.get("/photographer/singlep/" + this.photographer3.getPhotographerId().toString())
                .accept("text/html")
                .param("photographerId", this.photographer3.getPhotographerId().toString())
                .sessionAttrs(sessionAttrs))
                .andExpect(status().isOk())
                .andExpect(view().name("photographer/singlep"));
    }
    
    
    
}
