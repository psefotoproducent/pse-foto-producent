/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Entity;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wesley
 */
public class RightValidatorTest {

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }
    /**
     * Test of validate method, of class RightValidator.
     */
    @Test
    public void testValidate_HttpSession_UserType()
    {
        System.out.println("validate");
        HttpSession session = null;
        UserType userType = null;
        boolean result = RightValidator.validate(session, userType);
        assertFalse(result);
    }

    /**
     * Test of validate method, of class RightValidator.
     */
    @Test
    public void testValidate_HttpSession_List()
    {
        System.out.println("validate");
        HttpSession session = null;
        List<UserType> userTypes = null;
        boolean result = RightValidator.validate(session, userTypes);
        assertFalse(result);
    }
    
}
