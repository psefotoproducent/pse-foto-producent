/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Internationalization.Entity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class InternationalizationTest
{
    
    public InternationalizationTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getInternationalizationId method, of class Internationalization.
     */
    @Test
    public void testGetInternationalizationId()
    {
        System.out.println("getInternationalizationId");
        Internationalization instance = new Internationalization();
        Long expResult = null;
        Long result = instance.getInternationalizationId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSource method, of class Internationalization.
     */
    @Test
    public void testGetSource()
    {
        System.out.println("getSource");
        Internationalization instance = new Internationalization("source", "translation", "language");
        String expResult = "source";
        String result = instance.getSource();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTranslation method, of class Internationalization.
     */
    @Test
    public void testGetTranslation()
    {
        System.out.println("getTranslation");
        Internationalization instance = new Internationalization("source", "translation", "language");
        String expResult = "translation";
        String result = instance.getTranslation();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLanguage method, of class Internationalization.
     */
    @Test
    public void testGetLanguage()
    {
        System.out.println("getLanguage");

        Internationalization instance = new Internationalization("source", "translation", "language");
        String expResult = "language";
        String result = instance.getLanguage();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTranslation method, of class Internationalization.
     */
    @Test
    public void testSetTranslation()
    {
        System.out.println("setTranslation");
        String translation = "";
        Internationalization instance = new Internationalization();
        instance.setTranslation(translation);
        
    }

    /**
     * Test of toString method, of class Internationalization.
     */
    @Test
    public void testToString()
    {
        System.out.println("toString");        
        Internationalization instance = new Internationalization("source", "translation", "language");
        String expResult = "source => translation language";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
