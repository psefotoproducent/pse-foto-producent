/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Internationalization.Entity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class SaveTranslationTest
{

    /**
     * Test of getId method, of class SaveTranslation.
     */
    @Test
    public void testGetId()
    {
        System.out.println("getId");
        SaveTranslation instance = new SaveTranslation(1, "translation");
        int expResult = 1;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTranslation method, of class SaveTranslation.
     */
    @Test
    public void testGetTranslation()
    {
        System.out.println("getTranslation");
        SaveTranslation instance = new SaveTranslation(1, "translation");
        String expResult = "translation";
        String result = instance.getTranslation();
        assertEquals(expResult, result);
    }
    
}
