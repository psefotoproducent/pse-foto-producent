package Internationalization.controller;

import Internationalization.Entity.SaveTranslation;
import Login.Entity.Employee;
import Login.Entity.User;
import Login.Entity.UserType;
import java.io.IOException;
import java.net.MalformedURLException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Lars
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class InternationalizationManagementControllerTest
{
    private MockMvc mockMvc;
    private MockHttpSession session;
    private User user;
    private Employee employee;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() throws MalformedURLException, IOException
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        
        this.user = new User("aa", "aa", "aa", "aa", "aa", "aa", "aa", "aa", "test", "test", UserType.EMPLOYEE);

        this.employee = new Employee();

        this.employee.setUser(this.user);
        
        this.session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.employee);
        session.setAttribute("loggedInUser", this.employee.getUser());
    }
    
    @After
    public void tearDown()
    {
    }

    @Test
    public void testListAction() throws Exception
    {
        String lang = "id";

        this.mockMvc.perform(MockMvcRequestBuilders.get("/i18n-management/"+lang)
//                    .contentType(MediaType.APPLICATION_JSON)
    //                .content(content)
                    .accept("application/json")
                    .session(this.session)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testSaveAction() throws Exception
    {
        SaveTranslation st = new SaveTranslation(5, "Selamat pagi");
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        byte[] content = mapper.writeValueAsBytes(st);
        
        this.mockMvc.perform(MockMvcRequestBuilders.post("/i18n-management/save")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content)
                    .accept("application/json")
                    .session(this.session)
                )
                .andExpect(status().isOk());
    }
}
