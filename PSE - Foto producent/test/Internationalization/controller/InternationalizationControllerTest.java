package Internationalization.controller;

import Login.Entity.Photographer;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.LoginService;
import Login.Service.PhotographerService;
import Order.Entity.Invoice;
import Order.Entity.InvoiceLine;
import Order.Entity.ShoppingCartLine;
import Order.Service.InvoiceLineService;
import Order.Service.InvoiceService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import Product.Entity.Product;
import Product.Service.ProductService;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Lars
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class InternationalizationControllerTest
{
    private MockMvc mockMvc;
    private MockHttpSession session;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() throws MalformedURLException, IOException
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @After
    public void tearDown()
    {
    }

    @Test
    public void testSetLangAction() throws Exception
    {
        String lang = "id";

        this.mockMvc.perform(MockMvcRequestBuilders.post("/i18n/set/"+lang)
                    .contentType(MediaType.APPLICATION_JSON)
    //                .content(content)
                    .accept("application/json")
//                .session(this.session)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetLangAction() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/i18n/get/")
                    .contentType(MediaType.APPLICATION_JSON)
    //                .content(content)
                    .accept("application/json")
//                    .session(this.session)
                )
                .andExpect(status().isOk());
    }
}
