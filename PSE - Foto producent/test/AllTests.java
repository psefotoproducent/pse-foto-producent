import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Lars
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        Application.Controller.ApplicationControllerTest.class,
        Login.Controller.EmployeeManagementControllerTest.class,
        Login.Controller.LoginControllerTest.class,
        Login.Controller.PhotographerManagementControllerTest.class,
        Order.Controller.OrderControllerTest.class,
        Photo.Controller.PhotoControllerTest.class,
        Photo.Controller.PhotoSearchControllerTest.class,
        Photo.Controller.PhotoSessionControllerTest.class,
        Product.Controller.ProductControllerTest.class,
        Product.Controller.ProductManagementControllerTest.class,
})
public class AllTests
{

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

}
