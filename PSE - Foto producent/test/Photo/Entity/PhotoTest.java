/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Photo.Entity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class PhotoTest
{
    

    /**
     * Test of getPhotoId method, of class Photo.
     */
    @Test
    public void testGetPhotoId()
    {
        System.out.println("getPhotoId");
        Photo instance = new Photo();
        Long expResult = null;
        Long result = instance.getPhotoId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPhotoSession method, of class Photo.
     */
    @Test
    public void testGetPhotoSession()
    {
        System.out.println("getPhotoSession");
        Photo instance = new Photo();
        PhotoSession expResult = null;
        PhotoSession result = instance.getPhotoSession();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFilename method, of class Photo.
     */
    @Test
    public void testGetFilename()
    {
        System.out.println("getFilename");
        Photo instance = new Photo();
        String expResult = null;
        String result = instance.getFilename();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFilename method, of class Photo.
     */
    @Test
    public void testSetFilename()
    {
        System.out.println("setFilename");
        String filename = "";
        Photo instance = new Photo();
        instance.setFilename(filename);
    }

    /**
     * Test of getPrice method, of class Photo.
     */
    @Test
    public void testGetPrice()
    {
        System.out.println("getPrice");
        Photo instance = new Photo();
        Float expResult = null;
        Float result = instance.getPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPrice method, of class Photo.
     */
    @Test
    public void testSetPrice()
    {
        System.out.println("setPrice");
        Float price = null;
        Photo instance = new Photo();
        instance.setPrice(price);
    }

    /**
     * Test of getPhotoType method, of class Photo.
     */
    @Test
    public void testGetPhotoType()
    {
        System.out.println("getPhotoType");
        Photo instance = new Photo();
        PhotoType expResult = null;
        PhotoType result = instance.getPhotoType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPhotoType method, of class Photo.
     */
    @Test
    public void testSetPhotoType()
    {
        System.out.println("setPhotoType");
        PhotoType photoType = null;
        Photo instance = new Photo();
        instance.setPhotoType(photoType);
    }

    /**
     * Test of getThumbnail method, of class Photo.
     */
    @Test
    public void testGetThumbnail()
    {
        System.out.println("getThumbnail");
        Photo instance = new Photo();
        PhotoData expResult = null;
        PhotoData result = instance.getThumbnail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setThumbnail method, of class Photo.
     */
    @Test
    public void testSetThumbnail()
    {
        System.out.println("setThumbnail");
        PhotoData thumbnail = null;
        Photo instance = new Photo();
        instance.setThumbnail(thumbnail);
    }

    /**
     * Test of isActive method, of class Photo.
     */
    @Test
    public void testIsActive()
    {
        System.out.println("isActive");
        Photo instance = new Photo();
        boolean expResult = false;
        boolean result = instance.isActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of setActive method, of class Photo.
     */
    @Test
    public void testSetActive()
    {
        System.out.println("setActive");
        boolean active = false;
        Photo instance = new Photo();
        instance.setActive(active);
    }

    /**
     * Test of getClientName method, of class Photo.
     */
    @Test
    public void testGetClientName()
    {
        System.out.println("getClientName");
        Photo instance = new Photo();
        String expResult = null;
        String result = instance.getClientName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setClientName method, of class Photo.
     */
    @Test
    public void testSetClientName()
    {
        System.out.println("setClientName");
        String clientName = "";
        Photo instance = new Photo();
        instance.setClientName(clientName);
    }
    
}
