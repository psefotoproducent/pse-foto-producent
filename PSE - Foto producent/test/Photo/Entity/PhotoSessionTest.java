/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Photo.Entity;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class PhotoSessionTest
{
    

    /**
     * Test of getPhotoSessionId method, of class PhotoSession.
     */
    @Test
    public void testGetPhotoSessionId()
    {
        System.out.println("getPhotoSessionId");
        PhotoSession instance = new PhotoSession();
        Long expResult = null;
        Long result = instance.getPhotoSessionId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPhotographerId method, of class PhotoSession.
     */
    @Test
    public void testGetPhotographerId()
    {
        System.out.println("getPhotographerId");
        PhotoSession instance = new PhotoSession();
        Long expResult = null;
        Long result = instance.getPhotographerId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPhotographerId method, of class PhotoSession.
     */
    @Test
    public void testSetPhotographerId()
    {
        System.out.println("setPhotographerId");
        Long photographerId = null;
        PhotoSession instance = new PhotoSession();
        instance.setPhotographerId(photographerId);
    }

    /**
     * Test of getDescription method, of class PhotoSession.
     */
    @Test
    public void testGetDescription()
    {
        System.out.println("getDescription");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescription method, of class PhotoSession.
     */
    @Test
    public void testSetDescription()
    {
        System.out.println("setDescription");
        String description = "";
        PhotoSession instance = new PhotoSession();
        instance.setDescription(description);
    }

    /**
     * Test of getDate method, of class PhotoSession.
     */
    @Test
    public void testGetDate()
    {
        System.out.println("getDate");
        PhotoSession instance = new PhotoSession();
        Date expResult = null;
        Date result = instance.getDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDate method, of class PhotoSession.
     */
    @Test
    public void testSetDate()
    {
        System.out.println("setDate");
        Date date = null;
        PhotoSession instance = new PhotoSession();
        instance.setDate(date);
    }

    /**
     * Test of getOrganisation method, of class PhotoSession.
     */
    @Test
    public void testGetOrganisation()
    {
        System.out.println("getOrganisation");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getOrganisation();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganisation method, of class PhotoSession.
     */
    @Test
    public void testSetOrganisation()
    {
        System.out.println("setOrganisation");
        String organisation = "";
        PhotoSession instance = new PhotoSession();
        instance.setOrganisation(organisation);
    }

    /**
     * Test of getOrganisationHouseNumber method, of class PhotoSession.
     */
    @Test
    public void testGetOrganisationHouseNumber()
    {
        System.out.println("getOrganisationHouseNumber");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getOrganisationHouseNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganisationHouseNumber method, of class PhotoSession.
     */
    @Test
    public void testSetOrganisationHouseNumber()
    {
        System.out.println("setOrganisationHouseNumber");
        String organisationHouseNumber = "";
        PhotoSession instance = new PhotoSession();
        instance.setOrganisationHouseNumber(organisationHouseNumber);
    }

    /**
     * Test of getOrganisationCity method, of class PhotoSession.
     */
    @Test
    public void testGetOrganisationCity()
    {
        System.out.println("getOrganisationCity");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getOrganisationCity();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganisationCity method, of class PhotoSession.
     */
    @Test
    public void testSetOrganisationCity()
    {
        System.out.println("setOrganisationCity");
        String organisationCity = "";
        PhotoSession instance = new PhotoSession();
        instance.setOrganisationCity(organisationCity);
    }

    /**
     * Test of getOrganisationStreet method, of class PhotoSession.
     */
    @Test
    public void testGetOrganisationStreet()
    {
        System.out.println("getOrganisationStreet");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getOrganisationStreet();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganisationStreet method, of class PhotoSession.
     */
    @Test
    public void testSetOrganisationStreet()
    {
        System.out.println("setOrganisationStreet");
        String organisationStreet = "";
        PhotoSession instance = new PhotoSession();
        instance.setOrganisationStreet(organisationStreet);
    }

    /**
     * Test of getOrganisationPostalCode method, of class PhotoSession.
     */
    @Test
    public void testGetOrganisationPostalCode()
    {
        System.out.println("getOrganisationPostalCode");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getOrganisationPostalCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganisationPostalCode method, of class PhotoSession.
     */
    @Test
    public void testSetOrganisationPostalCode()
    {
        System.out.println("setOrganisationPostalCode");
        String organisationPostalCode = "";
        PhotoSession instance = new PhotoSession();
        instance.setOrganisationPostalCode(organisationPostalCode);
    }

    /**
     * Test of getAddress method, of class PhotoSession.
     */
    @Test
    public void testGetAddress()
    {
        System.out.println("getAddress");
        PhotoSession instance = new PhotoSession();
        String expResult = "";
        String result = instance.getAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPortraitClients method, of class PhotoSession.
     */
    @Test
    public void testGetPortraitClients()
    {
        System.out.println("getPortraitClients");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getPortraitClients();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPortraitClients method, of class PhotoSession.
     */
    @Test
    public void testSetPortraitClients()
    {
        System.out.println("setPortraitClients");
        String portraitClients = "";
        PhotoSession instance = new PhotoSession();
        instance.setPortraitClients(portraitClients);
    }

    /**
     * Test of getLoginCode method, of class PhotoSession.
     */
    @Test
    public void testGetLoginCode()
    {
        System.out.println("getLoginCode");
        PhotoSession instance = new PhotoSession();
        String expResult = null;
        String result = instance.getLoginCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLoginCode method, of class PhotoSession.
     */
    @Test
    public void testSetLoginCode()
    {
        System.out.println("setLoginCode");
        String loginCode = "";
        PhotoSession instance = new PhotoSession();
        instance.setLoginCode(loginCode);
    }
    
}
