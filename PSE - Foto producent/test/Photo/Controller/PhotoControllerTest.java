package Photo.Controller;

import Login.Entity.Photographer;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.LoginService;
import Login.Service.PhotographerService;
import Order.Entity.Invoice;
import Order.Entity.InvoiceLine;
import Order.Entity.ShoppingCartLine;
import Order.Service.InvoiceLineService;
import Order.Service.InvoiceService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import Product.Entity.Product;
import Product.Service.ProductService;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Lars
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class PhotoControllerTest
{

    private MockMvc mockMvc;
    private MockHttpSession session;

    private PhotoSession photoSession;
    private Photographer photographer;
    private Photo photo;
    private Product product;
    private User user;
    private InvoiceLine invoiceLine;
    private ShoppingCartLine scl;
    private Invoice invoice;

    @Autowired
    private LoginService loginService;

    @Autowired
    private PhotoSessionService photoSessionService;

    @Autowired
    private PhotographerService photographerService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ProductService productService;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private InvoiceLineService invoiceLineService;

    @Before
    public void setUp() throws Exception, MalformedURLException, IOException
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        Date date = new Date(2016, 10, 17);

        this.user = new User("voornaam", "insertion", "lastname", "street", "25", "5555GT", "Eindhoven", "12345678", "username", "unhashed", UserType.CLIENT);
        this.photographer = new Photographer();
        this.photographer.setGroupPrice(1F);
        this.photographer.setPortraitPrice(2F);
        this.photographer.setUser(user);
        this.photographerService.insert(this.photographer);

        this.photoSession = new PhotoSession();
        this.photoSession.setDate(date);
        this.photoSession.setDescription("Dikke description");
        this.photoSession.setLoginCode("12345");
        this.photoSession.setOrganisation("Fontys Eindhoven Unittest");
        this.photoSession.setOrganisationCity("Eindhoven");
        this.photoSession.setOrganisationHouseNumber("1");
        this.photoSession.setOrganisationStreet("Rachelsmolen");
        this.photoSession.setOrganisationPostalCode("1234BB");
        this.photoSession.setPhotographerId(this.photographer.getPhotographerId());
        this.photoSessionService.insert(this.photoSession);

        PhotoData photoData = new PhotoData();
        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        InputStream is;
        photoData.setData(IOUtils.toByteArray(imageUrl.openStream()));
        PhotoType photoType = PhotoType.GROUP;
        // plaatje.jpg moet echt bestaan om de tests te laten draaien
        this.photo = new Photo(this.photoSession, "c:\\Temp\\plaatje.jpg", 5F, photoType, photoData);
        this.photo.setActive(true);
//        this.photoService.insert(photo);

        this.product = new Product(1F, "Product description");
        product.setThumbnail(new PhotoData(new byte[]
        {
        }));
//        this.productService.insert(product);

        this.session = new MockHttpSession();
        this.session.setAttribute("loggedInPhotographer", this.photographer);
        this.session.setAttribute("loggedInUser", this.photographer.getUser());
        this.session.setAttribute("loginCode", "321321");

        this.invoice = new Invoice(new String("321321"), "Lars", "van der", "Sangen", "De dollard", "26", "5463RG", "Veghel", "lvandersangen@student.fontys.nl", "06-18524658787");
//        this.invoiceService.insert(invoice);

//        this.invoice = this.invoiceService.get(new Integer(this.invoice.getInvoiceId().toString()));
        this.invoiceLine = new InvoiceLine(this.invoice, this.photo, this.product, "Sepia", 2, 2.0F);
        this.invoiceLineService.insert(invoiceLine);
    }

    @After
    public void tearDown()
    {
        this.photographerService.remove(this.photographer);
        this.loginService.remove(this.user);
    }

    @Test
    public void testPhotoSessionUploadActionGETSucces() throws Exception
    {
        Long photoSessionId = this.photoSession.getPhotoSessionId();
        this.mockMvc.perform(get("/photosession/" + photoSessionId).accept("text/html"))
                .andExpect(status().isOk());
    }

    @Test
    public void testPhotoSessionUploadActionGETFail() throws Exception
    {
        this.mockMvc.perform(get("/photosession/NaN").accept("text/html"))
                .andExpect(status().isOk());
    }

    @Test
    public void testPhotoSessionUploadActionPOSTSuccess() throws Exception
    {
        Long photoSessionId = this.photoSession.getPhotoSessionId();
        this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/photosession/" + photoSessionId).accept("text/html")
                .pathInfo("/\\een\\andere\\padnaam\\bestand.jpeg"))
                .andExpect(status().isFound());
    }

    @Test
    public void testGetPictureActionGET() throws Exception
    {
        Long photoId = this.photo.getPhotoId();
        this.mockMvc.perform(get("/photosession/getPicture/" + photoId))
                .andExpect(status().isOk());
    }


    @Test
    public void testsavePortraitClient() throws Exception
    {
        this.photoSession.setPortraitClients("Client1-12jn12uoin4,Client2-1509nwk2342k,Client3-23nkfjn349");
        this.photoSessionService.update(this.photoSession);

        Long photoId = this.photo.getPhotoId();
        Long photoSessionId = this.photoSession.getPhotoSessionId();
        String clientCode = this.photoSession.getClientCode("Client1");

        this.mockMvc.perform(MockMvcRequestBuilders.post("/photosession/savePortraitClient/")
                .param("photoSessionId", photoSessionId.toString())
                .param("photoId", photoId.toString())
                .param("client", clientCode))
                .andExpect(status().isOk());
    }

    @Test
    public void testsavePortraitClientSecondBranch() throws Exception
    {
        this.photoSession.setPortraitClients("Client1-12jn12uoin4,Client2-1509nwk2342k,Client3-23nkfjn349");
        this.photoSessionService.update(this.photoSession);

        Long photoId = this.photo.getPhotoId();
        Long photoSessionId = this.photoSession.getPhotoSessionId();
        String clientCode = this.photoSession.getClientCode("Client1");

        this.mockMvc.perform(MockMvcRequestBuilders.post("/photosession/savePortraitClient/")
                .session(this.session)
                .param("photoSessionId", photoSessionId.toString())
                .param("photoId", photoId.toString())
                .param("client", clientCode))
                .andExpect(status().isOk());
    }

}
