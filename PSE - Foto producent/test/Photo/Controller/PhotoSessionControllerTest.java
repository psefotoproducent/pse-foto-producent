package Photo.Controller;

import Login.Entity.Photographer;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.LoginService;
import Login.Service.PhotographerService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Lars
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class PhotoSessionControllerTest
{

    private MockMvc mockMvc;

    private PhotoSession photoSession;
    private Photographer photographer;
    private Photo photo;
    private User user;
    private PhotoData photoData;
    private PhotoType photoType;

    @Autowired
    private LoginService loginService;

    @Autowired
    private PhotoSessionService photoSessionService;

    @Autowired
    private PhotographerService photographerService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() throws MalformedURLException, IOException
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        Date date = new Date(2016, 10, 17);

        this.user = new User("voornaam", "insertion", "lastname", "street", "25", "5555GT", "Eindhoven", "12345678", "username", "unhashed", UserType.PHOTOGRAPHER);
        this.photographer = new Photographer();
        this.photographer.setGroupPrice(1F);
        this.photographer.setPortraitPrice(2F);
        this.photographer.setUser(user);
        this.photographerService.insert(this.photographer);

        this.photoSession = new PhotoSession();
        this.photoSession.setDate(date);
        this.photoSession.setDescription("Dikke description");
        this.photoSession.setLoginCode("12345");
        this.photoSession.setOrganisation("Fontys Eindhoven Unittest");
        this.photoSession.setOrganisationCity("Eindhoven");
        this.photoSession.setOrganisationHouseNumber("1");
        this.photoSession.setOrganisationStreet("Rachelsmolen");
        this.photoSession.setOrganisationPostalCode("1234BB");
        this.photoSession.setPhotographerId(this.photographer.getPhotographerId());
        this.photoSessionService.insert(this.photoSession);

        PhotoData photoData = new PhotoData();
        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        InputStream is;
        photoData.setData(IOUtils.toByteArray(imageUrl.openStream()));
        PhotoType photoType = PhotoType.GROUP;
        // plaatje.jpg moet echt bestaan om de tests te laten draaien
        this.photo = new Photo(this.photoSession, "c:\\Temp\\plaatje.jpg", 5F, photoType, photoData);
        this.photo.setActive(true);
        this.photoService.insert(photo);

    }

    @After
    public void tearDown()
    {
        this.photographerService.remove(this.photographer);
        this.loginService.remove(this.user);
        this.photoSessionService.remove(this.photoSession.getPhotoSessionId().intValue());
    }

    @Test
    public void testPhotoSessionActionGET() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        this.mockMvc.perform(get("/photosession").accept("text/html").
                session(session))
                .andExpect(status().isOk());
    }

    @Test
    public void testPhotoSessionActionPOST() throws Exception
    {
        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        MockMultipartFile firstFile = new MockMultipartFile("data.png", "filename.png", "image/png", imageUrl.openStream());

        this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/photosession/" + this.photoSession.getPhotoSessionId().toString())
                .file(firstFile)
                .param("price", "4")
                .pathInfo("/Temp/plaatje.jpg"))
                .andExpect(status().isFound());
    }

    @Test
    public void testManagementPhotoSessionAddActionGet() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        this.mockMvc.perform(get("/management/photosession/add")
                .accept("text/html")
                .session(session))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("photo/sessions/edit"));
    }

    @Test
    public void testManagementPhotoSessionAddActionPost() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photosession/add")
                .accept("text/html")
                .param("photographerId", this.photographer.getPhotographerId().toString())
                .param("description", "description")
                .param("organisation", "Fontys")
                .param("organisationStreet", "Rachelsmolen")
                .param("organisationHouseNumber", "1")
                .param("organisationPostalCode", "1111GG")
                .param("organisationCity", "Eindhoven")
                .param("allClients", "allclients")
                .session(session))
                .andExpect(status().isFound());
    }

    @Test
    public void testManagementPhotoSessionDeleteActionPost() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photosession/delete")
                .accept("text/html")
                .param("photoSessionId", this.photoSession.getPhotoSessionId().toString())
                .session(session))
                .andExpect(status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/photosession"));
        
        Date date = new Date(2016, 10, 17);
        this.photoSession = new PhotoSession();
        this.photoSession.setDate(date);
        this.photoSession.setDescription("Dikke description");
        this.photoSession.setLoginCode("12345");
        this.photoSession.setOrganisation("Fontys Eindhoven Unittest");
        this.photoSession.setOrganisationCity("Eindhoven");
        this.photoSession.setOrganisationHouseNumber("1");
        this.photoSession.setOrganisationStreet("Rachelsmolen");
        this.photoSession.setOrganisationPostalCode("1234BB");
        this.photoSession.setPhotographerId(this.photographer.getPhotographerId());
        this.photoSessionService.insert(this.photoSession);
    }

    @Test
    public void testManagementPhotosessionAddBufferGET() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        this.mockMvc.perform(MockMvcRequestBuilders.get("/management/photosession/addBuffer")
                .accept("text/html")
                .param("photoSessionId", this.photoSession.getPhotoSessionId().toString())
                .session(session))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("photo/sessions/edit_multiple"));
    }

    @Test
    public void testManagementPhotosessionAddBufferPOST() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photosession/addBuffer")
                .accept("text/plain")
                .param("photographerId", this.photographer.getPhotographerId().toString())
                .param("sessioncount", "2")
                .param("description", "description")
                .param("organisation", "Fontys")
                .param("organisationStreet", "Rachelsmolen")
                .param("organisationHouseNumber", "1")
                .param("organisationPostalCode", "1111GG")
                .param("organisationCity", "Eindhoven")
                .session(session))
                .andExpect(status().isOk());
    }
    @Test
    public void testPhotoSessionActionPost() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/photosession/getLoginCodes/" + this.photoSession.getPhotoSessionId().toString())
                .accept("text/plain")
                .param("photographerId", this.photographer.getPhotographerId().toString())
                .param("sessioncount", "2")
                .param("description", "description")
                .param("organisation", "Fontys")
                .param("organisationStreet", "Rachelsmolen")
                .param("organisationHouseNumber", "1")
                .param("organisationPostalCode", "1111GG")
                .param("organisationCity", "Eindhoven")
                .session(session))
                .andExpect(status().isOk());
    }

    @Test
    public void testManagementPhotoSessionDeleteActionPostNAN() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        List<PhotoSession> list = this.photoSessionService.findAll(this.photographer.getPhotographerId());
        PhotoSession ph = list.get(0);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photosession/delete")
                .accept("text/html")
                .param("photoSessionId", "NAN")
                .session(session))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("photo/sessions/overview"));
    }

    @Test
    public void testManagementPhotosessionEditGet() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        List<PhotoSession> list = this.photoSessionService.findAll(this.photographer.getPhotographerId());
        PhotoSession ph = list.get(0);
        Long photoSessionId = ph.getPhotoSessionId();

        this.mockMvc.perform(get("/management/photosession/edit/" + photoSessionId.toString())
                .accept("text/html")
                .session(session))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("photo/sessions/edit"));
    }

    @Test
    public void testManagementPhotosessionEditPost() throws Exception
    {
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInPhotographer", this.photographer);
        session.setAttribute("loggedInUser", this.photographer.getUser());

        List<PhotoSession> list = this.photoSessionService.findAll(this.photographer.getPhotographerId());
        PhotoSession ph = list.get(0);
        Long photoSessionId = ph.getPhotoSessionId();

        this.mockMvc.perform(MockMvcRequestBuilders.post("/management/photosession/edit/" + photoSessionId.toString())
                .param("description", "description")
                .param("organisation", "Fontys")
                .param("organisationStreet", "Rachelsmolen")
                .param("organisationHouseNumber", "1")
                .param("organisationPostalCode", "1111GG")
                .param("organisationCity", "Eindhoven")
                .accept("text/html")
                .session(session))
                .andExpect(status().isFound())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/photosession"));
    }

}
