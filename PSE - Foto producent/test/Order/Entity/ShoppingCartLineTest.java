/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class ShoppingCartLineTest
{
    
    /**
     * Test of getShoppingCartLineId method, of class ShoppingCartLine.
     */
    @Test
    public void testGetShoppingCartLineId()
    {
        System.out.println("getShoppingCartLineId");
        ShoppingCartLine instance = new ShoppingCartLine();
        int expResult = 0;
        int result = instance.getShoppingCartLineId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setShoppingCartLineId method, of class ShoppingCartLine.
     */
    @Test
    public void testSetShoppingCartLineId()
    {
        System.out.println("setShoppingCartLineId");
        int shoppingCartLineId = 0;
        ShoppingCartLine instance = new ShoppingCartLine();
        instance.setShoppingCartLineId(shoppingCartLineId);
    }

    /**
     * Test of getPhotoId method, of class ShoppingCartLine.
     */
    @Test
    public void testGetPhotoId()
    {
        System.out.println("getPhotoId");
        ShoppingCartLine instance = new ShoppingCartLine();
        Long expResult = null;
        Long result = instance.getPhotoId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPhotoId method, of class ShoppingCartLine.
     */
    @Test
    public void testSetPhotoId()
    {
        System.out.println("setPhotoId");
        Long photoId = null;
        ShoppingCartLine instance = new ShoppingCartLine();
        instance.setPhotoId(photoId);
    }

    /**
     * Test of getProductId method, of class ShoppingCartLine.
     */
    @Test
    public void testGetProductId()
    {
        System.out.println("getProductId");
        ShoppingCartLine instance = new ShoppingCartLine();
        Long expResult = null;
        Long result = instance.getProductId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setProductId method, of class ShoppingCartLine.
     */
    @Test
    public void testSetProductId()
    {
        System.out.println("setProductId");
        Long productId = null;
        ShoppingCartLine instance = new ShoppingCartLine();
        instance.setProductId(productId);
    }

    /**
     * Test of getProductDescription method, of class ShoppingCartLine.
     */
    @Test
    public void testGetProductDescription()
    {
        System.out.println("getProductDescription");
        ShoppingCartLine instance = new ShoppingCartLine();
        String expResult = null;
        String result = instance.getProductDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of setProductDescription method, of class ShoppingCartLine.
     */
    @Test
    public void testSetProductDescription()
    {
        System.out.println("setProductDescription");
        String productDescription = "";
        ShoppingCartLine instance = new ShoppingCartLine();
        instance.setProductDescription(productDescription);
    }

    /**
     * Test of getPrice method, of class ShoppingCartLine.
     */
    @Test
    public void testGetPrice()
    {
        System.out.println("getPrice");
        ShoppingCartLine instance = new ShoppingCartLine();
        float expResult = 0.0F;
        float result = instance.getPrice();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setPrice method, of class ShoppingCartLine.
     */
    @Test
    public void testSetPrice()
    {
        System.out.println("setPrice");
        float price = 0.0F;
        ShoppingCartLine instance = new ShoppingCartLine();
        instance.setPrice(price);
    }

    /**
     * Test of getColor method, of class ShoppingCartLine.
     */
    @Test
    public void testGetColor()
    {
        System.out.println("getColor");
        ShoppingCartLine instance = new ShoppingCartLine();
        String expResult = null;
        String result = instance.getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of setColor method, of class ShoppingCartLine.
     */
    @Test
    public void testSetColor()
    {
        System.out.println("setColor");
        String color = "";
        ShoppingCartLine instance = new ShoppingCartLine();
        instance.setColor(color);
    }

    /**
     * Test of getAmount method, of class ShoppingCartLine.
     */
    @Test
    public void testGetAmount()
    {
        System.out.println("getAmount");
        ShoppingCartLine instance = new ShoppingCartLine();
        int expResult = 0;
        int result = instance.getAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAmount method, of class ShoppingCartLine.
     */
    @Test
    public void testSetAmount()
    {
        System.out.println("setAmount");
        int amount = 0;
        ShoppingCartLine instance = new ShoppingCartLine();
        instance.setAmount(amount);
    }

}
