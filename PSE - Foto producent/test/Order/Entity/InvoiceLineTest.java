/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import Photo.Entity.Photo;
import Product.Entity.Product;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class InvoiceLineTest
{
    
    /**
     * Test of getInvoiceLineId method, of class InvoiceLine.
     */
    @Test
    public void testGetInvoiceLineId()
    {
        System.out.println("getInvoiceLineId");
        InvoiceLine instance = new InvoiceLine();
        Long expResult = null;
        Long result = instance.getInvoiceLineId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInvoiceLineId method, of class InvoiceLine.
     */
    @Test
    public void testSetInvoiceLineId()
    {
        System.out.println("setInvoiceLineId");
        Long invoiceLineId = null;
        InvoiceLine instance = new InvoiceLine();
        instance.setInvoiceLineId(invoiceLineId);
    }

    /**
     * Test of getInvoice method, of class InvoiceLine.
     */
    @Test
    public void testGetInvoice()
    {
        System.out.println("getInvoice");
        InvoiceLine instance = new InvoiceLine();
        Invoice expResult = null;
        Invoice result = instance.getInvoice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInvoice method, of class InvoiceLine.
     */
    @Test
    public void testSetInvoice()
    {
        System.out.println("setInvoice");
        Invoice invoice = null;
        InvoiceLine instance = new InvoiceLine();
        instance.setInvoice(invoice);
    }

    /**
     * Test of getPhoto method, of class InvoiceLine.
     */
    @Test
    public void testGetPhoto()
    {
        System.out.println("getPhoto");
        InvoiceLine instance = new InvoiceLine();
        Photo expResult = null;
        Photo result = instance.getPhoto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPhotoId method, of class InvoiceLine.
     */
    @Test
    public void testSetPhotoId()
    {
        System.out.println("setPhotoId");
        Photo photo = null;
        InvoiceLine instance = new InvoiceLine();
        instance.setPhotoId(photo);
    }

    /**
     * Test of getProduct method, of class InvoiceLine.
     */
    @Test
    public void testGetProduct()
    {
        System.out.println("getProduct");
        InvoiceLine instance = new InvoiceLine();
        Product expResult = null;
        Product result = instance.getProduct();
        assertEquals(expResult, result);
    }

    /**
     * Test of setProductId method, of class InvoiceLine.
     */
    @Test
    public void testSetProductId()
    {
        System.out.println("setProductId");
        Product product = null;
        InvoiceLine instance = new InvoiceLine();
        instance.setProductId(product);
    }

    /**
     * Test of getColor method, of class InvoiceLine.
     */
    @Test
    public void testGetColor()
    {
        System.out.println("getColor");
        InvoiceLine instance = new InvoiceLine();
        String expResult = null;
        String result = instance.getColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of setColor method, of class InvoiceLine.
     */
    @Test
    public void testSetColor()
    {
        System.out.println("setColor");
        String color = "";
        InvoiceLine instance = new InvoiceLine();
        instance.setColor(color);
    }

    /**
     * Test of getAmount method, of class InvoiceLine.
     */
    @Test
    public void testGetAmount()
    {
        System.out.println("getAmount");
        InvoiceLine instance = new InvoiceLine();
        int expResult = 0;
        int result = instance.getAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAmount method, of class InvoiceLine.
     */
    @Test
    public void testSetAmount()
    {
        System.out.println("setAmount");
        int amount = 0;
        InvoiceLine instance = new InvoiceLine();
        instance.setAmount(amount);
    }

    /**
     * Test of getPrice method, of class InvoiceLine.
     */
    @Test
    public void testGetPrice()
    {
        System.out.println("getPrice");
        InvoiceLine instance = new InvoiceLine();
        float expResult = 0.0F;
        float result = instance.getPrice();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setPrice method, of class InvoiceLine.
     */
    @Test
    public void testSetPrice()
    {
        System.out.println("setPrice");
        float price = 0.0F;
        InvoiceLine instance = new InvoiceLine();
        instance.setPrice(price);
    }

    /**
     * Test of setDone method, of class InvoiceLine.
     */
    @Test
    public void testSetDone()
    {
        System.out.println("setDone");
        int i = 0;
        InvoiceLine instance = new InvoiceLine();
        instance.setDone(i);
    }

    /**
     * Test of reverseDone method, of class InvoiceLine.
     */
    @Test
    public void testReverseDone()
    {
        System.out.println("reverseDone");
        int i = 0;
        InvoiceLine instance = new InvoiceLine();
        instance.reverseDone(i);
    }

    /**
     * Test of setDoneFalse method, of class InvoiceLine.
     */
    @Test
    public void testSetDoneFalse()
    {
        System.out.println("setDoneFalse");
        InvoiceLine instance = new InvoiceLine();
        int expResult = 0;
        int result = instance.setDoneFalse();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDone method, of class InvoiceLine.
     */
    @Test
    public void testGetDone()
    {
        System.out.println("getDone");
        InvoiceLine instance = new InvoiceLine();
        int expResult = 0;
        int result = instance.getDone();
        assertEquals(expResult, result);
    }

   
}
