/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class InvoiceTest
{
    
    /**
     * Test of onCreate method, of class Invoice.
     */
    @Test
    public void testOnCreate()
    {
        System.out.println("onCreate");
        Invoice instance = new Invoice();
        instance.onCreate();
    }

    /**
     * Test of getInvoiceId method, of class Invoice.
     */
    @Test
    public void testGetInvoiceId()
    {
        System.out.println("getInvoiceId");
        Invoice instance = new Invoice();
        Long expResult = null;
        Long result = instance.getInvoiceId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInvoiceId method, of class Invoice.
     */
    @Test
    public void testSetInvoiceId()
    {
        System.out.println("setInvoiceId");
        Long invoiceId = null;
        Invoice instance = new Invoice();
        instance.setInvoiceId(invoiceId);
    }

    /**
     * Test of getLoginCode method, of class Invoice.
     */
    @Test
    public void testGetLoginCode()
    {
        System.out.println("getLoginCode");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getLoginCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLoginCode method, of class Invoice.
     */
    @Test
    public void testSetLoginCode()
    {
        System.out.println("setLoginCode");
        String loginCode = "";
        Invoice instance = new Invoice();
        instance.setLoginCode(loginCode);
    }

    /**
     * Test of getCreated method, of class Invoice.
     */
    @Test
    public void testGetCreated()
    {
        System.out.println("getCreated");
        Invoice instance = new Invoice();
        Date expResult = null;
        Date result = instance.getCreated();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDate method, of class Invoice.
     */
    @Test
    public void testSetDate()
    {
        System.out.println("setDate");
        Date date = null;
        Invoice instance = new Invoice();
        instance.setDate(date);
    }

    /**
     * Test of getFirstName method, of class Invoice.
     */
    @Test
    public void testGetFirstName()
    {
        System.out.println("getFirstName");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getFirstName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFirstName method, of class Invoice.
     */
    @Test
    public void testSetFirstName()
    {
        System.out.println("setFirstName");
        String firstName = "";
        Invoice instance = new Invoice();
        instance.setFirstName(firstName);
    }

    /**
     * Test of getInsertion method, of class Invoice.
     */
    @Test
    public void testGetInsertion()
    {
        System.out.println("getInsertion");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getInsertion();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInsertion method, of class Invoice.
     */
    @Test
    public void testSetInsertion()
    {
        System.out.println("setInsertion");
        String insertion = "";
        Invoice instance = new Invoice();
        instance.setInsertion(insertion);
    }

    /**
     * Test of getSurName method, of class Invoice.
     */
    @Test
    public void testGetSurName()
    {
        System.out.println("getSurName");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getSurName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSurName method, of class Invoice.
     */
    @Test
    public void testSetSurName()
    {
        System.out.println("setSurName");
        String surName = "";
        Invoice instance = new Invoice();
        instance.setSurName(surName);
    }

    /**
     * Test of getStreet method, of class Invoice.
     */
    @Test
    public void testGetStreet()
    {
        System.out.println("getStreet");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getStreet();
        assertEquals(expResult, result);
    }

    /**
     * Test of setStreet method, of class Invoice.
     */
    @Test
    public void testSetStreet()
    {
        System.out.println("setStreet");
        String street = "";
        Invoice instance = new Invoice();
        instance.setStreet(street);
    }

    /**
     * Test of getHouseNumber method, of class Invoice.
     */
    @Test
    public void testGetHouseNumber()
    {
        System.out.println("getHouseNumber");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getHouseNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHouseNumber method, of class Invoice.
     */
    @Test
    public void testSetHouseNumber()
    {
        System.out.println("setHouseNumber");
        String houseNumber = "";
        Invoice instance = new Invoice();
        instance.setHouseNumber(houseNumber);
    }

    /**
     * Test of getPostalCode method, of class Invoice.
     */
    @Test
    public void testGetPostalCode()
    {
        System.out.println("getPostalCode");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getPostalCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPostalCode method, of class Invoice.
     */
    @Test
    public void testSetPostalCode()
    {
        System.out.println("setPostalCode");
        String postalCode = "";
        Invoice instance = new Invoice();
        instance.setPostalCode(postalCode);
    }

    /**
     * Test of getCity method, of class Invoice.
     */
    @Test
    public void testGetCity()
    {
        System.out.println("getCity");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getCity();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCity method, of class Invoice.
     */
    @Test
    public void testSetCity()
    {
        System.out.println("setCity");
        String city = "";
        Invoice instance = new Invoice();
        instance.setCity(city);
    }

    /**
     * Test of getEmail method, of class Invoice.
     */
    @Test
    public void testGetEmail()
    {
        System.out.println("getEmail");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmail method, of class Invoice.
     */
    @Test
    public void testSetEmail()
    {
        System.out.println("setEmail");
        String email = "";
        Invoice instance = new Invoice();
        instance.setEmail(email);
    }

    /**
     * Test of getPhoneNumber method, of class Invoice.
     */
    @Test
    public void testGetPhoneNumber()
    {
        System.out.println("getPhoneNumber");
        Invoice instance = new Invoice();
        String expResult = null;
        String result = instance.getPhoneNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPhoneNumber method, of class Invoice.
     */
    @Test
    public void testSetPhoneNumber()
    {
        System.out.println("setPhoneNumber");
        String phoneNumber = "";
        Invoice instance = new Invoice();
        instance.setPhoneNumber(phoneNumber);
    }

    /**
     * Test of getInvoiceLines method, of class Invoice.
     */
    @Test
    public void testGetInvoiceLines()
    {
        System.out.println("getInvoiceLines");
        Invoice instance = new Invoice();
        Set<InvoiceLine> expResult =  new HashSet<InvoiceLine>();
        Set<InvoiceLine> result = instance.getInvoiceLines();
        assertEquals(expResult, result);
    }

    /**
     * Test of isPayed method, of class Invoice.
     */
    @Test
    public void testIsPayed()
    {
        System.out.println("isPayed");
        Invoice instance = new Invoice();
        boolean expResult = false;
        boolean result = instance.isPayed();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPayed method, of class Invoice.
     */
    @Test
    public void testSetPayed()
    {
        System.out.println("setPayed");
        boolean payed = false;
        Invoice instance = new Invoice();
        instance.setPayed(payed);
    }
    
}
