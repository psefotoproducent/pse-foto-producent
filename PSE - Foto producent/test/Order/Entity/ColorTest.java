/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class ColorTest
{

    /**
     * Test of getValue method, of class Color.
     */
    @Test
    public void testGetValue()
    {
        System.out.println("getValue");
        Color instance = Color.COLOR;
        int expResult = 1;
        int result = instance.getValue();
        assertEquals(expResult, result);
    }
    
}
