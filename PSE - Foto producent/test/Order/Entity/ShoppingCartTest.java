/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class ShoppingCartTest
{
   

    /**
     * Test of addShoppingCartLine method, of class ShoppingCart.
     */
    @Test
    public void testAddShoppingCartLine()
    {
        System.out.println("addShoppingCartLine");
        ShoppingCartLine shoppingCartLine = new ShoppingCartLine();
        ShoppingCart instance = new ShoppingCart();
        boolean expResult = true;
        boolean result = instance.addShoppingCartLine(shoppingCartLine);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeShoppingCartLine method, of class ShoppingCart.
     */
    @Test
    public void testRemoveShoppingCartLine()
    {
        System.out.println("removeShoppingCartLine");
        ShoppingCartLine shoppingCartLine = null;
        ShoppingCart instance = new ShoppingCart();
        boolean expResult = false;
        boolean result = instance.removeShoppingCartLine(shoppingCartLine);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTotalPrice method, of class ShoppingCart.
     */
    @Test
    public void testGetTotalPrice()
    {
        System.out.println("getTotalPrice");
        ShoppingCart instance = new ShoppingCart();
        float expResult = 0.0F;
        float result = instance.getTotalPrice();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getSize method, of class ShoppingCart.
     */
    @Test
    public void testGetSize()
    {
        System.out.println("getSize");
        ShoppingCart instance = new ShoppingCart();
        int expResult = 0;
        int result = instance.getSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAllShoppingCartLines method, of class ShoppingCart.
     */
    @Test
    public void testGetAllShoppingCartLines()
    {
        System.out.println("getAllShoppingCartLines");
        ShoppingCart instance = new ShoppingCart();
        ArrayList<ShoppingCartLine> expResult = new ArrayList<>();
        ArrayList<ShoppingCartLine> result = instance.getAllShoppingCartLines();
        assertEquals(expResult, result);
    }

    /**
     * Test of getShoppingCartLine method, of class ShoppingCart.
     */
    @Test
    public void testGetShoppingCartLine()
    {
        System.out.println("getShoppingCartLine");
        int shoppingCartLineId = 0;
        ShoppingCart instance = new ShoppingCart();
        ShoppingCartLine expResult = null;
        ShoppingCartLine result = instance.getShoppingCartLine(shoppingCartLineId);
        assertEquals(expResult, result);
    }
    
}
