/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity.Interface;

import Order.Entity.ShoppingCartLine;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class IShoppingCartTest
{
    
    public IShoppingCartTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of addShoppingCartLine method, of class IShoppingCart.
     */
    @Test
    public void testAddShoppingCartLine()
    {
        System.out.println("addShoppingCartLine");
        ShoppingCartLine shoppingCartLine = null;
        IShoppingCart instance = new IShoppingCartImpl();
        boolean expResult = false;
        boolean result = instance.addShoppingCartLine(shoppingCartLine);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeShoppingCartLine method, of class IShoppingCart.
     */
    @Test
    public void testRemoveShoppingCartLine()
    {
        System.out.println("removeShoppingCartLine");
        ShoppingCartLine shoppingCartLine = null;
        IShoppingCart instance = new IShoppingCartImpl();
        boolean expResult = false;
        boolean result = instance.removeShoppingCartLine(shoppingCartLine);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTotalPrice method, of class IShoppingCart.
     */
    @Test
    public void testGetTotalPrice()
    {
        System.out.println("getTotalPrice");
        IShoppingCart instance = new IShoppingCartImpl();
        float expResult = 0.0F;
        float result = instance.getTotalPrice();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSize method, of class IShoppingCart.
     */
    @Test
    public void testGetSize()
    {
        System.out.println("getSize");
        IShoppingCart instance = new IShoppingCartImpl();
        int expResult = 0;
        int result = instance.getSize();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getShoppingCartLine method, of class IShoppingCart.
     */
    @Test
    public void testGetShoppingCartLine()
    {
        System.out.println("getShoppingCartLine");
        int shoppingCartLineId = 0;
        IShoppingCart instance = new IShoppingCartImpl();
        ShoppingCartLine expResult = null;
        ShoppingCartLine result = instance.getShoppingCartLine(shoppingCartLineId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllShoppingCartLines method, of class IShoppingCart.
     */
    @Test
    public void testGetAllShoppingCartLines()
    {
        System.out.println("getAllShoppingCartLines");
        IShoppingCart instance = new IShoppingCartImpl();
        ArrayList<ShoppingCartLine> expResult = null;
        ArrayList<ShoppingCartLine> result = instance.getAllShoppingCartLines();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    public class IShoppingCartImpl implements IShoppingCart
    {

        public boolean addShoppingCartLine(ShoppingCartLine shoppingCartLine)
        {
            return false;
        }

        public boolean removeShoppingCartLine(ShoppingCartLine shoppingCartLine)
        {
            return false;
        }

        public float getTotalPrice()
        {
            return 0.0F;
        }

        public int getSize()
        {
            return 0;
        }

        public ShoppingCartLine getShoppingCartLine(int shoppingCartLineId)
        {
            return null;
        }

        public ArrayList<ShoppingCartLine> getAllShoppingCartLines()
        {
            return null;
        }
    }
    
}
