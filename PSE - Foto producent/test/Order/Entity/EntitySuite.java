/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Lars
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
    Order.Entity.Interface.InterfaceSuite.class, Order.Entity.ColorTest.class, Order.Entity.InvoiceTest.class, Order.Entity.InvoiceLineTest.class, Order.Entity.ShoppingCartTest.class, Order.Entity.ShoppingCartLineTest.class
})
public class EntitySuite
{

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }
    
}
