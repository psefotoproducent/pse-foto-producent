package Order.Controller;

import Login.Entity.Photographer;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.LoginService;
import Login.Service.PhotographerService;
import Order.Entity.Invoice;
import Order.Entity.InvoiceLine;
import Order.Entity.ShoppingCartLine;
import Order.Service.InvoiceLineService;
import Order.Service.InvoiceService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import Product.Entity.Product;
import Product.Service.ProductService;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author Lars
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations =
{
    "/servlet.xml", "/dispatcher-servlet.xml"
})
public class OrderControllerTest
{
    private MockMvc mockMvc;
    private MockHttpSession session;

    private PhotoSession photoSession;
    private Photographer photographer;
    private Photo photo;
    private Product product;
    private User user;
    private InvoiceLine invoiceLine;
    private ShoppingCartLine scl;
    private Invoice invoice;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private PhotoSessionService photoSessionService;

    @Autowired
    private PhotographerService photographerService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ProductService productService;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private InvoiceLineService invoiceLineService;

    @Before
    public void setUp() throws MalformedURLException, IOException
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        Date date = new Date(2016, 10, 17);

        this.user = new User("voornaam", "insertion", "lastname", "street", "25", "5555GT", "Eindhoven", "12345678", "username", "unhashed", UserType.CLIENT);
        this.photographer = new Photographer();
        this.photographer.setGroupPrice(1F);
        this.photographer.setPortraitPrice(2F);
        this.photographer.setUser(user);
        this.photographerService.insert(this.photographer);

        this.photoSession = new PhotoSession();
        this.photoSession.setDate(date);
        this.photoSession.setDescription("Dikke description");
        this.photoSession.setLoginCode("12345");
        this.photoSession.setOrganisation("Fontys Eindhoven Unittest");
        this.photoSession.setOrganisationCity("Eindhoven");
        this.photoSession.setOrganisationHouseNumber("1");
        this.photoSession.setOrganisationStreet("Rachelsmolen");
        this.photoSession.setOrganisationPostalCode("1234BB");
        this.photoSession.setPhotographerId(this.photographer.getPhotographerId());
        this.photoSessionService.insert(this.photoSession);

        PhotoData photoData = new PhotoData();
        URL imageUrl = new URL("http://test.b3-encare.senet.nl/images/logo-header.png");
        InputStream is;
        photoData.setData(IOUtils.toByteArray(imageUrl.openStream()));
        PhotoType photoType = PhotoType.GROUP;
        // plaatje.jpg moet echt bestaan om de tests te laten draaien
        this.photo = new Photo(this.photoSession, "c:\\Temp\\plaatje.jpg", 5F, photoType, photoData);
        this.photo.setActive(true);
//        this.photoService.insert(photo);

        this.product = new Product(1F, "Product description");
        product.setThumbnail(new PhotoData(new byte[]
        {
        }));
//        this.productService.insert(product);

        this.session = new MockHttpSession();
        this.session.setAttribute("loggedInPhotographer", this.photographer);
        this.session.setAttribute("loggedInUser", this.photographer.getUser());
        this.session.setAttribute("loginCode", "321321");
        this.session.setAttribute("firstRequest", true);

        this.invoice = new Invoice(new String("321321"), "Lars", "van der", "Sangen", "De dollard", "26", "5463RG", "Veghel", "lvandersangen@student.fontys.nl", "06-18524658787");
//        this.invoiceService.insert(invoice);

//        this.invoice = this.invoiceService.get(new Integer(this.invoice.getInvoiceId().toString()));
        this.invoiceLine = new InvoiceLine(this.invoice, this.photo, this.product, "Sepia", 2, 2.0F);
        this.invoiceLineService.insert(invoiceLine);
    }
    
    @After
    public void tearDown()
    {
        this.photographerService.remove(this.photographer);
        this.loginService.remove(this.user);
        this.invoiceLineService.remove(this.invoiceLine);
        this.invoiceService.remove(this.invoice);
        this.photoSessionService.remove(this.photoSession.getPhotoSessionId().intValue());
    }

    @Test
    public void testOverViewActionGetSuccess() throws Exception
    {
        this.mockMvc.perform(get("/order/overview/" + this.photoSession.getPhotoSessionId())
                .session(this.session)
                .accept("text/html"))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateShoppingCartLinePOST() throws Exception
    {

        Long photoId = this.photo.getPhotoId();
        String price = "1";
        Long productId = this.product.getProductId();
        String productDescription = "productDescription";
        String color = "Sepia";
        String amount = "1";

        ShoppingCartLine scl = new ShoppingCartLine(0, photoId, productId, productDescription, 0, color, 0);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        byte[] content = mapper.writeValueAsBytes(scl);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/order/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept("application/json")
                .param("photoId", photoId.toString())
                .param("productId", productId.toString())
                .param("price", price)
                .param("color", color)
                .param("amount", amount)
                .param("productDescription", productDescription)
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testRemoveShoppingCartLinePOST() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/order/remove")
                .contentType(MediaType.APPLICATION_JSON)
                .accept("application/json")
                .param("shoppingCartLine", "1")
                .session(this.session))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetShoppingCartLineGET() throws Exception
    {
        this.mockMvc.perform(get("/order/shoppingcart")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk())
                .andExpect(view().name("/order/shoppingcart"));
    }

    @Test
    public void testGetOverViewActionGET() throws Exception
    {
        long photoSessionId = this.photoSession.getPhotoSessionId();
        this.photoSession.setPortraitClients("Client1-12jn12uoin4,Client2-1509nwk2342k,Client3-23nkfjn349");
        this.photoSessionService.update(this.photoSession);
        
        String clientCode = this.photoSession.getClientCode("Client1");
        
        this.mockMvc.perform(get("/order/overview/" + photoSessionId + "/" + clientCode)
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk())
                .andExpect(view().name("order/overview"));
    }

    @Test
    public void testConfirmOrderGET() throws Exception
    {
        this.mockMvc.perform(get("/order/confirmOrder")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk())
                .andExpect(view().name("/order/confirmation"));
    }

    @Test
    public void testGetPreviewBlackwhiteGET() throws Exception
    {
        Long photoId = this.photo.getPhotoId();
        this.mockMvc.perform(get("/order/overview/getPreview/" + photoId + "/Blackwhite")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPreviewSepiaGET() throws Exception
    {
        String photoId = this.photo.getPhotoId().toString();
        this.mockMvc.perform(get("/order/overview/getPreview/" + photoId + "/Sepia")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPreviewThumbGET() throws Exception
    {
        Long photoId = this.photo.getPhotoId();
        this.mockMvc.perform(get("/order/overview/getPreviewThumb/" + photoId + "/Sepia")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPreviewThumbBlackwhiteGET() throws Exception
    {
        Long photoId = this.photo.getPhotoId();
        this.mockMvc.perform(get("/order/overview/getPreviewThumb/" + photoId + "/Blackwhite")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testPrintGET() throws Exception
    {
        Long invoiceId = this.invoice.getInvoiceId();
        this.mockMvc.perform(get("/order/print/" + invoiceId)
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testOverViewActionGetFail() throws Exception
    {
        this.mockMvc.perform(get("/order/overview/nan")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetProductActionGETSuccess() throws Exception
    {
        this.mockMvc.perform(get("/order/overview/getProduct/" + this.product.getProductId())
                .accept("text/html"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetProductActionGetFail() throws Exception
    {
        this.mockMvc.perform(get("/order/overview/getProduct/Nan")
                .accept("text/html"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPhotoActionGETSuccess() throws Exception
    {
        this.mockMvc.perform(get("/order/overview/getPhoto/" + this.photo.getPhotoId())
                .accept("text/html"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPhotoActionGetFail() throws Exception
    {
        this.mockMvc.perform(get("/order/overview/getPhoto/Nan")
                .accept("text/html")
                .session(this.session))
                .andExpect(status().isOk());
    }

    @Test
    public void testConfirmOrderActionPostSucces() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/order/confirmOrder")
                .accept("text/html")
                .param("firstname", "Henk")
                .param("insertion", "de")
                .param("surname", "Vries")
                .param("street", "Rachelsmolen")
                .param("housenumber", "1")
                .param("postalcode", "12345CC")
                .param("city", "Eindhoven")
                .param("email", "l.vandersangen@student.fontys.nl")
                .param("phonenumber", "0123456789")
                .session(this.session))
                .andExpect(status().isFound());
    }
    @Test
    public void testManagementEditActionGET() throws Exception
    {
        this.photographer.getUser().setUserType(UserType.EMPLOYEE);
        this.photographerService.update(this.photographer);
        
        String invoiceLineId = this.invoiceLine.getInvoiceLineId().toString();
        this.mockMvc.perform(get("/order/management/edit/" + invoiceLineId + "/done")
                .session(this.session))
                .andExpect(status().isFound());
    }

    @Test
    public void testDecreaseActionGet() throws Exception
    {
        this.photographer.getUser().setUserType(UserType.EMPLOYEE);
        this.photographerService.update(this.photographer);
        
        String invoiceLineId = this.invoiceLine.getInvoiceLineId().toString();
        this.mockMvc.perform(get("/order/management/decrease/" + invoiceLineId + "/done")
                .session(this.session))
                .andExpect(status().isFound());
    }


    @Test
    public void testGetProductGETFail() throws Exception
    {
        this.mockMvc.perform(get("/order/overview/NAN").accept("text/html")
                .session(session)
                .param("searchText", "zoeken"))
                .andExpect(status().isOk());
    }

}
