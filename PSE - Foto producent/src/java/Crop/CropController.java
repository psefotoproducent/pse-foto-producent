package Crop;

import Application.Model.FileMeta;
import Application.Model.StorageProperties;
import Application.Service.StorageService;
import Login.Entity.Photographer;
import Login.Service.PhotographerService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import com.itextpdf.text.pdf.qrcode.Mode;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import java.sql.SQLException;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.mock.web.MockMultipartFile;
import static java.lang.Math.toIntExact;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.util.StringUtils.isEmpty;
import org.springframework.web.bind.annotation.PathVariable;
import java.security.SecureRandom;
import java.math.BigInteger;

@Controller
public class CropController {

    FileMeta fileMeta = null;
    @Autowired
    private PhotoSessionService photoSessionService;
    StorageService storageService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ServletContext context;
    ;
 @Autowired
    PhotographerService pService;

    @RequestMapping(value = "/cropping", method = RequestMethod.GET)
    public String Crop() {

        return "crop/crop";
    }

    @RequestMapping(value = "/cropping/cropped/{cropId}", method = RequestMethod.POST)
    public String CropCr(@RequestParam("croppedImage") MultipartFile file, @PathVariable String cropId, ModelMap model, HttpSession session) throws IOException, ServletException, SQLException {

        BufferedImage src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
        File destination = new File("C:\\Temp/tmpPhoto.png");
        ImageIO.write(src, "PNG", destination);
        
        String price = photoService.get(Integer.parseInt(cropId)).getPrice().toString();
        String photoSession = photoService.get(Integer.parseInt(cropId)).getPhotoSession().getPhotoSessionId().toString();
        int photoType = photoService.get(Integer.parseInt(cropId)).getPhotoType().getValue();

        return "redirect:/cropping/upload/" + photoSession + "/" + price + "/" + photoType + "/" + cropId;

    }

    private SecureRandom random = new SecureRandom();

    public String nextSessionId() {
        return new BigInteger(130, random).toString(32);
    }

    @RequestMapping(value = "/cropping/upload/{photoS}/{price}/{photoType}/{cropId}", method = RequestMethod.GET)
    public String CropSave(@PathVariable String photoS, @PathVariable String price, @PathVariable String photoType, @PathVariable String cropId, ModelMap model, HttpSession session) throws IOException {
        PhotoSession pS = photoSessionService.get(Integer.parseInt(photoS));
        Path path = Paths.get("C:\\Temp/tmpPhoto.png");

        String name = "tmpPhoto.png";
        String originalFileName = "tmpPhoto.png";
        String contentType = "image/";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);

        String filePath = "C:/Temp/Photos/" + pS.getPhotoSessionId();
        this.storageService = new StorageService(new StorageProperties(filePath), this.context);
        this.storageService.init();

        PhotoSession photoSession = this.photoSessionService.get(Integer.parseInt(photoS));
        long foo = photoSessionService.get(Integer.parseInt(photoS)).getPhotographerId();
        int bar = toIntExact(foo);
        Photographer photographer = pService.get(bar);
        

        MultipartFile mpf;

        mpf = result;

        try {

            Photo photo = new Photo(photoSession);
            photo.setActive(true);
            PhotoData photoData = new PhotoData();
            String random = nextSessionId();
            fileMeta = new FileMeta();
            fileMeta.setFileName(random + mpf.getOriginalFilename());
            fileMeta.setFileSize(mpf.getSize() / 1024 + " Kb");
            fileMeta.setFileType(mpf.getContentType());

            fileMeta.setBytes(mpf.getBytes());
            byte[] img = this.storageService.store(fileMeta, true);

            photo.setFilename(this.storageService.load(random + mpf.getOriginalFilename()).toString());

            Float priceFloat = Float.parseFloat(price);
            if (priceFloat > 0) {
                photo.setPrice(priceFloat);
            } else if (photographer != null) {
                if (Integer.parseInt(photoType) == 1) {
                    photo.setPrice(photographer.getGroupPrice());
                } else {
                    photo.setPrice(photographer.getPortraitPrice());
                }
            } else {
                photo.setPrice(new Float(0));
            }

            if (Integer.parseInt(photoType) == 1) {
                photo.setPhotoType(PhotoType.GROUP);
            } else {
                photo.setPhotoType(PhotoType.PORTRAIT);
                String clientCode = photoService.get(Integer.parseInt(cropId)).getClientCode();
                String clientName = photoService.get(Integer.parseInt(cropId)).getClientName();
                
               photo.setClientCode(clientCode); 
                 photo.setClientName(clientName);
               
            }

            photoData.setData(img);
            photo.setThumbnail(photoData);
           
            this.photoService.insert(photo);

        } catch (IOException e) {

            e.printStackTrace();
        }

        return "redirect:/cropping";
    }

}
