package Management.Controller;

import Management.Service.ChartService;
import Management.Entity.DataBean;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/management")
public class ManagementController {
    
    @Autowired
    ChartService chartService;
    
    @RequestMapping(value = "/charts/getPaidUnpaidChart")
    @ResponseBody
    public DataBean getPaidUnpaidChart() {
        return chartService.getPaidUnpaidChart();
    }
    
    @RequestMapping(value = "/charts/getPaidUnpaidPriceChart")
    @ResponseBody
    public DataBean getPaidUnpaidPriceChart() {
        return chartService.getPaidUnpaidPriceChart();
    }
}
