package Management.Service;

import Management.Entity.DataBean;
import Management.Entity.SeriesBean;
import Order.Entity.Invoice;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class ChartService {
    
    @PersistenceContext
    private EntityManager em;
    
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

    public DataBean getPaidUnpaidChart() {
        List<SeriesBean> list = new ArrayList<>();
        List<String> categories = this.removeDuplicates(this.findAllInvoices());
        
        String[] categoriesArray = categories.toArray(new String[0]);
        double[] paidArray = new double[categoriesArray.length];
        double[] unpaidArray = new double[categoriesArray.length];
        
        for (int i = 0; i < categoriesArray.length; i++) {
            String stringDate = categoriesArray[i];
            Date date = null;
            
            try {
                date = dateFormatter.parse(stringDate);
            } catch (ParseException ex) {
                Logger.getLogger(ChartService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (date != null) {
                paidArray[i] = this.getSumInvoices(date, true);
                unpaidArray[i] = this.getSumInvoices(date, false);
            }
        }
        
        list.add(new SeriesBean("Paid", "#4ecdc4", paidArray));
        list.add(new SeriesBean("Unpaid", "#ed5565", unpaidArray));
        
        return new DataBean("paid-unpaid-chart", "", "Number of invoices", "Invoicedates", Arrays.asList(categoriesArray), list);
    }
    
    public DataBean getPaidUnpaidPriceChart() {
        List<SeriesBean> list = new ArrayList<>();
        List<String> categories = this.removeDuplicates(this.findAllInvoices());
        
        String[] categoriesArray = categories.toArray(new String[0]);
        double[] paidArray = new double[categoriesArray.length];
        double[] unpaidArray = new double[categoriesArray.length];
        
        for (int i = 0; i < categoriesArray.length; i++) {
            String stringDate = categoriesArray[i];
            Date date = null;
            
            try {
                date = dateFormatter.parse(stringDate);
            } catch (ParseException ex) {
                Logger.getLogger(ChartService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (date != null) {
                paidArray[i] = this.getSumInvoicePrice(date, true);
                unpaidArray[i] = this.getSumInvoicePrice(date, false);
            }
        }
        
        list.add(new SeriesBean("Paid", "#4ecdc4", paidArray));
        list.add(new SeriesBean("Unpaid", "#ed5565", unpaidArray));
        
        return new DataBean("paid-unpaid-price-chart", "", "Sum of invoice amount", "Invoicedates", Arrays.asList(categoriesArray), list);
    }
    
    private List<String> removeDuplicates(List<Invoice> invoices) {
        Map<String, Invoice> map = new HashMap<String, Invoice>();
        for (Invoice invoice : invoices) {
             String key = dateFormatter.format(invoice.getCreated());
             
             if (!map.containsKey(key)) {
                  map.put(key, invoice);
             }
        }
        
        return new ArrayList<String>(map.keySet());
    }
    
    @Transactional
    public Long getSumInvoices(Date date, boolean paid) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        Date dateEvening = c.getTime();
        
        Query q = this.em.createQuery("SELECT COUNT(*) FROM Order.Entity.Invoice I WHERE created BETWEEN :startCreated AND :endCreated AND payed = :paid");
        q.setParameter("startCreated", date);
        q.setParameter("endCreated", dateEvening);
        q.setParameter("paid", paid);
        
        return (Long)q.getSingleResult();
    }
    
    @Transactional
    public List<Invoice> findAllInvoices() {
        Query q = this.em.createQuery("FROM Order.Entity.Invoice", Invoice.class);
        return q.getResultList();
    }
    
    @Transactional
    public Double getSumInvoicePrice(Date date, boolean paid) {
        Object result = null;
        Double resultDouble = null;
        
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        Date dateEvening = c.getTime();
        
        Query q = this.em.createQuery("SELECT SUM(IL.price) FROM Order.Entity.InvoiceLine IL LEFT JOIN IL.invoice I WHERE I.created BETWEEN :startCreated AND :endCreated AND I.payed = :paid");
        q.setParameter("startCreated", date);
        q.setParameter("endCreated", dateEvening);
        q.setParameter("paid", paid);
        
        result = q.getSingleResult();
        if (result != null) {
            resultDouble = round((Double)result, 2);
        } else {
            resultDouble = new Double(0);
        }
        
        return resultDouble;
    }
    
    @Transactional
    public Double getSumInvoicePrice(boolean paid) {        
        Object result = null;
        Double resultDouble = null;
        
        Query q = this.em.createQuery("SELECT SUM(IL.price) FROM Order.Entity.InvoiceLine IL LEFT JOIN IL.invoice I WHERE I.payed = :paid");
        q.setParameter("paid", paid);
        
        result = q.getSingleResult();
        if (result != null) {
            resultDouble = round((Double)result, 2);
        } else {
            resultDouble = new Double(0);
        }
        
        return resultDouble;
    }
    
    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    @Transactional
    public List<Invoice> get10latestInvoices() {
        Query q = this.em.createQuery("FROM Order.Entity.Invoice I ORDER BY created DESC", Invoice.class);
        q.setMaxResults(10);
        return q.getResultList();
    }
    
    @Transactional
    public List<Invoice> get10longestStandingInvoices() {
        Query q = this.em.createQuery("FROM Order.Entity.Invoice I WHERE I.payed = 0 ORDER BY created ASC", Invoice.class);
        q.setMaxResults(10);
        return q.getResultList();
    }
    
}