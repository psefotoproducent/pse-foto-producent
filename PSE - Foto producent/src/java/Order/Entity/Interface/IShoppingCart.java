/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity.Interface;

import Order.Entity.ShoppingCartLine;
import java.util.ArrayList;

/**
 *
 * @author Niek
 */
public interface IShoppingCart 
{

    boolean addShoppingCartLine(ShoppingCartLine shoppingCartLine);

    boolean removeShoppingCartLine(ShoppingCartLine shoppingCartLine);

    float getTotalPrice();
    
    int getSize();
    
    ShoppingCartLine getShoppingCartLine(int shoppingCartLineId);
    
    ArrayList<ShoppingCartLine> getAllShoppingCartLines();

}
