/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import Photo.Entity.Photo;
import Product.Entity.Product;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Niek
 */
@Entity
@Table(name = "invoiceline")
@NamedQueries(value
        = {
            @NamedQuery(name = "invoiceline.namedquery", query = "FROM Order.Entity.InvoiceLine"),
            @NamedQuery(name = "invoiceline.findInvoiceLine", query = "FROM Order.Entity.InvoiceLine where invoiceLineId= :invoiceLineId")
        })
public class InvoiceLine implements Serializable {

    @Id
    @GeneratedValue
    protected Long invoiceLineId;

    // relation to Invoice which lines belong to
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "invoiceId")
    @Fetch(FetchMode.JOIN)
    protected Invoice invoice;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "photoId")
    @Fetch(FetchMode.JOIN)
    protected Photo photo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "productId")
    @Fetch(FetchMode.JOIN)
    protected Product product;

    protected String color;

    protected int amount;

    protected float price;

    protected int done;

    public InvoiceLine() {
    }

    public InvoiceLine(Invoice invoice, Photo photo, Product product, String color, int amount, float price) {
        this.invoice = invoice;
        this.photo = photo;
        this.product = product;
        this.color = color;
        this.amount = amount;
        this.price = price;
        this.done = 0;
    }

    public Long getInvoiceLineId() {
        return invoiceLineId;
    }

    public void setInvoiceLineId(Long invoiceLineId) {
        this.invoiceLineId = invoiceLineId;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhotoId(Photo photo) {
        this.photo = photo;
    }

    public Product getProduct() {
        return product;
    }

    public void setProductId(Product product) {
        this.product = product;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setDone(int i) {

        switch (i) {
            case 0:
                this.done = 1;
                break;
            case 1:
                this.done = 2;
                break;
            case 2:
                this.done = 3;
                break;
            default:
                this.done = 3;
                break;
        }

    }

    public void reverseDone(int i) {
   
        switch (i) {
            case 3:
                this.done = 2;
                break;
            case 2:
                this.done = 1;
                break;
            case 1:
                this.done = 0;
                break;            
            default:
                this.done = 0;
                break;
        }
       

    }

    public int setDoneFalse() {
        return this.done = 0;
    }

    public int getDone() {
        return this.done;
    }

    @Override
    public String toString() {
        return "\nInvoiceID:" + this.invoice.getInvoiceId()
                + "\nProductid: " + this.product.getProductId()
                + "\nPhotoId: " + this.photo.getPhotoId()
                + "\nPrice: " + this.price
                + "\nColor: " + this.color
                + "\nAmount: " + this.amount;
    }
}
