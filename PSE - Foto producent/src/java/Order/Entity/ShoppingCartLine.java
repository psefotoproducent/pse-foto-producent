/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

/**
 * SCL is in fact an invoiceline, open for changes. (no persistence needed)
 *
 * @author Niek
 */
public class ShoppingCartLine 
{    
    protected int shoppingCartLineId;

    protected Long photoId;

    protected Long productId;
    
    protected String productDescription;
    
    protected float price;

    protected String color;

    protected int amount;
    
    public ShoppingCartLine(){
        // empty constructor
    }

    public ShoppingCartLine(int shoppingCartLineId, Long photoId, Long productId, String productDescription, float price, String color, int amount)
    {
        this.shoppingCartLineId = shoppingCartLineId;
        this.photoId = photoId;
        this.productId = productId;
        this.productDescription = productDescription;        
        this.price = price;
        this.color = color;
        this.amount = amount;        
    }

    public int getShoppingCartLineId() {
        return shoppingCartLineId;
    }

    public void setShoppingCartLineId(int shoppingCartLineId) {
        this.shoppingCartLineId = shoppingCartLineId;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long photoId) {
        this.photoId = photoId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString(){
        return  "SCLID: " + this.shoppingCartLineId 
                + "\nProductid: " + this.productId 
                + "\nPhotoId: " + this.photoId 
                + "\nPrice: " + this.price 
                + "\nColor: " + this.color 
                + "\nAmount: " + this.amount;
    }
}
