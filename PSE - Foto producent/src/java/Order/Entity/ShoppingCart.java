/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

import java.io.Serializable;
import Order.Entity.Interface.IShoppingCart;
import java.util.ArrayList;

/**
 *
 * @author Niek
 */
public class ShoppingCart implements IShoppingCart, Serializable
{

    private final ArrayList<ShoppingCartLine> shoppingCartLines;
    
    private float totalPrice;

    public ShoppingCart()
    {
        this.shoppingCartLines = new ArrayList<>();
        totalPrice = 0;
    }

    @Override
    public boolean addShoppingCartLine(ShoppingCartLine shoppingCartLine)
    {
        if(this.shoppingCartLines.add(shoppingCartLine)){
            this.totalPrice += shoppingCartLine.getPrice();
            return true;
        }
        
        return false;
    }

    @Override
    public boolean removeShoppingCartLine(ShoppingCartLine shoppingCartLine)
    {
        
        if(this.shoppingCartLines.remove(shoppingCartLine)){
            this.totalPrice -= shoppingCartLine.getPrice();
            return true;
        }
        
        return false;
    }

    @Override
    public float getTotalPrice()
    {
        return this.totalPrice;
    }
    
    @Override
    public int getSize()
    {
        return this.shoppingCartLines.size();
    }
    
    @Override
    public ArrayList<ShoppingCartLine> getAllShoppingCartLines(){
        return this.shoppingCartLines;
    }

    @Override
    public ShoppingCartLine getShoppingCartLine(int shoppingCartLineId) {
        for(ShoppingCartLine scl : getAllShoppingCartLines()){
            if(scl.shoppingCartLineId == shoppingCartLineId){
                return scl;
            }
        }
        
        return null;
    }

}
