/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Entity;

/**
 *
 * @author Niek
 */
public enum Color
{
    COLOR(1),
    BLACKWHITE(2),
    SEPIA(3);

    private int value;

    Color(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}
