package Order.Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "invoice")
@NamedQueries(value =
{
    @NamedQuery(name = "invoice.findInvoice", query = "FROM Order.Entity.Invoice where invoiceId= :invoiceId")
})
public class Invoice implements Serializable
{

    @Id
    @GeneratedValue
    protected Long invoiceId;

    protected String loginCode;

    protected Date created;
    @PrePersist
    protected void onCreate() {
      created = new Date();
    }

    protected String firstName;

    protected String insertion;

    protected String surName;

    protected String street;

    protected String houseNumber;

    protected String postalCode;

    protected String city;

    protected String email;

    protected String phoneNumber;
    
    protected boolean payed;
    
    @OneToMany(targetEntity=InvoiceLine.class, mappedBy="invoice", cascade={CascadeType.ALL}, orphanRemoval=true, fetch = FetchType.EAGER)
    Set<InvoiceLine> invoiceLines = new HashSet<InvoiceLine>();

    public Invoice()
    {
        this.payed = false;
    }

    public Invoice(String loginCode, String firstName, String insertion,
            String surName, String street, String houseNumber, String postalCode,
            String city, String email, String phoneNumber)
    {
        this.loginCode = loginCode;
        this.firstName = firstName;
        this.insertion = insertion;
        this.surName = surName;
        this.street = street;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.email = email;
        this.phoneNumber = phoneNumber;
        
        this.payed = false;
    }

    public Long getInvoiceId()
    {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId)
    {
        this.invoiceId = invoiceId;
    }

    public String getLoginCode()
    {
        return loginCode;
    }

    public void setLoginCode(String loginCode)
    {
        this.loginCode = loginCode;
    }

    public Date getCreated()
    {
        return this.created;
    }

    public void setDate(Date date)
    {
        this.created = date;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getInsertion()
    {
        return insertion;
    }

    public void setInsertion(String insertion)
    {
        this.insertion = insertion;
    }

    public String getSurName()
    {
        return surName;
    }

    public void setSurName(String surName)
    {
        this.surName = surName;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getHouseNumber()
    {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber)
    {
        this.houseNumber = houseNumber;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public Set<InvoiceLine> getInvoiceLines() {
        return invoiceLines;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }    
    
    public String getAddress()
    {
        String result = "";

        if ((this.street != null) && (!"".equals(this.street)))
        {
            result = result + this.street;
        }

        if ((this.houseNumber != null) && (!"".equals(this.houseNumber)))
        {
            result = result + " " + this.houseNumber;
        }

        if ((this.postalCode != null) && (!"".equals(this.postalCode)))
        {
            result = result + ", " + this.postalCode;
        }

        if ((this.city != null) && (!"".equals(this.city)))
        {
            result = result + " " + this.city;
        }

        return result;
    }
    
}
