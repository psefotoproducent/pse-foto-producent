/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Controller;

import Application.Service.PrintService;
import Internationalization.controller.I18n;
import Login.Entity.RightValidator;
import Login.Entity.UserType;
import Order.Entity.Color;
import Order.Entity.Invoice;
import Order.Entity.InvoiceLine;
import Order.Entity.ShoppingCart;
import Order.Entity.ShoppingCartLine;
import Order.Service.InvoiceLineService;
import Order.Service.InvoicePrintService;
import Order.Service.InvoiceService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoType;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import Product.Entity.Product;
import Product.Service.ProductService;
import com.itextpdf.text.DocumentException;
import static com.itextpdf.text.pdf.PdfName.URL;
import com.itextpdf.xmp.XMPException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.hibernate.Hibernate;
import org.omg.CORBA.NameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * // todo 1.photopage (based on logincode) 2.productpage (including previews)
 * 3.shoppingcartpage 4.orderdetails/confirmationpage 5.ordersuccespage
 *
 * @author Niek
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    List<Color> colorList = Arrays.asList(Color.values());

    @Autowired
    protected InvoiceService invoiceService;

    @Autowired
    protected InvoiceLineService invoiceLineService;

    @Autowired
    protected InvoicePrintService invoicePrintService;

    @Autowired
    protected PhotoSessionService photoSessionService;

    @Autowired
    protected PhotoService photoService;

    @Autowired
    protected ProductService productService;

    protected PrintService printService = new PrintService();

    protected ShoppingCart shoppingCart = new ShoppingCart();

    protected int shoppingCartLineId = 0;

    /**
     * Get all data for logged in customer and fill view with data
     * This method is for viewing all photo's in photosession (no portraitphotos/multiple clients)
     *
     * @param photoSessionId
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "/overview/{photoSessionId}", method = RequestMethod.GET)
    public String orderOverviewAction(
            @PathVariable("photoSessionId") String photoSessionId, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.CLIENT);
        
        boolean firstRequest = (boolean) session.getAttribute("firstRequest");
        
        if (firstRequest) {
            shoppingCart = new ShoppingCart();
            session.setAttribute("firstRequest", false);
        }

        session.setAttribute("loggedInClient", "client");
        session.setAttribute("photoSessionId", photoSessionId);
        session.setAttribute("shoppingCart", this.shoppingCart);
        System.out.println("Client, photoSessionId and shoppingcart are set in session");
        
        try {
            model.put("shoppingcart", this.shoppingCart);
            model.put("photoSession", this.photoSessionService.get(Integer.parseInt(photoSessionId)));
            model.put("products", this.productService.findAll());            
            model.put("photos", this.photoSessionService.findAllPhotos(Long.parseLong(photoSessionId)));

            // Format enum color values from allCAPS to Firstlettercap
            List<String> colorListFormatted = new ArrayList<>();

            for (int i = 0; i < colorList.size(); i++) {
                String colorAllCaps = colorList.get(i).toString();
                String colorAllLowercase = colorAllCaps.toLowerCase();
                String colorFirstLetterCap = StringUtils.capitalize(colorAllLowercase);
                colorListFormatted.add(colorFirstLetterCap);
            }

            model.put("colors", colorListFormatted);
            model.addAttribute("shoppingcart", new ShoppingCartLine());
        } catch (NumberFormatException nfe) {
            // Error
        }
        return "order/overview";
    }
    
    /**
     * Get all data for logged in customer and fill view with data
     * Method specifically to only retrieve client-specific photos (portrait + group)
     *
     * @param photoSessionId
     * @param clientCode
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "/overview/{photoSessionId}/{clientCode}", method = RequestMethod.GET)
    public String clientSpecificOrderOverviewAction(
            @PathVariable("photoSessionId") String photoSessionId, 
            @PathVariable("clientCode") String clientCode, 
            HttpSession session, 
            ModelMap model) 
    {
        RightValidator.hardValidate(session, UserType.CLIENT);
        
        boolean firstRequest = (boolean) session.getAttribute("firstRequest");
        
        if (firstRequest) {
            shoppingCart = new ShoppingCart();
            session.setAttribute("firstRequest", false);
        }

        session.setAttribute("loggedInClient", clientCode);
        session.setAttribute("photoSessionId", photoSessionId);
        session.setAttribute("shoppingCart", this.shoppingCart);
        System.out.println("Client, photoSessionId and shoppingcart are set in session");
        try {
            model.put("shoppingcart", this.shoppingCart);
            model.put("photoSession", this.photoSessionService.get(Integer.parseInt(photoSessionId)));
            model.put("products", this.productService.findAll());
            
            // Get only photo's from photoSession which are groupphoto's or contain client            
            List<Photo> allPhotosFromPhotosession = this.photoSessionService.findAllPhotos(Long.parseLong(photoSessionId));
                        
            List<Photo> allPhotosSpecificForClient = new ArrayList<>();
            
            for(Photo p : allPhotosFromPhotosession){
                // Add all groupphoto's to list
                if(p.getPhotoType() == PhotoType.GROUP){
                    allPhotosSpecificForClient.add(p);
                }          
                
                // Add all portraitphoto's with clientcode to list
                if(p.getClientCode() != null && p.getClientCode().equals(clientCode)){
                    allPhotosSpecificForClient.add(p);
                }
            }
            
            model.put("photos", allPhotosSpecificForClient);

            // Format enum color values from allCAPS to Firstlettercap
            List<String> colorListFormatted = new ArrayList<>();

            for (int i = 0; i < colorList.size(); i++) {
                String colorAllCaps = colorList.get(i).toString();
                String colorAllLowercase = colorAllCaps.toLowerCase();
                String colorFirstLetterCap = StringUtils.capitalize(colorAllLowercase);
                colorListFormatted.add(colorFirstLetterCap);
            }

            model.put("colors", colorListFormatted);
            model.addAttribute("shoppingcart", new ShoppingCartLine());
        } catch (NumberFormatException nfe) {
            // Error
        }
        
        return "order/overview";
    }

    /**
     * Method to add shoppingcartline to shoppingcart via AJAX (overview.jsp)
     *
     * @param scl
     * @param model
     * @return
     */
    @RequestMapping(
            value = "/create",
            method = RequestMethod.POST,
            headers = {"Content-type=application/json"}
    )
    @ResponseBody
    public String createShoppingCartLine(@RequestBody ShoppingCartLine scl, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.CLIENT);

        // do something with shoppingcartline
        ShoppingCartLine scLine = new ShoppingCartLine(this.shoppingCartLineId, scl.getPhotoId(),
                scl.getProductId(), scl.getProductDescription(),
                scl.getPrice(), scl.getColor(), scl.getAmount());

        // calculate total of productprice and photoprice
        Product product = productService.get(scl.getProductId().intValue());
        Float productPrice = product.getPrice();

        Photo photo = photoService.get(scl.getPhotoId().intValue());
        Float photoPrice = photo.getPrice();

        Float totalPrice = (productPrice * scl.getAmount()) + (photoPrice * scl.getAmount());

        scLine.setPrice(totalPrice);

        if (shoppingCart.addShoppingCartLine(scLine)) {
            this.shoppingCartLineId++;
            System.out.println("SCL added succesfully");
            System.out.println("Size of shoppingcart: " + this.shoppingCart.getSize());
            shoppingCart.getAllShoppingCartLines().stream().forEach((s) -> {
                System.out.println(s.toString());
            });
            model.put("shoppingCart", shoppingCart);
            System.out.println("size of shoppingcart in order: " + shoppingCart.getSize());
            return String.valueOf(shoppingCart.getSize());
        }

        return "FAIL";
    }

    /**
     * Method to add shoppingcartline to shoppingcart via AJAX (overview.jsp)
     *
     * @param scl
     * @param model
     * @return
     */
    @RequestMapping(
            value = "/remove",
            method = RequestMethod.POST,
            headers = {"Content-type=application/json"}
    )
    @ResponseBody
    public String removeShoppingCartLine(@RequestBody ShoppingCartLine scl, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.CLIENT);

        // do something with shoppingcartline
        int sclId = scl.getShoppingCartLineId();

        System.out.println("Trying to remove ID: " + scl.getShoppingCartLineId());
        ShoppingCartLine lineToRemove = shoppingCart.getShoppingCartLine(sclId);

        if (shoppingCart.removeShoppingCartLine(lineToRemove)) {
            System.out.println("SCL removed succesfully");
            System.out.println("Size of shoppingcart after removeAction: " + this.shoppingCart.getSize());
            return String.valueOf(shoppingCart.getSize());
        }

        return "FAIL";
    }

    /**
     * GET Shoppingcart/confirmation page with all added shoppingcartlines After
     * filling in invoice-information, client can confirm order and create
     * invoice
     *
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "/shoppingcart", method = RequestMethod.GET)
    public String shoppingCartOverviewAction(HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.CLIENT);
        try {
            // put shoppingCart in model
            model.put("shoppingCart", this.shoppingCart.getAllShoppingCartLines());
            model.put("totalPrice", this.shoppingCart.getTotalPrice());
        } catch (NumberFormatException nfe) {
            // Error
        }
        return "/order/shoppingcart";
    }

    /**
     * Get confirmation form (name, billingadres etc.)
     *
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "/confirmOrder", method = RequestMethod.GET)
    public String createConfirmFormAction(HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.CLIENT);

        return "/order/confirmation";
    }

    /**
     * POST confirmation form (name, billingadres etc.) Convert shoppingCart
     * (temporary object) to InvoiceLines and Invoice to be stored in db
     *
     * @param firstname
     * @param insertion
     * @param surname
     * @param street
     * @param housenumber
     * @param postalcode
     * @param city
     * @param email
     * @param phonenumber
     * @param model
     * @param session
     * @return
     */
    @RequestMapping(value = "/confirmOrder", method = RequestMethod.POST)
    public String confirmOrderAction(@RequestParam String firstname, @RequestParam String insertion,
            @RequestParam String surname, @RequestParam String street, @RequestParam String housenumber,
            @RequestParam String postalcode, @RequestParam String city, @RequestParam String email,
            @RequestParam String phonenumber, ModelMap model, HttpSession session) {
        RightValidator.hardValidate(session, UserType.CLIENT);

        Invoice invoice;

        try {
            invoice = new Invoice();
            String loginCode = (String) session.getAttribute("loginCode");
            invoice.setLoginCode(loginCode); 
            invoice.setFirstName(firstname);
            invoice.setInsertion(insertion);
            invoice.setSurName(surname);
            invoice.setStreet(street);
            invoice.setHouseNumber(housenumber);
            invoice.setPostalCode(postalcode);
            invoice.setCity(city);
            invoice.setEmail(email);
            invoice.setPhoneNumber(phonenumber);

            this.invoiceService.insert(invoice);
            System.out.println("Invoice inserted in table");

            for (ShoppingCartLine scl : this.shoppingCart.getAllShoppingCartLines()) {

                InvoiceLine il = new InvoiceLine();

                il.setInvoice(invoice);
                il.setPhotoId(this.photoService.get(scl.getPhotoId().intValue()));
                il.setProductId(this.productService.get(scl.getProductId().intValue()));
                il.setColor(scl.getColor());
                il.setAmount(scl.getAmount());
                il.setPrice(scl.getPrice());

                // update is important, with persist ObjectDetachedException occurs on 'Invoice'
                this.invoiceLineService.update(il);
                System.out.println("InvoiceLine inserted in table");
            }
        } catch (NumberFormatException nfe) {
            System.out.println("nfeexception: " + nfe.getMessage());
            model.put("errorMsg", I18n.l("Logincode can't be parsed to int"));
            return "redirect:/order/confirmation";
        } catch (Exception ex) {
            model.put("errorMsg", I18n.l("Something went wrong creating the Invoice"));
            System.out.println("EXCEPTION:" + ex.getMessage());
            return "redirect:/order/confirmation";
        }
        
        String invoiceId = invoice.getInvoiceId().toString();
        return "redirect:/order/finishPayment/" + invoiceId;
    }
    
    @RequestMapping(value = "/checkPayment/{invoiceId}", method = RequestMethod.POST)
    public String checkPayment(@PathVariable String invoiceId, @RequestParam String action, HttpServletResponse response, HttpServletRequest request, ModelMap model)
            throws ServletException, IOException
    {
        Invoice invoice = this.invoiceService.get(Integer.parseInt(invoiceId));
        
        if( action.equals("OK") ){
           model.put("confirmMsg", I18n.l("&#10004; Your order has been confirmed!"));
           invoice.setPayed(true);
        }
        else if( action.equals("notOK") ){
           model.put("failMsg", I18n.l("&#10004; Your order has not been confirmed!"));
           invoice.setPayed(false);
        }
        
        model.put("invoice", invoice);
        model.put("totalprice", this.shoppingCart.getTotalPrice());
        
        return "/order/confirmed"; // this view needs to be page after payment
    }
    
    @RequestMapping(value = "/finishPayment/{invoiceId}", method = RequestMethod.GET)
    public String loadOmnikassa(@PathVariable String invoiceId, HttpSession session, ModelMap model)
    {
        Invoice invoice = this.invoiceService.get(Integer.parseInt(invoiceId));
        model.put("invoice", invoice);
        model.put("totalprice", this.shoppingCart.getTotalPrice());
        return "order/omnikassa";
    }

    //// METHODS TO FILL PAGE WITH RELEVANT PHOTO'S AND PRODUCTS ////
    /**
     * Get available product photo's
     *
     * @param productId
     * @param response
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/overview/getProduct/{productId}", method = RequestMethod.GET)
    public void getProduct(@PathVariable String productId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException {
        Product product;
        try {
            product = this.productService.get(Integer.parseInt(productId));
            Hibernate.initialize(product.getThumbnail());

            response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
            response.getOutputStream().write(product.getThumbnail().getData());
            response.getOutputStream().close();
        } catch (NumberFormatException exc) {
            // do nothing
        }
    }

    /**
     * Get customer photo's
     *
     * @param photoId
     * @param response
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/overview/getPhoto/{photoId}", method = RequestMethod.GET)
    public void getPhoto(@PathVariable String photoId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException {
        Product product;
        try {

            Photo photo = this.photoService.get(Integer.parseInt(photoId));
            Hibernate.initialize(photo.getThumbnail());

            response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
            response.getOutputStream().write(photo.getThumbnail().getData());
            response.getOutputStream().close();
        } catch (NumberFormatException exc) {
            // do nothing
        }

    }

    /**
     * Convert image to colorscheme based on selection
     *
     * @param photoId
     * @param color
     * @param response
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/overview/getPreview/{photoId}/{color}", method = RequestMethod.GET)
    public void getPreview(@PathVariable String photoId, @PathVariable String color, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException {
        Photo photo = this.photoService.get(Integer.parseInt(photoId));

        // Get converted image
        BufferedImage img = null;

        if ("Blackwhite".equals(color)) {
            img = this.printService.toGrayScale(photo.getFilename());
        } else if ("Sepia".equals(color)) {
            img = this.printService.toSepia(photo.getFilename());
        } else {
            img = this.printService.toNormal(photo.getFilename());
        }

        // Convert to byte[]
        byte[] imageInByte;
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(img, FilenameUtils.getExtension(photo.getFilename()), baos);
            baos.flush();
            imageInByte = baos.toByteArray();
        }

        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(imageInByte);
        response.getOutputStream().close();
    }

    /**
     * Convert image to colorscheme based on selection
     *
     * @param photoId
     * @param color
     * @param response
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/overview/getPreviewThumb/{photoId}/{color}", method = RequestMethod.GET)
    public void getPreviewThumb(@PathVariable String photoId, @PathVariable String color, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException {
        Photo photo = this.photoService.get(Integer.parseInt(photoId));

        // Get converted image
        BufferedImage img = null;

        if ("Blackwhite".equals(color)) {
            img = this.printService.toGrayScale(photo.getThumbnail());
        } else if ("Sepia".equals(color)) {
            img = this.printService.toSepia(photo.getThumbnail());
        } else {
            img = this.printService.toNormal(photo.getThumbnail());
        }

        // Convert to byte[]
        byte[] imageInByte;
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(img, FilenameUtils.getExtension(photo.getFilename()), baos);
            baos.flush();
            imageInByte = baos.toByteArray();
        }

        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(imageInByte);
        response.getOutputStream().close();
    }

    @RequestMapping(value = "/print/{invoiceId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getPreview(@PathVariable String invoiceId, HttpServletResponse response,
            HttpServletRequest request, HttpSession session) {
        RightValidator.hardValidate(session, UserType.CLIENT);

        String fileName = "";
        try {
            fileName = invoicePrintService.generatePDF(Integer.parseInt(invoiceId));
        } catch (DocumentException | IOException | XMPException | ParseException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Bestand is gegenereerd, dus we kunnen verder
        if (!"".equals(fileName)) {
            try {
                String outputName = String.format("Invoice_%05d.pdf", Integer.parseInt(invoiceId));

                InputStream stream = new FileInputStream(new File(fileName));
                byte[] contents = IOUtils.toByteArray(stream);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.parseMediaType("application/pdf"));
                headers.setContentDispositionFormData(outputName, outputName);
                headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
                ResponseEntity<byte[]> res = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);

                return res;
            } catch (IOException ex) {
                Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }

    @RequestMapping(value = "/management/productlist", method = RequestMethod.GET)
    public String ProductList(HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        model.put("invoiceList", this.invoiceLineService.findAll());

        return "order/productlist";
    }

    @RequestMapping(value = "/management/edit/{invoiceLineId}/{done}", method = RequestMethod.GET)
    public String ProductListEdit(@PathVariable String invoiceLineId, @PathVariable String done, HttpSession session, ModelMap model) {
RightValidator.hardValidate(session, UserType.EMPLOYEE);
        //in.setDone(Integer.parseInt(done));
        InvoiceLine in;
        try {
            in = this.invoiceLineService.get(Integer.parseInt(invoiceLineId));
            in.setAmount(in.getAmount());
            in.setDone(in.getDone());
            in.setInvoice(in.getInvoice());
            in.setInvoiceLineId(in.getInvoiceLineId());
            in.setPhotoId(in.getPhoto());
            in.setPrice(in.getPrice());
            in.setProductId(in.getProduct());
            in.setColor(in.getColor());
            
            
            this.invoiceLineService.update(in);
            
            
        } catch (NumberFormatException nfe) {
                System.out.println(nfe);
        }

        

        return "redirect:/order/management/productlist";
    }

    
     @RequestMapping(value = "/management/decrease/{invoiceLineId}/{done}", method = RequestMethod.GET)
    public String ProductListDecrease(@PathVariable String invoiceLineId, @PathVariable String done, HttpSession session, ModelMap model) {
RightValidator.hardValidate(session, UserType.EMPLOYEE);
        //in.setDone(Integer.parseInt(done));
        InvoiceLine in;
        try {
            in = this.invoiceLineService.get(Integer.parseInt(invoiceLineId));
            in.setAmount(in.getAmount());
            in.reverseDone(in.getDone());
            in.setInvoice(in.getInvoice());
            in.setInvoiceLineId(in.getInvoiceLineId());
            in.setPhotoId(in.getPhoto());
            in.setPrice(in.getPrice());
            in.setProductId(in.getProduct());
            in.setColor(in.getColor());
            
            
            this.invoiceLineService.update(in);
            
            
        } catch (NumberFormatException nfe) {
                System.out.println(nfe);
        }

        

        return "redirect:/order/management/productlist";
    }
    
    @RequestMapping(value = "/management/setPaid/{invoideId}", method = RequestMethod.POST)
    public void setPaid(@PathVariable String invoideId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException
    {
        Invoice invoice = this.invoiceService.get(Integer.parseInt(invoideId));
        invoice.setPayed(true);
        this.invoiceService.update(invoice);
    }
    
}


