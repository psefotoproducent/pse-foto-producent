/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Service;

import Order.Entity.Invoice;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Niek
 */
@Service
public class InvoiceService
{

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(Invoice invoice)
    {
        this.em.persist(invoice);
    }

    @Transactional
    public Invoice get(Integer id)
    {
        return this.em.find(Invoice.class, (long) id);
    }

    @Transactional
    public Invoice update(Invoice invoice)
    {
        return this.em.merge(invoice);
    }

    @Transactional
    public void remove(Invoice invoice)
    {
        this.em.remove(this.em.merge(invoice));
    }

    @Transactional
    public List<Invoice> findAll()
    {
        Query q = this.em.createNamedQuery("invoice.namedquery", Invoice.class);
        return q.getResultList();
    }
}
