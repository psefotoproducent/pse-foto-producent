package Order.Service;

import Application.Service.PrintService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.xmp.XMPException;

import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import Order.Entity.Invoice;
import Order.Entity.InvoiceLine;
import Photo.Entity.Photo;
import Product.Entity.Product;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// http://developers.itextpdf.com/examples/zugferd/creating-pdf-invoices-scratch-basic-profile#33-pdfinvoicesbasic.java
@Service
public class InvoicePrintService {
        
    @Autowired
    protected InvoiceService invoiceService;
    
    protected PrintService printService = new PrintService();
    
    public static final String DEST = "C:/Temp/Invoices/%05d.pdf";
    
    private final Font normalFont;
    private final Font boldFont;
    private final Font bigFont;
    
    public InvoicePrintService() {
        File file = new File("C:/Temp/Invoices/");
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        this.normalFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
        this.boldFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
        this.bigFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
    }
    
    public String generatePDF(Integer invoiceId) throws DocumentException, FileNotFoundException, XMPException, ParseException {
        String dest = String.format(DEST, invoiceId);
        
        Invoice invoice = this.invoiceService.get(invoiceId);
        Set<InvoiceLine> lines = invoice.getInvoiceLines();
        float total = 0;
        
        // Step 1 - Init document
        Document document = new Document();
        
        // Step 2 - Create writer
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
        
        // Step 3 - Create and open
        document.open();
        
        // Step 4 - Header
            // Step 4.1 - Right elements
            Paragraph p = new Paragraph("Fotoproducent", this.boldFont);
            p.setAlignment(Element.ALIGN_RIGHT);
            document.add(p);

            p = new Paragraph("Rachelsmolen 1", this.normalFont);
            p.setAlignment(Element.ALIGN_RIGHT);
            document.add(p);

            p = new Paragraph("5612 MA Eindhoven", this.normalFont);
            p.setAlignment(Element.ALIGN_RIGHT);
            document.add(p);

            p = new Paragraph(System.lineSeparator(), this.normalFont);
            p.setAlignment(Element.ALIGN_RIGHT);
            document.add(p);

            p = new Paragraph("info@fotoproducent.fontys.nl", this.normalFont);
            p.setAlignment(Element.ALIGN_RIGHT);
            document.add(p);

            p = new Paragraph("08850 74122", this.normalFont);
            p.setAlignment(Element.ALIGN_RIGHT);
            document.add(p);
            
            // Step 4.2 - Left elements
            p = new Paragraph(invoice.getFirstName() + " " + invoice.getInsertion() + " " + invoice.getSurName(), this.normalFont);
            p.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            
            p = new Paragraph(invoice.getStreet()+ " " + invoice.getHouseNumber(), this.normalFont);
            p.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            
            p = new Paragraph(invoice.getPostalCode()+ " " + invoice.getCity(), this.normalFont);
            p.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            
            p = new Paragraph(System.lineSeparator(), this.normalFont);
            p.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            
            p = new Paragraph(invoice.getEmail(), this.normalFont);
            p.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            
            p = new Paragraph(invoice.getPhoneNumber(), this.normalFont);
            p.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            
            p = new Paragraph(System.lineSeparator(), this.normalFont);
            document.add(p);
            document.add(p);
        
            // Step 4.3 - Invoice header (BIG)
            p = new Paragraph("Invoice " + convertDate(invoice.getCreated(), "dd-mm-yyyy") + String.format("%05d", invoice.getInvoiceId()), this.bigFont);
            p.setAlignment(Element.ALIGN_LEFT);
            document.add(p);
            
            p = new Paragraph(System.lineSeparator(), this.normalFont);
            document.add(p);
            document.add(p);
        
        // Step 5 - Items
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);
        table.setSpacingBefore(10);
        table.setSpacingAfter(10);
        table.setWidths(new int[]{3, 4, 2, 2, 2, 2});
        table.addCell(getCell("Photo", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("Product", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("Color", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("Amount", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("Subtotal", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("Total", Element.ALIGN_LEFT, this.boldFont));
        
        Product product;
        Photo photo;
        BufferedImage image;
        ByteArrayOutputStream baos;
        Image iTextImage = null;
        
        for (InvoiceLine line : lines) {
            product = line.getProduct();
            photo = line.getPhoto();
            image = null;

            if (null != line.getColor()) switch (line.getColor()) {
                case "Blackwhite":
                    image = this.printService.toGrayScale(photo.getThumbnail());
                    break;
                case "Sepia":
                    image = this.printService.toSepia(photo.getThumbnail());
                    break;
                default:
                    image = this.printService.toNormal(photo.getThumbnail());
                    break;
            }
            
            baos = new ByteArrayOutputStream();            
            try {
                ImageIO.write(image, "png", baos);
                iTextImage = Image.getInstance(baos.toByteArray());
            } catch (BadElementException ex) {
                Logger.getLogger(InvoicePrintService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(InvoicePrintService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            table.addCell(iTextImage);
            table.addCell(getCell(product.getDescription(), Element.ALIGN_LEFT, this.normalFont));
            table.addCell(getCell(line.getColor(), Element.ALIGN_LEFT, this.normalFont));
            table.addCell(getCell(String.valueOf(line.getAmount()), Element.ALIGN_LEFT, this.normalFont));
            table.addCell(getCell("€ " + String.format("%.02f", (line.getPrice() / line.getAmount())).replace(".", ","), Element.ALIGN_RIGHT, this.normalFont)); // TODO: LILIK DIE PRIJS ZO
            table.addCell(getCell("€ " + String.format("%.02f", (line.getPrice())).replace(".", ","), Element.ALIGN_RIGHT, this.normalFont));
            
            total = total + line.getPrice();
        }
        
        // Step 6 - Grand totals
        table.addCell(getCell("", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("", Element.ALIGN_LEFT, this.boldFont));
        table.addCell(getCell("Total", Element.ALIGN_RIGHT, this.boldFont));
        table.addCell(getCell("€ " + String.format("%.02f", total).replace(".", ","), Element.ALIGN_RIGHT, this.boldFont));
        
        // Step 7 - Add table
        document.add(table);
        
        // Step 8 - Save and close
        document.close();
        
        // Step 9 - Return document filename
        return dest;
    }
    
    public PdfPCell getCell(String value, int alignment, Font font) {
        PdfPCell cell = new PdfPCell();
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        Paragraph p = new Paragraph(value, font);
        p.setAlignment(alignment);
        cell.addElement(p);
        return cell;
    }
    
    public String convertDate(Date d, String newFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(newFormat);
        return sdf.format(d);
    }
    
}
