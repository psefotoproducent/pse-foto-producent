/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Order.Service;

import Order.Entity.InvoiceLine;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;


/**
 *
 * @author Niek
 */
@Service
public class InvoiceLineService
{

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(InvoiceLine invoiceLine)
    {
        this.em.persist(invoiceLine);
    }

    @Transactional
    public InvoiceLine get(Integer id)
    {
        return this.em.find(InvoiceLine.class, (long) id);
    }

    @Transactional
    public InvoiceLine update(InvoiceLine invoiceLine)
    {
        return this.em.merge(invoiceLine);
    }

    @Transactional
    public void remove(InvoiceLine invoiceLine)
    {
        this.em.remove(this.em.merge(invoiceLine));
    }

    @Transactional
    public List<InvoiceLine> findAll()
    {
        Query q = this.em.createNamedQuery("invoiceline.namedquery", InvoiceLine.class);
        return q.getResultList();
    }
}
