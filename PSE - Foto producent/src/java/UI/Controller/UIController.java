package UI.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Just some controller for route only to view ... lol why
 */
@Controller
public class UIController
{

    @RequestMapping(value = "/ui/styleguide", method = RequestMethod.GET)
    public String photoAction()
    {
        return "ui/styleguide";
    }

}
