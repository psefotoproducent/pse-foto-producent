package Application.Model;

public class StorageProperties
{

    private String location = "C:/Temp";

    public StorageProperties()
    {

    }

    public StorageProperties(String location)
    {
        this.location = location;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }
}
