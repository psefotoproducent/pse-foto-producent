package Application.Controller;

import Login.Entity.User;
import Login.Entity.UserType;
import Management.Service.ChartService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lars
 */
@Controller
public class ApplicationController
{

    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    ChartService chartService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loginAction(ModelMap model, HttpSession session)
    {        
            System.out.println("attribu");
        if(session.getAttribute("errorLogincode") != null){
            System.out.println("attribute filled");
            model.put("errorLogincode", session.getAttribute("errorLogincode"));
            session.removeAttribute("errorLogincode");
        }
        
        if(session.getAttribute("errorLogin") != null){
            System.out.println("attribute filed");
            model.put("errorLogin", session.getAttribute("errorLogin"));
            session.removeAttribute("errorLogin");
        }
        
        return "login/home";
    }
    
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String homeAction(ModelMap model, HttpSession session)
    {        
        User loggedInUser = (User)session.getAttribute("loggedInUser");
        if (loggedInUser.getUserType() == UserType.EMPLOYEE) {
            model.put("paidSum", this.chartService.getSumInvoicePrice(true));
            model.put("unpaidSum", this.chartService.getSumInvoicePrice(false));
            model.put("latestInvoices", this.chartService.get10latestInvoices());
            model.put("longestStandingInvoices", this.chartService.get10longestStandingInvoices());
            
            return "management/charts";
        } else {
            return "/main/home";
        }
    }

    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception
    {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
