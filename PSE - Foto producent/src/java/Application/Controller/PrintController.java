package Application.Controller;

import Application.Service.PrintService;
import Photo.Entity.Photo;
import Photo.Service.PhotoService;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PrintController {
    private PrintService service;
    
    @Autowired
    private PhotoService photoService;
    
    @RequestMapping(value = "/print/grayscale/{photoId}", method = RequestMethod.GET)
    public void printGrayScaleAction(@PathVariable String photoId, HttpServletResponse response, HttpServletRequest request) throws IOException
    {
        this.service = new PrintService();
        
        Photo photo = this.photoService.get(Integer.parseInt(photoId));
        BufferedImage buf = this.service.toGrayScale(photo.getFilename());
        
        byte[] imageInByte;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(buf, FilenameUtils.getExtension(photo.getFilename()), baos);
            baos.flush();
            imageInByte = baos.toByteArray();
        }
        
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(imageInByte);
        response.getOutputStream().close();
    }
    
    @RequestMapping(value = "/print/sepia/{photoId}", method = RequestMethod.GET)
    public void printSepiaAction(@PathVariable String photoId, HttpServletResponse response, HttpServletRequest request) throws IOException
    {
        this.service = new PrintService();
        
        Photo photo = this.photoService.get(Integer.parseInt(photoId));
        BufferedImage buf = this.service.toSepia(photo.getFilename());
        
        byte[] imageInByte;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(buf, FilenameUtils.getExtension(photo.getFilename()), baos);
            baos.flush();
            imageInByte = baos.toByteArray();
        }
        
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(imageInByte);
        response.getOutputStream().close();
    }
}
