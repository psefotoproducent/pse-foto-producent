package Application.Service;

import Application.Exception.StorageException;
import Application.Exception.StorageFileNotFoundException;
import Application.Model.FileMeta;
import Application.Model.StorageProperties;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;

@Service
public class StorageService
{

    private ServletContext context;

    private final Path rootLocation;

    public StorageService()
    {
        this.rootLocation = Paths.get("");
    }

    public StorageService(StorageProperties properties, ServletContext context)
    {
        this.rootLocation = Paths.get(properties.getLocation());
        this.context = context;
    }

    public void init()
    {
        if (!this.rootLocation.toFile().exists())
        {
            this.rootLocation.toFile().mkdirs();
        }
    }

    public byte[] store(FileMeta file, boolean withThumbnail)
    {
        try
        {
            if (file.getFileName() == null)
            {
                throw new StorageException("Failed to store empty file");
            }

            Path path = this.rootLocation.resolve(file.getFileName());
            FileCopyUtils.copy(file.getBytes(), new FileOutputStream(path.toFile()));

            if (withThumbnail)
            {
                return this.createThumbnail(path.toFile());
            }
            else
            {
                return null;
            }
        }
        catch (IOException e)
        {
            throw new StorageException("Failed to store file " + file.getFileName(), e);
        }
    }

    private byte[] createThumbnail(File file)
    {
        try
        {
            if (file == null)
            {
                throw new StorageException("Failed to store empty file");
            }

            BufferedImage img = ImageIO.read(file);
            BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY, Mode.AUTOMATIC, 500, 500, Scalr.OP_ANTIALIAS);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(thumbImg, FilenameUtils.getExtension(file.getName()), baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();

            return imageInByte;

//            String thumbFilename = FilenameUtils.removeExtension(file.getName());
//            thumbFilename = thumbFilename + "_thumb." + FilenameUtils.getExtension(file.getName());
//            
//            Path path = this.rootLocation.resolve(thumbFilename);
//            ImageIO.write(thumbImg, "jpg", path.toFile());
        }
        catch (IOException e)
        {
            throw new StorageException("Failed to store thumbnail " + file.getName(), e);
        }
    }

    public Stream<Path> loadAll()
    {
        try
        {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(path -> this.rootLocation.relativize(path));
        }
        catch (IOException e)
        {
            throw new StorageException("Failed to read stored files", e);
        }
    }

    public Path load(String filename)
    {
        return rootLocation.resolve(filename);
    }

    public Resource loadAsResource(String filename)
    {
        try
        {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable())
            {
                return resource;
            }
            else
            {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        }
        catch (MalformedURLException e)
        {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    public void deleteAll()
    {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    public void delete(String filename)
    {
        Path path = this.load(filename);
        FileSystemUtils.deleteRecursively(path.toFile());
    }
}
