package Application.Service;

import Photo.Entity.PhotoData;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class PrintService {
    
    public PrintService() {
        
    }
    
    public BufferedImage toNormal(PhotoData data) {
        InputStream in = new ByteArrayInputStream(data.getData());
        try {
            return ImageIO.read(in);
        } catch (IOException ex) {
            Logger.getLogger(PrintService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public BufferedImage toNormal(String filename) {
        try {
            return ImageIO.read(new File(filename));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return null;
    }
    
    private BufferedImage processGrayScale(BufferedImage master) {
        if (master != null) {
            int width = master.getWidth();
            int height = master.getHeight();

            //convert to sepia
            for(int y = 0; y < height; y++){
              for(int x = 0; x < width; x++){
                  Color c = new Color(master.getRGB(x, y));
                  int red = (int)(c.getRed() * 0.299);
                  int green = (int)(c.getGreen() * 0.587);
                  int blue = (int) (c.getBlue() * 0.114);
                  Color newColor = new Color(
                          red + green + blue, 
                          red + green + blue, 
                          red + green + blue
                  );
                  
                  master.setRGB(x, y, newColor.getRGB());
              }
            }
            
            return master;
        }
        
        return null;
    }
    
    public BufferedImage processSepia(BufferedImage master) {
        if (master != null) {
            int width = master.getWidth();
            int height = master.getHeight();

            //convert to sepia
            for(int y = 0; y < height; y++){
              for(int x = 0; x < width; x++){
                int p = master.getRGB(x,y);

                int a = (p>>24)&0xff;
                int r = (p>>16)&0xff;
                int g = (p>>8)&0xff;
                int b = p&0xff;

                //calculate tr, tg, tb
                int tr = (int)(0.393*r + 0.769*g + 0.189*b);
                int tg = (int)(0.349*r + 0.686*g + 0.168*b);
                int tb = (int)(0.272*r + 0.534*g + 0.131*b);

                //check condition
                if(tr > 255){
                  r = 255;
                }else{
                  r = tr;
                }

                if(tg > 255){
                  g = 255;
                }else{
                  g = tg;
                }

                if(tb > 255){
                  b = 255;
                }else{
                  b = tb;
                }

                //set new RGB value
                p = (a<<24) | (r<<16) | (g<<8) | b;

                master.setRGB(x, y, p);
              }
            }
            
            return master;
        }
        
        return null;
    }
    
    public BufferedImage toGrayScale(String filename) {
        BufferedImage master = this.toNormal(filename);
        return this.processGrayScale(master);
    }
    
    public BufferedImage toGrayScale(PhotoData data) {
        BufferedImage master = this.toNormal(data);
        return this.processGrayScale(master);
    }
    
    public BufferedImage toSepia(String filename) {
        BufferedImage master = this.toNormal(filename);
        return this.processSepia(master);
    }
    
    public BufferedImage toSepia(PhotoData data) {
        BufferedImage master = this.toNormal(data);
        return this.processSepia(master);
    }
}
