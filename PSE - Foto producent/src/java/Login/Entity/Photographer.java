/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Lars
 */
@Entity
@Table(name = "photographer")
@NamedQueries(value =
{
    @NamedQuery(name = "photographer.namedquery", query = "FROM Login.Entity.Photographer")
    ,
            @NamedQuery(name = "photographer.findPhotographer", query = "SELECT p FROM Photographer p WHERE userId= :userId")
})

public class Photographer implements Serializable
{

    @Id
    @GeneratedValue
    @Column(name = "photographerId")
    protected Long photographerId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    protected User user;

    @Column(name = "portraitPrice")
    protected Float portraitPrice;

    @Column(name = "groupPrice")
    protected Float groupPrice;

    // protected List<IPhotoSession> allPhotosessions; <- todo
    public Photographer()
    {
    }

    public Long getPhotographerId()
    {
        return photographerId;
    }

    public void setPhotographerId(Long photographerId)
    {
        this.photographerId = photographerId;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Float getPortraitPrice()
    {
        return portraitPrice;
    }

    public void setPortraitPrice(Float portraitPrice)
    {
        this.portraitPrice = portraitPrice;
    }

    public Float getGroupPrice()
    {
        return groupPrice;
    }

    public void setGroupPrice(Float groupPrice)
    {
        this.groupPrice = groupPrice;
    }

}
