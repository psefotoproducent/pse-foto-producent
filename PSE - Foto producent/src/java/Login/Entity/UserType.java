/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Entity;

/**
 *
 * @author Niek
 */
public enum UserType
{
    EMPLOYEE(0),
    PHOTOGRAPHER(1),
    CLIENT(2);

    private int value;

    UserType(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}
