/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Entity;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Niek
 */
@Entity
@Table(name = "user")
@NamedQueries(value =
{
    @NamedQuery(name = "user.findUser", query = "SELECT u FROM User u WHERE username= :username AND password= :password")
})

public class User implements Serializable
{

    @Id
    @GeneratedValue
    protected Long userId;

    protected String firstName;

    protected String insertion;

    protected String surName;

    protected String street;

    protected String houseNumber;

    protected String postalCode;

    protected String city;

    protected String phoneNumber;

    // unique is false for unit testing purposes
    @Column(unique = false)
    protected String username;

    protected String password;

    protected UserType userType;

    public User()
    {
    }

    public User(String firstName, String insertion, String surName, String street, String houseNumber,
            String postalCode, String city, String phoneNumber, String username, String password, UserType userType)
    {
        this.firstName = firstName;
        this.insertion = insertion;
        this.surName = surName;
        this.street = street;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.username = username;

        String encPassword;

        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1");

            byte[] bytes = md.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            encPassword = sb.toString();

            this.password = encPassword;
        }
        catch (NoSuchAlgorithmException e)
        {
        }
        this.userType = userType;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getInsertion()
    {
        return insertion;
    }

    public void setInsertion(String insertion)
    {
        this.insertion = insertion;
    }

    public String getSurName()
    {
        return surName;
    }

    public void setSurName(String surName)
    {
        this.surName = surName;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getHouseNumber()
    {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber)
    {
        this.houseNumber = houseNumber;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    // 'un'-hashing?
    public String getPassword()
    {
        return password;
    }

    // hashing?
    public void setPassword(String password)
    {
        String encPassword;

        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1");

            byte[] bytes = md.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            encPassword = sb.toString();

            this.password = encPassword;
        }
        catch (NoSuchAlgorithmException e)
        {
        }
    }

    public UserType getUserType()
    {
        return userType;
    }

    public void setUserType(UserType userType)
    {
        this.userType = userType;
    }

}
