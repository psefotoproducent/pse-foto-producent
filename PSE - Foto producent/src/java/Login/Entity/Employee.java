/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Lars
 */
@Entity
@Table(name = "employee")
@NamedQueries(value =
{
    @NamedQuery(name = "employee.namedquery", query = "FROM Login.Entity.Employee")
    ,
            @NamedQuery(name = "employee.findEmployee", query = "SELECT e FROM Employee e WHERE userId= :userId")
})

public class Employee implements Serializable
{

    @Id
    @GeneratedValue
    @Column(name = "employeeId")
    protected Long employeeId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    protected User user;

    public Employee()
    {
    }

    public Long getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this.employeeId = employeeId;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }
}
