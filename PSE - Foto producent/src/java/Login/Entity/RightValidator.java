/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Entity;

import java.security.AccessControlException;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wesley
 */
public class RightValidator {

    public static void hardValidate(HttpSession session, UserType userType) throws AccessControlException {
        if(RightValidator.validate(session, userType) == false) {
            throw new AccessControlException("User type " + userType + " required!");
        }
    }

    public static void hardValidate(HttpSession session, List<UserType> userTypes) throws AccessControlException {
        if(RightValidator.validate(session, userTypes) == false) {
            throw new AccessControlException("One of the user types " + userTypes + " required!");
        }
    }
    
    public static boolean validate(HttpSession session, UserType userType) {
        try {
            User user = (User) session.getAttribute("loggedInUser");
            
            return user.getUserType().equals(userType);
        } catch(NullPointerException npe) {
            return false;
        }
    }
    
    public static boolean validate(HttpSession session, List<UserType> userTypes) {
        try {
            User user = (User) session.getAttribute("loggedInUser");
            
            return userTypes.contains(user.getUserType());
        } catch(NullPointerException npe) {
            return false;
        }
    }
}
