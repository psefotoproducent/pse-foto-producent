package Login.Service;

import Login.Entity.Photographer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nick
 */
@Service
public class PhotographerService
{

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(Photographer photographer)
    {
        this.em.persist(photographer);
    }

    @Transactional
    public Photographer get(Integer id)
    {
        return this.em.find(Photographer.class, (long) id);
    }

    @Transactional
    public Photographer update(Photographer photographer)
    {
        return this.em.merge(photographer);
    }

    @Transactional
    public void remove(Photographer photographer)
    {
        this.em.remove(this.em.merge(photographer));
    }

    @Transactional
    public List<Photographer> findAll()
    {
        Query q = this.em.createNamedQuery("photographer.namedquery", Photographer.class);
        return q.getResultList();
    }
}
