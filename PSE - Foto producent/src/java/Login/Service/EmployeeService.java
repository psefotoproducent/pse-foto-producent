package Login.Service;

import Login.Entity.Employee;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Niek
 */
@Service
public class EmployeeService
{

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(Employee employee)
    {
        try
        {
            this.em.persist(employee);
        }
        catch (Exception e)
        {
        }
        
    }

    @Transactional
    public Employee get(Integer id)
    {
        return this.em.find(Employee.class, (long) id);
    }

    @Transactional
    public Employee update(Employee employee)
    {
        return this.em.merge(employee);
    }

    @Transactional
    public void remove(Employee employee)
    {
        this.em.remove(this.em.merge(employee));
    }

    @Transactional
    public List<Employee> findAll()
    {
        Query q = this.em.createNamedQuery("employee.namedquery", Employee.class);
        return q.getResultList();
    }
}
