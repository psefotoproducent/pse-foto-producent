/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Service;

import Login.Entity.Employee;
import Login.Entity.Photographer;
import Login.Entity.User;
import Photo.Entity.PhotoSession;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Niek
 */
@Service
public class LoginService
{

    @PersistenceContext
    private EntityManager em;

    public User authenticate(String username, String password)
    {
        String encPassword = getEncryptedPassword(password);

        try
        {
            User user = this.em.createNamedQuery("user.findUser", User.class)
                    .setParameter("username", username)
                    .setParameter("password", encPassword)
                    .getSingleResult();

            if (user.getUsername().equals(username))
            {
                return user;
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Transactional
    public Photographer getPhotographer(Long userId)
    {
        try
        {
            Photographer photographer = this.em.createNamedQuery("photographer.findPhotographer", Photographer.class)
                    .setParameter("userId", userId)
                    .getSingleResult();
            System.out.println("getPhotographerId: " + photographer.getPhotographerId());
            return photographer;
        }
        catch (Exception ex)
        {
            System.out.println("Photographer not found: " + ex.getMessage());
            return null;
        }
    }

    @Transactional
    public Employee getEmployee(Long userId)
    {
        try
        {
            Employee employee = this.em.createNamedQuery("employee.findEmployee", Employee.class)
                    .setParameter("userId", userId)
                    .getSingleResult();
            System.out.println("getPhotographerId: " + employee.getEmployeeId());
            return employee;
        }
        catch (Exception ex)
        {
            System.out.println("Employee not found: " + ex.getMessage());
            return null;
        }
    }

    @Transactional
    public PhotoSession getPhotosession(String loginCode)
    {
        try
        {
            PhotoSession photosession = this.em.createNamedQuery("photosession.findPhotosession", PhotoSession.class)
                    .setParameter("loginCode", loginCode)
                    .getSingleResult();
            return photosession;
        }
        catch (Exception ex)
        {
            System.out.println("Photosession not found: " + ex.getMessage());
            return null;
        }
    }

    @Transactional
    public void insert(User user)
    {
        // authenticate(user) doesn't work in this method for some reason...-_-
        String username = user.getUsername();
        String password = user.getPassword();

        try
        {
            User potentialUser = this.em.createNamedQuery("user.findUser", User.class)
                    .setParameter("username", username)
                    .setParameter("password", password)
                    .setMaxResults(1)
                    .getSingleResult();

            System.out.println("User already exists: " + potentialUser.getUsername());
        }
        catch (NoResultException ex)
        {
            // persist function is for NEW entities in database
            this.em.persist(user);
            System.out.println(user.getUsername() + " inserted");
        }
    }

    /**
     * Encrypt password using method, to check record in database (where password is encr.)
     *
     * @param password
     * @return
     */
    public String getEncryptedPassword(String password)
    {
        String encPassword;

        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1");

            byte[] bytes = md.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            encPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
            return null;
        }

        return encPassword;
    }

    @Transactional
    public void remove(User user)
    {
        this.em.remove(this.em.merge(user));
    }

}
