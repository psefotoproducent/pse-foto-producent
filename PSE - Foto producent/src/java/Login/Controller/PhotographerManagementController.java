/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Controller;

import Internationalization.controller.I18n;

import Login.Entity.Photographer;
import Login.Entity.RightValidator;
import Login.Entity.User;
import Login.Entity.UserType;

import Login.Service.PhotographerService;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nick
 */
@Controller
public class PhotographerManagementController {

    @Autowired
    private PhotographerService service;
    
    List<UserType> rightList;

    public PhotographerManagementController() {
        super();
        
        rightList = new ArrayList<>();
        rightList.add(UserType.PHOTOGRAPHER);
        rightList.add(UserType.EMPLOYEE);
    }

    @RequestMapping(value = "/photographer/management", method = RequestMethod.GET)
    public String photographerManagementAction(HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, rightList);

        model.put("photographerList", this.service.findAll());
        //model.put("message", I18n.l(""));

        return "photographer/list";
    }

    @RequestMapping(value = "/management/photographer/edit/{photographerId}", method = RequestMethod.GET)
    public String photographerManagementEditAction(@PathVariable String photographerId, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, rightList);

        try {
            model.put("photographer", this.service.get(Integer.parseInt(photographerId)));
        } catch (NumberFormatException nfe) {
            model.put("photographer", new Photographer());
        }

        model.put("message", I18n.l(""));

        return "photographer/edit";
    }

    @RequestMapping(value = "/management/photographer/edit/{photographerId}", method = RequestMethod.POST)
    public String photographerManagementEditAction(@PathVariable String photographerId, @RequestParam String username,
            @RequestParam String password, @RequestParam String firstname,
            @RequestParam String insertion, @RequestParam String surname,
            @RequestParam String housenumber, @RequestParam String postalcode,
            @RequestParam String city, @RequestParam String phonenumber,
            @RequestParam Float portraitPrice, @RequestParam Float groupPrice,
            HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, rightList);

        Photographer photographer;
        try {
            photographer = this.service.get(Integer.parseInt(photographerId));
        } catch (NumberFormatException nfe) {
            photographer = new Photographer();
        }
        try {
            User user = photographer.getUser();
            user.setUsername(username);
            user.setPassword(password);
            user.setFirstName(firstname);
            user.setInsertion(insertion);
            user.setSurName(surname);
            user.setHouseNumber(housenumber);
            user.setPostalCode(postalcode);
            user.setCity(city);
            user.setPhoneNumber(phonenumber);

            photographer.setUser(user);
            photographer.setGroupPrice(groupPrice);
            photographer.setPortraitPrice(portraitPrice);

            this.service.update(photographer);
        } catch (NumberFormatException nfe) {
            model.put("message", I18n.l("This photographer has not been saved. check the price, it needs to be a number."));
            model.put("photographer", photographer);

            return "photographer/edit";
        }

        model.put("message", I18n.l("This photographer has been saved"));

        return "redirect:/photographer/management";
    }

    @RequestMapping(value = "/management/photographer/add", method = RequestMethod.GET)
    public String photographerManagementaddAction(HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);

        model.put("photographer", new Login.Entity.Photographer());
        model.put("message", I18n.l(""));

        return "photographer/edit";
    }

    @RequestMapping(value = "/management/photographer/add", method = RequestMethod.POST)
    public String photographerManagementAddAction(@RequestParam String username,
            @RequestParam String password, @RequestParam String firstname,
            @RequestParam String insertion, @RequestParam String surname,
            @RequestParam String housenumber, @RequestParam String postalcode,
            @RequestParam String city, @RequestParam String phonenumber,
            @RequestParam Float portraitPrice, @RequestParam Float groupPrice,
            HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);

        Photographer photographer = new Photographer();
        User user = new User(firstname, insertion, surname, surname, housenumber,
                postalcode, city, phonenumber, username, password, UserType.PHOTOGRAPHER);

        photographer.setUser(user);
        photographer.setGroupPrice(groupPrice);
        photographer.setPortraitPrice(portraitPrice);

        try {
            this.service.insert(photographer);
        } catch (NumberFormatException nfe) {
            model.put("message", I18n.l("This photographer has not been saved. check the price, it needs to be a number."));
            model.put("photographer", photographer);

            return "photographer/edit";
        }

        model.put("message", I18n.l("This photographer has been saved"));

        return "redirect:/photographer/management";
    }

    @RequestMapping(value = "/photographer/management", method = RequestMethod.POST)
    public String photographerManagementDeleteAction(@RequestParam String photographerId, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, rightList);

        Photographer photographer = this.service.get(Integer.parseInt(photographerId));
        try {
            this.service.remove(photographer);
        } catch (NumberFormatException nfe) {
            model.put("employeeList", this.service.findAll());
            return "photographer/list";
        }

        model.put("message", I18n.l("The photographer has been removed."));

        return "redirect:/photographer/management";
    }

    //////Right validators toevoegen
    @RequestMapping(value = "/photographer/singlep/{photographerId}", method = RequestMethod.GET)
    public String photographerSingle(@PathVariable String photographerId, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);
        model.put("photographer", this.service.get(parseInt(photographerId)));
        //model.put("message", I18n.l(""));

        return "photographer/singlep";
    }

    @RequestMapping(value = "/management/photographerSingle/edit/{photographerId}", method = RequestMethod.GET)
    public String photographerManagementEditActionSingle(@PathVariable String photographerId, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);
        try {
            model.put("photographer", this.service.get(Integer.parseInt(photographerId)));
        } catch (NumberFormatException nfe) {
            model.put("photographer", new Photographer());
        }

        model.put("message", I18n.l(""));

        return "photographer/edit";
    }

    @RequestMapping(value = "/management/photographerSingle/edit/{photographerId}", method = RequestMethod.POST)
    public String photographerManagementEditActionSingle(@PathVariable String photographerId, @RequestParam String username,
            @RequestParam String password, @RequestParam String firstname,
            @RequestParam String insertion, @RequestParam String surname,
            @RequestParam String housenumber, @RequestParam String postalcode,
            @RequestParam String city, @RequestParam String phonenumber,
            @RequestParam Float portraitPrice, @RequestParam Float groupPrice,
            HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);
        Photographer photographer;
        try {
            photographer = this.service.get(Integer.parseInt(photographerId));
        } catch (NumberFormatException nfe) {
            photographer = new Photographer();
        }
        try {
            User user = photographer.getUser();
            user.setUsername(username);
            if(!password.isEmpty()){
               user.setPassword(password); 
            }      
            user.setFirstName(firstname);
            user.setInsertion(insertion);
            user.setSurName(surname);
            user.setHouseNumber(housenumber);
            user.setPostalCode(postalcode);
            user.setCity(city);
            user.setPhoneNumber(phonenumber);

            photographer.setUser(user);
            photographer.setGroupPrice(groupPrice);
            photographer.setPortraitPrice(portraitPrice);

            this.service.update(photographer);
        } catch (NumberFormatException nfe) {
            model.put("message", I18n.l("This photographer has not been saved. check the price, it needs to be a number."));
            model.put("photographer", photographer);

            return "photographer/edit";
        }

        model.put("message", I18n.l("This photographer has been saved"));

        return "redirect:/photographer/singlep/{photographerId}";
    }

}
