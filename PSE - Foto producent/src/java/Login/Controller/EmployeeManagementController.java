/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login.Controller;

import Internationalization.controller.I18n;
import Login.Entity.Employee;
import Login.Entity.RightValidator;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.EmployeeService;
import static java.lang.Integer.parseInt;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Lars
 */
@Controller
public class EmployeeManagementController {

    @Autowired
    private EmployeeService service;

    public EmployeeManagementController() {
        super();
    }

    @RequestMapping(value = "/employee/management", method = RequestMethod.GET)
    public String employeeManagementAction(HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        model.put("employeeList", this.service.findAll());
        //model.put("message", I18n.l(""));

        return "employee/list";
    }

    @RequestMapping(value = "/management/employee/edit/{employeeId}", method = RequestMethod.GET)
    public String employeeManagementEditAction(@PathVariable String employeeId, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        try {
            model.put("employee", this.service.get(Integer.parseInt(employeeId)));
        } catch (NumberFormatException nfe) {
            model.put("employee", new Employee());
        }

        model.put("message", I18n.l(""));

        return "employee/edit";
    }

    @RequestMapping(value = "/management/employee/edit/{employeeId}", method = RequestMethod.POST)
    public String employeeManagementEditAction(@PathVariable String employeeId, @RequestParam String username,
            @RequestParam String password, @RequestParam String firstname,
            @RequestParam String insertion, @RequestParam String surname,
            @RequestParam String housenumber, @RequestParam String postalcode,
            @RequestParam String city, @RequestParam String phonenumber,
            HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        Employee employee;
        try {
            employee = this.service.get(Integer.parseInt(employeeId));
        } catch (NumberFormatException nfe) {
            employee = new Employee();
        }
        try {
            User user = employee.getUser();
            user.setUsername(username);
            user.setPassword(password);
            user.setFirstName(firstname);
            user.setInsertion(insertion);
            user.setSurName(surname);
            user.setHouseNumber(housenumber);
            user.setPostalCode(postalcode);
            user.setCity(city);
            user.setPhoneNumber(phonenumber);

            employee.setUser(user);

            this.service.update(employee);
        } catch (NumberFormatException nfe) {
            model.put("message", I18n.l("This employee has not been saved. check the price, it needs to be a number."));
            model.put("employee", employee);

            return "employee/edit";
        }

        model.put("message", I18n.l("This employee has been saved"));

        return "redirect:/employee/management";
    }

    @RequestMapping(value = "/management/employee/add", method = RequestMethod.GET)
    public String employeeManagementaddAction(HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        model.put("employee", new Login.Entity.Employee());
        model.put("message", I18n.l(""));

        return "employee/edit";
    }

    @RequestMapping(value = "/management/employee/add", method = RequestMethod.POST)
    public String employeeManagementAddAction(@RequestParam String username,
            @RequestParam String password, @RequestParam String firstname,
            @RequestParam String insertion, @RequestParam String surname,
            @RequestParam String housenumber, @RequestParam String postalcode,
            @RequestParam String city, @RequestParam String phonenumber,
            HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        Employee employee = new Employee();
        User user = new User(firstname, insertion, surname, surname, housenumber,
                postalcode, city, phonenumber, username, password, UserType.EMPLOYEE);

        employee.setUser(user);

        try {
            this.service.insert(employee);
        } catch (NumberFormatException nfe) {
            model.put("message", I18n.l("This employee has not been saved. check the price, it needs to be a number."));
            model.put("employee", employee);

            return "employee/edit";
        }

        model.put("message", I18n.l("This employee has been saved"));

        return "redirect:/employee/management";
    }

    @RequestMapping(value = "/employee/management", method = RequestMethod.POST)
    public String employeeManagementDeleteAction(@RequestParam String employeeId, HttpSession session, ModelMap model) {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        Employee employee = this.service.get(Integer.parseInt(employeeId));
        try {
            this.service.remove(employee);
        } catch (NumberFormatException nfe) {
            model.put("employeeList", this.service.findAll());
            return "employee/list";
        }

        model.put("message", I18n.l("The employee has been removed."));

        return "redirect:/employee/management";
    }

    ////Right validators toevoegen
    @RequestMapping(value = "/employee/singlee/{employeeId}", method = RequestMethod.GET)
    public String EmployeeSingle(@PathVariable String employeeId, HttpSession session, ModelMap model) {
           RightValidator.hardValidate(session, UserType.EMPLOYEE);

        model.put("employee", this.service.get(parseInt(employeeId)));
        //model.put("message", I18n.l(""));

        return "employee/singlee";
    }

    @RequestMapping(value = "/employee/single/edit/{employeeId}", method = RequestMethod.GET)
    public String EmployeeEditActionSingle(@PathVariable String employeeId, HttpSession session, ModelMap model) {
           RightValidator.hardValidate(session, UserType.EMPLOYEE);
        try {
            model.put("employee", this.service.get(Integer.parseInt(employeeId)));
        } catch (NumberFormatException nfe) {
            model.put("employee", new Employee());
        }

        model.put("message", I18n.l(""));

        return "employee/edit";
    }

    @RequestMapping(value = "/employee/single/edit/{employeeId}", method = RequestMethod.POST)
    public String EmployeeEditActionSingle(@PathVariable String employeeId, @RequestParam String username,
            @RequestParam String password, @RequestParam String firstname,
            @RequestParam String insertion, @RequestParam String surname,
            @RequestParam String housenumber, @RequestParam String postalcode,
            @RequestParam String city, @RequestParam String phonenumber, HttpSession session,
            ModelMap model) {
           RightValidator.hardValidate(session, UserType.EMPLOYEE);
        Employee employee;
        try {
            employee = this.service.get(Integer.parseInt(employeeId));
        } catch (NumberFormatException nfe) {
            employee = new Employee();
        }
        try {
            User user = employee.getUser();
            user.setUsername(username);
            user.setPassword(password);
            user.setFirstName(firstname);
            user.setInsertion(insertion);
            user.setSurName(surname);
            user.setHouseNumber(housenumber);
            user.setPostalCode(postalcode);
            user.setCity(city);
            user.setPhoneNumber(phonenumber);

            employee.setUser(user);

            this.service.update(employee);
        } catch (NumberFormatException nfe) {
            model.put("message", I18n.l("This employee has not been saved. check the price, it needs to be a number."));
            model.put("employee", employee);

            return "employee/edit";
        }

        model.put("message", I18n.l("This employee has been saved"));

        return "redirect:/employee/singlee/{employeeId}";
    }

}
