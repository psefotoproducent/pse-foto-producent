package Login.Controller;

import Login.Entity.Employee;
import Login.Entity.Photographer;
import Login.Entity.RightValidator;
import Login.Entity.User;
import Login.Entity.UserType;
import Login.Service.LoginService;
import Photo.Entity.Photo;
import Photo.Entity.PhotoSession;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

/**
 * homepage login
 *
 * @author Niek
 */
@Controller
public class LoginController
{

    @Autowired
    protected LoginService loginService;
    
    @Autowired
    protected PhotoSessionService photoSessionService;
    
    
    @Autowired
    private PhotoService photoService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginSubmit(  @RequestParam("username") String username, 
                                @RequestParam("password") String password,
                                HttpSession session,
                                ModelMap model){
        
        User validUser = this.loginService.authenticate(username, password);
        
        
        if (validUser != null)
        {
            session.setAttribute("loggedInUser", validUser);
            System.out.println("User is set in session");
            
            Long userId = validUser.getUserId();
            UserType userType = validUser.getUserType();
            
            switch (userType) {
                case EMPLOYEE:
                    Employee employee = this.loginService.getEmployee(userId);
                    session.setAttribute("loggedInEmployee", employee);
                    System.out.println("Employee is set in session");
                    break;
                case PHOTOGRAPHER:
                    Photographer photographer = this.loginService.getPhotographer(userId);
                    session.setAttribute("loggedInPhotographer", photographer);
                    System.out.println("Photographer is set in session");
                    break;
                default:
                    System.out.println("No photographer or employee was set in session");
                    break;
            }
            
            // return the view-name/path
            return "redirect:/home";
        } else {
            String errormsg = "Invalid username/password combination";
            session.setAttribute("errorLogin", errormsg);
            return "redirect:/";
        }
    }
    
    @RequestMapping(value = "/loginClient", method = RequestMethod.POST)
    public String loginCodeSubmit(@RequestParam String loginCode,
                                HttpSession session,
                                ModelMap model){
        
        String photoSessionId = null;
        String clientCode = null;
        boolean clientSpecificSession = false;
        PhotoSession photoSession;
        
        if(loginCode.contains("-")){                
                String[] photoSessionAndClient = loginCode.split("-");
                photoSessionId = photoSessionAndClient[0];
                clientCode = photoSessionAndClient[1];
                clientSpecificSession = true;
        } else {
            // if normal logincode is entered, but photosession contains portraits, show message to use clientcode instead
            photoSessionId = loginCode;
            photoSession = this.loginService.getPhotosession(loginCode);
            if(photoSession != null){
                if(photoSession.getPortraitClients() != null){
                    String errormsg = "ClientCode required for this photosession";
                    session.setAttribute("errorLogincode", errormsg);
                    return "redirect:/";          
                }
            }
        }
        
        photoSession = this.loginService.getPhotosession(photoSessionId);      
        
        if (photoSession != null)
        {
            User client = new User();
            client.setUsername(loginCode);
            client.setUserType(UserType.CLIENT);
            session.setAttribute("loggedInUser", client);
            session.setAttribute("loginCode", photoSessionId);
            
            System.out.println("logincode set in session");
            model.put("userType", UserType.CLIENT);
            
            photoSessionId = Long.toString(photoSession.getPhotoSessionId());
            
            session.setAttribute("clientSpecificSession", clientSpecificSession);
            session.setAttribute("firstRequest", true);
            session.setAttribute("photoSessionId", photoSessionId);
            session.setAttribute("clientCode", clientCode);
            
            // Check if logincode is for portraitClient, if true, get only group and specific portraitphoto's
            if(clientSpecificSession){
                // return orderpage with only client-specific photos (group + portrait)
                return "redirect:order/overview/" + photoSessionId + "/" + clientCode;
            }
            
            // if no client-specific code was entered, return complete photosession in orderpage
            return "redirect:order/overview/" + photoSessionId;            
            
        } else {
            String errormsg = "Invalid logincode";
            session.setAttribute("errorLogincode", errormsg);
            return "redirect:/";
        }
    }
    
    @RequestMapping(value = "order/getPicture/{photoId}", method = RequestMethod.GET)
    public void getPictue(@PathVariable String photoId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException
    {
        /** @todo check the validation, only client? add: RightValidator.hardValidate(session, UserType.CLIENT); */
        Photo photo = this.photoService.get(Integer.parseInt(photoId));
        Hibernate.initialize(photo.getThumbnail());

        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(photo.getThumbnail().getData());
        response.getOutputStream().close();
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(SessionStatus status, HttpSession session) {
        ArrayList userTypes = new ArrayList<UserType>();
        
        userTypes.add(UserType.EMPLOYEE);
        userTypes.add(UserType.PHOTOGRAPHER);
        userTypes.add(UserType.CLIENT);
        
        RightValidator.hardValidate(session, userTypes);
        
        // order session to change status and delete sessionAttributes
        status.isComplete();
        session.invalidate();
        return "redirect:/";
    }       
}
