/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Product.Service;

import Product.Entity.Product;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wesley
 */
@Service
public class ProductService
{

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(Product product)
    {
        // insert into database
        // persist function is for NEW entities in database
        this.em.persist(product);
    }

    @Transactional
    public Product get(Integer id)
    {
        // this gets the entity from the database and returns it
        return this.em.find(Product.class, (long) id);
    }

    @Transactional
    public Product update(Product product)
    {
        // this updates the ExampleEntity in within the database
        return this.em.merge(product);
    }

    @Transactional
    public void remove(Integer id)
    {
        Product product = this.em.find(Product.class, (long) id);
        product.delete();

        // this updates the product in within the database
        this.update(product);
    }

    @Transactional
    public List<Product> findAll()
    {
        Query q = this.em.createNamedQuery("product.namedquery", Product.class);
        return q.getResultList();
    }
}
