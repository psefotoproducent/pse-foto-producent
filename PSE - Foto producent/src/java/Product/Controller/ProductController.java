/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Product.Controller;

import Product.Service.ProductService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Wesley
 */
@Controller
@RequestMapping("/product")
public class ProductController
{

    @PersistenceContext
    protected EntityManager em;

    @Autowired
    protected ProductService service;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String productAction(ModelMap model)
    {
        model.put("productList", this.service.findAll());

        return "product/overview";
    }

}
