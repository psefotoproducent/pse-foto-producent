/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Product.Controller;

import Application.Model.FileMeta;
import Application.Model.StorageProperties;
import Application.Service.StorageService;
import Product.Entity.Product;
import Product.Service.ProductService;
import Internationalization.controller.I18n;
import Login.Entity.RightValidator;
import Login.Entity.UserType;
import Photo.Entity.PhotoData;
import java.io.IOException;
import java.util.Iterator;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author Wesley
 */
@Controller
@RequestMapping("/management/product")
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public final class ProductManagementController extends RuntimeException 
{

    FileMeta fileMeta = null;
    StorageService storageService;
    
    @Autowired
    private ProductService service;
    
    @Autowired
    private ServletContext context;

    public ProductManagementController()
    {
        super();
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String productManagementAction(HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        model.put("productList", this.service.findAll());
        //model.put("message", I18n.l(""));

        return "product/list";
    }

    @RequestMapping(value = "/edit/{productId}", method = RequestMethod.GET)
    public String productManagementEditAction(@PathVariable String productId, HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        try
        {
            model.put("product", this.service.get(Integer.parseInt(productId)));
        }
        catch (NumberFormatException nfe)
        {
            model.put("product", new Product());
        }

        model.put("message", I18n.l(""));

        return "product/edit";
    }

    @RequestMapping(value = "/edit/{productId}", method = RequestMethod.POST)
    public String productManagementEditAction(@PathVariable String productId, 
            @RequestParam String description, @RequestParam String price, 
            MultipartHttpServletRequest request, HttpServletResponse response, 
            HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        Product product;
        try
        {
            product = this.service.get(Integer.parseInt(productId));
        }
        catch (NumberFormatException nfe)
        {
            product = new Product();
        }
        try
        {
            product.setDescription(description);
            product.setPrice(Float.parseFloat(price));
            //this.service.update(product);
            
            this.uploadPicture(product, request);
        }
        catch (NumberFormatException nfe)
        {
            model.put("message", I18n.l("This product has not been saved. check the price, it needs to be a number."));
            model.put("product", product);

            return "product/edit";
        }

        model.put("message", I18n.l("This product has been saved"));

        return "redirect:/management/product";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String productManagementaddAction(HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        
        model.put("product", new Product());

        return "product/edit";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String productManagementAddAction(@RequestParam String description, 
            @RequestParam String price, MultipartHttpServletRequest request, 
            HttpServletResponse response, HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        Product product = new Product((float) 0, description);

        try
        {
            product.setPrice(Float.parseFloat(price));
            this.service.insert(product);
        }
        catch (NumberFormatException nfe)
        {
            model.put("message", I18n.l("This product has not been saved. check the price, it needs to be a number."));
            model.put("product", product);

            return "product/edit";
        }
        
        // Uplodad picture
        this.uploadPicture(product, request);

        model.put("message", I18n.l("This product has been saved"));

        return "redirect:/management/product";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String productManagementDeleteAction(@RequestParam String productId, HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        try
        {
            this.service.remove(
                    Integer.parseInt(productId)
            );
        }
        catch (NumberFormatException nfe)
        {
            model.put("productList", this.service.findAll());
            return "product/list";
        }

        model.put("message", I18n.l("The product has been removed."));

        return "redirect:/management/product";
    }
    
    private void uploadPicture(Product product, MultipartHttpServletRequest request) 
    {
        String filePath = "C:/Temp/Products/" + product.getProductId();
        this.storageService = new StorageService(new StorageProperties(filePath), this.context);
        this.storageService.init();

        //1. build an iterator
        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = null;
        
        //2. get each file
        while (itr.hasNext())
        {
            //2.1 get next MultipartFile
            mpf = request.getFile(itr.next());

            if (!mpf.getContentType().startsWith("image/"))
            {
                continue;
            }

            //2.3 create new fileMeta
            fileMeta = new FileMeta();
            fileMeta.setFileName(mpf.getOriginalFilename());
            fileMeta.setFileSize(mpf.getSize() / 1024 + " Kb");
            fileMeta.setFileType(mpf.getContentType());

            try {
                PhotoData photoData = new PhotoData();

                fileMeta.setBytes(mpf.getBytes());
                byte[] img = this.storageService.store(fileMeta, true);

                product.setFilename(this.storageService.load(mpf.getOriginalFilename()).toString());
                photoData.setData(img);
                product.setThumbnail(photoData);

                this.service.update(product);
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    
    @RequestMapping(value = "/getPicture/{productId}", method = RequestMethod.GET)
    public void getPictue(@PathVariable String productId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException
    {
        Product product = this.service.get(Integer.parseInt(productId));
        Hibernate.initialize(product.getThumbnail());

        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(product.getThumbnail().getData());
        response.getOutputStream().close();
    }
}
