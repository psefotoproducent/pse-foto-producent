/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Product.Entity;

import Photo.Entity.Photo;
import Photo.Entity.PhotoData;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Wesley
 */
@Entity
@Table(name = "product")
@NamedQuery(name = "product.namedquery", query = "FROM Product.Entity.Product WHERE deleted = 0")
public class Product implements Serializable
{

    @Id
    @GeneratedValue
    @Column(name = "productId")
    protected Long productId;

    @Column(name = "filename")
    protected String filename;

    @Column(name = "price")
    protected Float price;

    @Column(name = "description")
    protected String description;

    @Column(name = "deleted")
    protected int deleted;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "photoDataId")
    @Fetch(FetchMode.JOIN)
    protected PhotoData thumbnail;

    public Product()
    {
    }

    public Product(Float price, String description)
    {
        this.price = price;
        this.description = description;
    }

    public Long getProductId()
    {
        return productId;
    }

    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public Float getPrice()
    {
        return price;
    }

    public void setPrice(Float price)
    {
        this.price = price;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getDeleted()
    {
        return deleted;
    }

    public void setDeleted(int deleted)
    {
        this.deleted = deleted;
    }
    
    public PhotoData getThumbnail()
    {
        return thumbnail;
    }

    public void setThumbnail(PhotoData thumbnail)
    {
        this.thumbnail = thumbnail;
    }
    

    public void delete()
    {
        this.deleted = 1;
    }

    public String getPrintPrice()
    {
        return String.format("%.02f", this.price).replace(".", ",");
    }
}
