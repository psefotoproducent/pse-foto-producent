package Internationalization.controller;

import Internationalization.Entity.Internationalization;
import Internationalization.Service.InternationalizationService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import javax.annotation.Resource;
import javax.xml.ws.Service;
import javax.xml.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This class is used for internationalization (i18n for short)
 *
 * @author Wesley Verheijen
 */
@Controller
//@RequestMapping("/i18n")
public class I18n
{

    @Resource(name = "InternationalizationService")
    //private Service service;
    private InternationalizationService service;

    private static InternationalizationService staticService;

    private static String mainLanguage = "nl";
    private static List<Internationalization> translations;
    //private static String pathToLangFiles = I18n.class.getResource("");

    private String message;
    private String translation;

    public I18n()
    {
    }

    public I18n(String message)
    {
        this.message = message;
        //this.setTranslation();
    }

    private void setTranslation()
    {
        if (service == null)
        {
            //QName name;
            //name = new QName("InternationalizationService");
            //service = (Service.create(name));
            service = new InternationalizationService();
            service.init();
        }

        System.out.println(service);

        if (translations == null)
        {
            translations = new ArrayList<Internationalization>();
            //translations = service.findAll();//new ArrayList<Internationalization>(service);
            //translations = (new InternationalizationService()).findAll();//new ArrayList<Internationalization>(service);
        }

        Stream filter = translations.stream().filter((translation)
                -> translation.getSource().equals(message) && translation.getLanguage().equals(I18n.mainLanguage)
        );

        if (filter.count() == 0)
        {
            service.insert(new Internationalization(message, message, I18n.mainLanguage));
        }
        else
        {
            translation = ((Internationalization) filter.findFirst().get()).getTranslation();
        }
    }

    public static String l(String message)
    {
        return (new I18n(message)).toString();
    }

//    @RequestMapping(value = "/{message}")
    public String i18nAction(@PathVariable String message, ModelMap model)
    {
        this.message = message;
        this.setTranslation();

        model.put("message", toString());

        return "i18n/message";
    }

//    @RequestMapping(value = "/init")
    public String i18nInitAction(ModelMap model)
    {
        I18n.staticService = service;
        return "";
    }

    @Override
    public String toString()
    {

        System.out.println(message);
//        if(I18n.translations.containsKey(message)) {
//            return (String) I18n.translations.get(message);
//        }
        return (translation != null ? translation : message);
    }
}
