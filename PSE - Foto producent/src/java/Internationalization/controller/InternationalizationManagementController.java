/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Internationalization.controller;

import Internationalization.Entity.Internationalization;
import Internationalization.Entity.SaveTranslation;
import Internationalization.Service.InternationalizationService;
import Login.Entity.RightValidator;
import Login.Entity.UserType;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This class is used for internationalization settings
 *
 * @author Wesley Verheijen
 */
@Controller
@RequestMapping("/i18n-management")
public class InternationalizationManagementController {
    
    @Autowired
    private InternationalizationService service;

    public InternationalizationManagementController()
    {
    }
    
    @RequestMapping(
            value = "",
            method = RequestMethod.GET
    )
    public String listAction(HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        model.put("messageList", this.service.findAll());
        
        return "i18n/management";
    }
    
    @RequestMapping(
            value = "/save",
            headers = {"Content-type=application/json"},
            method = RequestMethod.POST
    )
    @ResponseBody
    public String saveAction(@RequestBody SaveTranslation translation, HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        
        Internationalization modify = service.get(translation.getId());
        modify.setTranslation(translation.getTranslation());
        service.update(modify);
        
        InternationalizationController.truncateTranslations();
        
        return "{\"result\": \"ok\"}";
    }
    
    @RequestMapping(
            value = "/{language}",
            method = RequestMethod.GET
    )
    public String listAction(@PathVariable String language, HttpSession session, ModelMap model)
    {
        RightValidator.hardValidate(session, UserType.EMPLOYEE);
        model.put("messageList", this.service.findAllByLanguage(language));
        
        return "i18n/management";
    }
}
