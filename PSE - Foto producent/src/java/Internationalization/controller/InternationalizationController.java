package Internationalization.controller;

import Internationalization.Entity.Internationalization;
import Internationalization.Service.InternationalizationService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This class is used for internationalization (i18n for short)
 *
 * @author Wesley Verheijen
 */
@Controller
@RequestMapping("/i18n")
public class InternationalizationController
{

    @Autowired
    private InternationalizationService service;

    private static InternationalizationService staticService;

    private static String mainLanguage = "nl";
    private static List<Internationalization> translations;
    //private static String pathToLangFiles = I18n.class.getResource("");

    private String message;
    private String translation;

    public InternationalizationController()
    {
    }

    public InternationalizationController(String message)
    {
        this.message = message;
        //this.setTranslation();
    }
    
    public static void truncateTranslations() {
        translations = null;
    }

    private void setTranslation()
    {
        if (translations == null)
        {
            if (service == null)
            {
                //QName name;
                //name = new QName("InternationalizationService");
                //service = (Service.create(name));
                service = new InternationalizationService();
                service.init();
            }

            System.out.println(service);

            if (translations == null)
            {
                translations = new ArrayList<Internationalization>();
                translations = service.findAll();

                System.out.println(translations);
                //translations = service.findAll();//new ArrayList<Internationalization>(service);
                //translations = (new InternationalizationService()).findAll();//new ArrayList<Internationalization>(service);
            }
        }

        Stream filter = translations.stream().filter((translation)
                -> translation != null
                && translation.getSource().equals(message)
        // && translation.getLanguage().equals(InternationalizationController.mainLanguage)
        );

        if (translations.stream().filter((translation)
                -> translation != null
                && translation.getSource().equals(message)
                && translation.getLanguage().equals(this.mainLanguage)
        // && translation.getLanguage().equals(InternationalizationController.mainLanguage)
        ).count() == 0)
        {
            service.insert(new Internationalization(message, message, InternationalizationController.mainLanguage));
            translation = message;
            InternationalizationController.truncateTranslations();
        }
        else
        {
            translation = ((Internationalization) translations.stream().filter((translation)
                    -> translation != null
                    && translation.getSource().equals(message)
                    && translation.getLanguage().equals(InternationalizationController.mainLanguage)
            ).findFirst().get()).getTranslation();
        }
    }

    public static String l(String message)
    {
        return (new InternationalizationController(message)).toString();
    }

    @RequestMapping(
            value = "/set/{lang}",
            headers = {"Content-type=application/json"},
            method = RequestMethod.POST
    )
    @ResponseBody
    public String i18nSetAction(@PathVariable String lang, ModelMap model)
    {
        /** @todo save lang in session */
        InternationalizationController.mainLanguage = lang;
        System.out.println(InternationalizationController.mainLanguage);
        /*
        $.ajax({
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            'type': 'POST',
            'url': 'i18n/set/en',
            'data': {},
            'dataType': 'json',
            'success': function(result) {console.log(result)}
        });
        */
        return "{\"result\": \"ok\"}";
    }

    @RequestMapping(
            value = "/get",
            headers = {"Content-type=application/json"},
            method = RequestMethod.POST
    )
    @ResponseBody
    public String i18nGetAction(ModelMap model)
    {
        return "{\"result\": \"" + InternationalizationController.mainLanguage + "\"}";
        /*
        $.ajax({
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            'type': 'POST',
            'url': 'i18n/get',
            'data': {},
            'dataType': 'json',
            'success': function(result) {console.log(result)}
        });
        */
    }

    @RequestMapping(
            value = "/{message}",
            method = RequestMethod.GET
    )
    public String i18nAction(@PathVariable String message, ModelMap model)
    {
        /** @todo load lang from session */
        this.message = message;
        this.setTranslation();

        model.put("message", toString());

        return "i18n/message";
    }

    @Override
    public String toString()
    {
        //System.out.println(service.findAll());
        System.out.println(message + "=>" + translation + " (" + InternationalizationController.mainLanguage + ")");
//        if(I18n.translations.containsKey(message)) {
//            return (String) I18n.translations.get(message);
//        }
        return translation;
    }
}
