/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Internationalization.Entity;

/**
 *
 * @author Wesley Verheijen
 */
public class SaveTranslation {
    protected int id;
    protected String translation;
    
    public SaveTranslation() {
        // empty constructor
    }

    public SaveTranslation(int id, String translation) {
        this.id = id;
        this.translation = translation;
    }

    public int getId() {
        return id;
    }

    public String getTranslation() {
        return translation;
    }
    
    
}
