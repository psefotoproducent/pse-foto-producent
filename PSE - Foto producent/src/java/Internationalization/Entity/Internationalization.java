/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Internationalization.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Wesley
 */
@Entity
@Table(name = "internationalization")
//@NamedQuery(name = "i18n.namedquery", query="SELECT * FROM internationalization")
@NamedQuery(name = "internationalization.namedquery", query = "FROM Internationalization.Entity.Internationalization")
public class Internationalization
{

    @Id
    @GeneratedValue
    @Column(name = "internationalizationId")
    protected Long internationalizationId;

    @Column(name = "source")
    protected String source;

    @Column(name = "translation")
    protected String translation;

    @Column(name = "language")
    protected String language;

    public Internationalization()
    {
    }

    public Internationalization(String source, String translation, String language)
    {
        this.source = source;
        this.translation = translation;
        this.language = language;
    }

    public Long getInternationalizationId()
    {
        return internationalizationId;
    }

    public String getSource()
    {
        return source;
    }

    public String getTranslation()
    {
        return translation;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setTranslation(String translation)
    {
        this.translation = translation;
    }

    @Override
    public String toString()
    {
        return source + " => " + translation + " " + language;
    }
}
