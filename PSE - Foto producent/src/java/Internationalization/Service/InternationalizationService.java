/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Internationalization.Service;

import Internationalization.Entity.Internationalization;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wesley
 */
@Service(value = "InternationalizationService")
public class InternationalizationService
{

    @PersistenceContext
    private EntityManager em;

    public void init()
    {
        if (em == null)
        {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysql_fotoproducent_rootPool");
            em = emf.createEntityManager();
        }
        System.out.println(em);
    }

    @Transactional
    public void insert(Internationalization internationalization)
    {
        // insert into database
        // persist function is for NEW entities in database
        this.em.persist(internationalization);
    }

    @Transactional
    public Internationalization get(Integer id)
    {
        // this gets the entity from the database and returns it
        return this.em.find(Internationalization.class, (long) id);
    }

    @Transactional
    public Internationalization update(Internationalization internationalization)
    {
        // this updates the ExampleEntity in within the database
        return this.em.merge(internationalization);
    }

    @Transactional
    public void remove(Integer id)
    {
        Internationalization internationalization = this.em.find(Internationalization.class, (long) id);

        // this updates the Internationalization in within the database
        this.em.remove(internationalization);
    }

    @Transactional
    public List<Internationalization> findAll()
    {
        Query q = this.em.createNamedQuery("internationalization.namedquery", Internationalization.class);
        //Query q = this.em.createQuery("SELECT i FROM Internationalization.Entity.internationalization i");
        return q.getResultList();
    }

    @Transactional
    public List<Internationalization> findAllByLanguage(String language)
    {
        Query q = this.em.createQuery("FROM Internationalization.Entity.Internationalization WHERE language = '" + language + "'", Internationalization.class);
        //Query q = this.em.createQuery("SELECT i FROM Internationalization.Entity.internationalization i");
        return q.getResultList();
    }
}
