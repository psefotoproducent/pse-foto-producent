package Photo.Entity;

import Photo.Entity.Interface.IPhotoSession;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name="photosession")
@NamedQueries(value=
    {
        @NamedQuery(name = "photosession.findPhotosession", query = "SELECT p FROM PhotoSession p WHERE loginCode= :loginCode"),
        @NamedQuery(name = "photosession.findPortraitClients", query = "SELECT portraitClients FROM PhotoSession p WHERE photoSessionId= :photoSessionId")
            // room for more
    })
public class PhotoSession implements IPhotoSession {
    
    @Id @GeneratedValue
    @Column(name="photoSessionId") 
    protected Long photoSessionId;

    @Column(name = "photographerId")
    protected Long photographerId;

    @Column(name = "description")
    protected String description;

    @Column(name = "date")
    @Temporal(javax.persistence.TemporalType.DATE)
    protected Date date;

    @Column(name = "organisation")
    protected String organisation;

    @Column(name = "organisationStreet")
    protected String organisationStreet;

    @Column(name = "organisationHouseNumber")
    protected String organisationHouseNumber;

    @Column(name = "organisationPostalCode")
    protected String organisationPostalCode;

    @Column(name = "organisationCity")
    protected String organisationCity;
    
    @Column(name = "portraitClients")
    protected String portraitClients;
    
    @OneToMany(targetEntity=Photo.class, mappedBy="photoSession", cascade={CascadeType.ALL}, orphanRemoval=true)
    Set<Photo> photos = new HashSet<Photo>();

    @Column(name="loginCode")
    protected String loginCode;
    
    public PhotoSession() {
        
    }

    public PhotoSession(Long photographerId, String description, Date date,
            String organisation, String organisationStreet,
            String organisationHouseNumber, String organisationPostalCode,
            String organisationCity)
    {
        this.photographerId = photographerId;
        this.description = description;
        this.date = date;
        this.organisation = organisation;
        this.organisationStreet = organisationStreet;
        this.organisationHouseNumber = organisationHouseNumber;
        this.organisationPostalCode = organisationPostalCode;
        this.organisationCity = organisationCity;                     
    }

    public PhotoSession(Long photographerId, String description, Date date,
            String organisation, String organisationStreet,
            String organisationHouseNumber, String organisationPostalCode,
            String organisationCity, String portraitClients)
    {
        this.photographerId = photographerId;
        this.description = description;
        this.date = date;
        this.organisation = organisation;
        this.organisationStreet = organisationStreet;
        this.organisationHouseNumber = organisationHouseNumber;
        this.organisationPostalCode = organisationPostalCode;
        this.organisationCity = organisationCity;  
        
        // below turns allClients-string (e.g. "Client1,Client2,Client3") into
        // Client1-sao124onioj,Client2-a082jnhlscdk,Client3-ciehjw018923
        String allClientAndCodes = "";        
        String[] clientNames = portraitClients.split(",");                    
        String previousSeparator = "";

        // append clientCode to each clientName
        for(String client : clientNames){            
            String clientAndCode = client.concat("-" + createClientCode());            

            allClientAndCodes = allClientAndCodes.concat(previousSeparator + clientAndCode);
            previousSeparator = ",";
        }

        System.out.println("AllClientsAndCodes=" + allClientAndCodes);

        this.portraitClients = allClientAndCodes;
              
    }

    @Override
    public Long getPhotoSessionId()
    {
        return photoSessionId;
    }

    @Override
    public Long getPhotographerId()
    {
        return photographerId;
    }

    public void setPhotographerId(Long photographerId)
    {
        this.photographerId = photographerId;
    }
    

    @Override
    public String getDescription()
    {
        return description;
    }

    @Override
    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public Date getDate()
    {
        return date;
    }

    @Override
    public void setDate(Date date)
    {
        this.date = date;
    }

    @Override
    public String getOrganisation()
    {
        return organisation;
    }

    @Override
    public void setOrganisation(String organisation)
    {
        this.organisation = organisation;
    }

    @Override
    public String getOrganisationHouseNumber()
    {
        return organisationHouseNumber;
    }

    @Override
    public void setOrganisationHouseNumber(String organisationHouseNumber)
    {
        this.organisationHouseNumber = organisationHouseNumber;
    }

    @Override
    public String getOrganisationCity()
    {
        return organisationCity;
    }

    @Override
    public void setOrganisationCity(String organisationCity)
    {
        this.organisationCity = organisationCity;
    }

    @Override
    public String getOrganisationStreet()
    {
        return organisationStreet;
    }

    @Override
    public void setOrganisationStreet(String organisationStreet)
    {
        this.organisationStreet = organisationStreet;
    }

    @Override
    public String getOrganisationPostalCode()
    {
        return organisationPostalCode;
    }

    @Override
    public void setOrganisationPostalCode(String organisationPostalCode)
    {
        this.organisationPostalCode = organisationPostalCode;
    }

    @Override
    public String getAddress()
    {
        String result = "";

        if ((this.organisationStreet != null) && (!"".equals(this.organisationStreet)))
        {
            result = result + this.organisationStreet;
        }

        if ((this.organisationHouseNumber != null) && (!"".equals(this.organisationHouseNumber)))
        {
            result = result + " " + this.organisationHouseNumber;
        }

        if ((this.organisationPostalCode != null) && (!"".equals(this.organisationPostalCode)))
        {
            result = result + ", " + this.organisationPostalCode;
        }

        if ((this.organisationCity != null) && (!"".equals(this.organisationCity)))
        {
            result = result + " " + this.organisationCity;
        }

        return result;
    }

    public String getPortraitClients() {
        return portraitClients;
    }

    public void setPortraitClients(String portraitClients) {
        this.portraitClients = portraitClients;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }
    
    /**
     * Creates unique, 12 numeric/character code for clientspecific login purposes
     * @return 
     */
    private String createClientCode(){
            
        UUID uuid = UUID.randomUUID();
        long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
        return Long.toString(l, Character.MAX_RADIX);
        
    }
    
    /**
     * Split clientname/code combination first (e.g. "Client1-12jn12uoin4,Client2-1509nwk2342k,Client3-23nkfjn349")
     * Find matching name, return code
     * @param clientName
     * @return 
     */
    public String getClientCode(String clientName){
        
        List<String> clients = Arrays.asList(portraitClients.split(","));

        
        for (String client : clients) {
            String name = client.split("-")[0];
            String code = client.split("-")[1];
                        
            if(name.equalsIgnoreCase(clientName)){
                return code;
            }
        }
        
        return null;
    }
}
    
