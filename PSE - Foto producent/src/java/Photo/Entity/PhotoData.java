package Photo.Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "photoData")
public class PhotoData implements Serializable
{

    @Id
    @GeneratedValue
    @Column(name = "photoDataId")
    protected Long photoDataId;

    @Column(name = "data", columnDefinition = "LONGBLOB")
    protected byte[] data;

    public PhotoData()
    {

    }

    public PhotoData(byte[] data)
    {
        this.data = data;
    }

    public Long getPhotoDataId()
    {
        return photoDataId;
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }
}
