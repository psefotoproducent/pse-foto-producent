/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Photo.Entity;

/**
 *
 * @author Dominique
 */
public enum PhotoType
{
    GROUP(1),
    PORTRAIT(2);

    private int value;

    PhotoType(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}
