package Photo.Entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "photo")
public class Photo implements Serializable
{

    @Id
    @GeneratedValue
    @Column(name = "photoId")
    protected Long photoId;
    
    @ManyToOne(targetEntity = PhotoSession.class)
    @JoinColumn(name = "photoSessionId")
    @Fetch(FetchMode.JOIN)
    protected PhotoSession photoSession;

    @Column(name = "filename")
    protected String filename;

    @Column(name = "price")
    protected Float price;

    @Column(name = "photoType")
    protected PhotoType photoType;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "photoDataId")
    @Fetch(FetchMode.JOIN)
    protected PhotoData thumbnail;
    
    @Column(name = "active")
    protected boolean active;
    
    @Column(name = "clientName")
    protected String clientName;
    
    @Column(name = "clientCode")
    protected String clientCode;

    public Photo()
    {
    }
    
    public Photo(PhotoSession photoSession)
    {
        this.photoSession = photoSession;
    }

    public Photo(PhotoSession photoSession, String filename, Float price, PhotoType photoType, PhotoData thumbnail)
    {
        this.photoSession = photoSession;
        this.filename = filename;
        this.price = price;
        this.photoType = photoType;
        this.thumbnail = thumbnail;
    }

    public Long getPhotoId()
    {
        return photoId;
    }

    public PhotoSession getPhotoSession() 
	{
        return photoSession;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public Float getPrice()
    {
        return price;
    }

    public void setPrice(Float price)
    {
        this.price = price;
    }

    public PhotoType getPhotoType()
    {
        return photoType;
    }

    public void setPhotoType(PhotoType photoType)
    {
        this.photoType = photoType;
    }

    public PhotoData getThumbnail()
    {
        return thumbnail;
    }

    public void setThumbnail(PhotoData thumbnail)
    {
        this.thumbnail = thumbnail;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }
}
