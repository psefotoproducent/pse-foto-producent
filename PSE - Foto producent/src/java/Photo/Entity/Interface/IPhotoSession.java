package Photo.Entity.Interface;

import java.io.Serializable;
import java.util.Date;

public interface IPhotoSession extends Serializable
{

    public Long getPhotoSessionId();

    public Long getPhotographerId();

    public String getDescription();

    public void setDescription(String description);

    public Date getDate();

    public void setDate(Date date);

    public String getOrganisation();

    public void setOrganisation(String organisation);

    public String getOrganisationHouseNumber();

    public void setOrganisationHouseNumber(String organisationHouseNumber);

    public String getOrganisationCity();

    public void setOrganisationCity(String organisationCity);

    public String getOrganisationStreet();

    public void setOrganisationStreet(String organisationStreet);

    public String getOrganisationPostalCode();

    public void setOrganisationPostalCode(String organisationPostalCode);

    public String getAddress();
}
