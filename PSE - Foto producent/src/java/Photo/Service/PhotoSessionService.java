package Photo.Service;

import Photo.Entity.Photo;
import Photo.Entity.PhotoSession;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class PhotoSessionService {
    
    /**
     * used for logincode 
     */
    protected SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(PhotoSession photoSession)
    {
        this.em.persist(photoSession);
                
        photoSession.setLoginCode(createLoginCode(photoSession));            
        
        update(photoSession);
    }

    @Transactional
    public PhotoSession get(Integer id)
    {
        return this.em.find(PhotoSession.class, (long) id);
    }

    @Transactional
    public PhotoSession update(PhotoSession photo)
    {
        return this.em.merge(photo);
    }

    @Transactional
    public void remove(Integer id)
    {
        this.em.remove(this.get(id));
    }

    @Transactional
    public List<PhotoSession> findAll(Long photographerId)
    {
        Query q = this.em.createQuery("FROM Photo.Entity.PhotoSession WHERE photographerId = :photographerId", PhotoSession.class);
        q.setParameter("photographerId", photographerId);
        return q.getResultList();
    }

    @Transactional
    public List<Photo> findAllPhotos(Long id)
    {
        Query q = this.em.createQuery("FROM Photo.Entity.Photo WHERE photoSessionId = :photoSessionId AND active = 1", Photo.class);
        q.setParameter("photoSessionId", id);
        return q.getResultList();
    }
    
    private String createLoginCode(PhotoSession photoSession){
        
        String loginCode;
        
        // logincode = photosessionId + photographedId + date ddmmyyyy
        String photoSessionId = photoSession.getPhotoSessionId().toString();
        String photographerId = photoSession.getPhotographerId().toString();
        String dateString = dateFormat.format(photoSession.getDate());  
        
        loginCode = photoSessionId + photographerId + dateString;
        
        return loginCode;
    }
}
