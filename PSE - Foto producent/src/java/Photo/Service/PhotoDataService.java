package Photo.Service;

import Photo.Entity.PhotoData;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class PhotoDataService {
    
    @PersistenceContext
    private EntityManager em;
    
    @Transactional
    public void insert(PhotoData photoData)
    {
        this.em.persist(photoData);
    }

    @Transactional
    public PhotoData get(Integer id)
    {
        return this.em.find(PhotoData.class, (long) id);
    }

    @Transactional
    public PhotoData update(PhotoData photoData)
    {
        return this.em.merge(photoData);
    }

    @Transactional
    public void remove(PhotoData photoData)
    {
        this.em.remove(this.em.merge(photoData));
    }
    
}
