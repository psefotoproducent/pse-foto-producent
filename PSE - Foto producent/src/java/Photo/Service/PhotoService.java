package Photo.Service;

import Photo.Entity.Photo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class PhotoService
{

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(Photo photo)
    {
        this.em.persist(photo);
    }

    @Transactional
    public Photo get(Integer id)
    {
        return this.em.find(Photo.class, (long) id);
    }

    @Transactional
    public Photo update(Photo photo)
    {
        return this.em.merge(photo);
    }

    @Transactional
    public void remove(Photo photo)
    {
        this.em.remove(photo);
    }

    @Transactional
    public List<Photo> findAll()
    {
        Query q = this.em.createQuery("FROM Photo.Entity.Photo WHERE active = 1", Photo.class);
        return q.getResultList();
    }

    @Transactional
    public List<Photo> findByText(String filter, Long photographerId) {
        String query = "FROM Photo.Entity.Photo photo JOIN FETCH photo.photoSession session";
        
        query = query + " WHERE Lower(photo.filename) LIKE Lower('%"+ filter +"%')";
        query = query + " AND photo.active = 1";
        
        if (photographerId > 0) {
            query = query + " AND session.photographerId = " + photographerId.toString();
        }
        
        Query q = this.em.createQuery(query, Photo.class);
        return q.getResultList();
    }
}
