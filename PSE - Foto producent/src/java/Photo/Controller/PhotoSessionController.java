package Photo.Controller;

import Login.Entity.Photographer;
import Login.Entity.RightValidator;
import Login.Entity.UserType;
import Photo.Entity.PhotoSession;
import Photo.Service.PhotoSessionService;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.sun.xml.ws.security.opt.impl.util.SOAPUtil;
import java.io.IOException;
import java.security.AccessControlException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PhotoSessionController
{

    @Autowired
    private PhotoSessionService service;

    @RequestMapping(value = "/photosession", method = RequestMethod.GET)
    public String photoSessionAction(ModelMap model, HttpSession session)
    {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

        List<PhotoSession> sessionList = null;
        Photographer photographer = (Photographer) session.getAttribute("loggedInPhotographer");
        if (photographer != null)
        {
            sessionList = this.service.findAll(photographer.getPhotographerId());
        }

        model.put("sessionlist", sessionList);

        return "photo/sessions/overview";
    }

    @RequestMapping(value = "/management/photosession/delete", method = RequestMethod.POST)
    public String photoSessionDeleteAction(@RequestParam String photoSessionId, ModelMap model, HttpSession session)
    {
        PhotoSession photoSession;
        Photographer photographer;
        try
        {
            RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

            photoSession = this.service.get(Integer.parseInt(photoSessionId));

            photographer = (Photographer) session.getAttribute("loggedInPhotographer");

            if (!photoSession.getPhotographerId().equals(photographer.getPhotographerId()))
            {
                throw new AccessControlException("No rights to view someone elses photo session!");
            }

            try
            {
                this.service.remove(
                        Integer.parseInt(photoSessionId)
                );
            }
            catch (DataAccessException ex)
            {
                System.out.println("Foreignkey constraint fail (Someone ordered photo's from this session, can't delete)");
            }
        }
        catch (NumberFormatException nfe)
        {
            List<PhotoSession> sessionList = null;
            photographer = (Photographer) session.getAttribute("loggedInPhotographer");
            if (photographer != null)
            {
                sessionList = this.service.findAll(photographer.getPhotographerId());
            }

            model.put("sessionlist", sessionList);
            return "photo/sessions/overview";
        }

        return "redirect:/photosession";
    }

    @RequestMapping(value = "/management/photosession/addBuffer", method = RequestMethod.GET)
    public String photoSessionAddBufferAction(ModelMap model, HttpSession session)
    {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

        Long photographerId = new Long(0);
        Photographer photographer = (Photographer) session.getAttribute("loggedInPhotographer");
        if (photographer != null)
        {
            photographerId = photographer.getPhotographerId();
        }

        model.put("photographerId", photographerId);
        model.put("photosession", new PhotoSession());

        return "photo/sessions/edit_multiple";
    }

    @RequestMapping(value = "/management/photosession/addBuffer", method = RequestMethod.POST)
    public void photoSessionAddBufferAction(
            @RequestParam String photographerId,
            @RequestParam String sessioncount,
            @RequestParam String description,
            @RequestParam String organisation,
            @RequestParam String organisationStreet,
            @RequestParam String organisationHouseNumber,
            @RequestParam String organisationPostalCode,
            @RequestParam String organisationCity,
            HttpSession session, ModelMap model,
            HttpServletResponse response
    ) throws IOException
    {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

        Integer sessies = Integer.parseInt(sessioncount);
        String textString = "Generated logincodes:";

        PhotoSession photoSession;
        for (int i = 1; i <= sessies; i++)
        {
            photoSession = new PhotoSession(
                    Long.parseLong(photographerId),
                    description,
                    new Date(),
                    organisation,
                    organisationStreet,
                    organisationHouseNumber,
                    organisationPostalCode,
                    organisationCity
            );
            this.service.insert(photoSession);

            textString = textString + System.lineSeparator() + photoSession.getLoginCode();
        }

        response.setContentType("text/plain");
        response.setHeader("Content-Disposition", "attachment;filename=logincodes.txt");
        ServletOutputStream out = response.getOutputStream();
        out.println(textString);
        out.flush();
        out.close();
    }

    @RequestMapping(value = "/management/photosession/add", method = RequestMethod.GET)
    public String photoSessionAddAction(ModelMap model, HttpSession session)
    {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

        Long photographerId = new Long(0);
        Photographer photographer = (Photographer) session.getAttribute("loggedInPhotographer");
        if (photographer != null)
        {
            photographerId = photographer.getPhotographerId();
        }

        model.put("photographerId", photographerId);
        model.put("photosession", new PhotoSession());

        return "photo/sessions/edit";
    }

    @RequestMapping(value = "/management/photosession/add", method = RequestMethod.POST)
    public String photoSessionAddAction(
            @RequestParam String photographerId,
            @RequestParam String description,
            @RequestParam String organisation,
            @RequestParam String organisationStreet,
            @RequestParam String organisationHouseNumber,
            @RequestParam String organisationPostalCode,
            @RequestParam String organisationCity,
            @RequestParam String allClients,
            HttpSession session, ModelMap model
    )
    {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

        PhotoSession photoSession;
        
        // use constructor 1 for session with only groupphoto's
        if(allClients.isEmpty()){
            
            photoSession = new PhotoSession(
                Long.parseLong(photographerId),
                description,
                new Date(),
                organisation,
                organisationStreet,
                organisationHouseNumber,
                organisationPostalCode,
                organisationCity
            );
            this.service.insert(photoSession);
            
        // use constructor 2 for session with portraitphoto's
        } else {
            
            photoSession = new PhotoSession(
                Long.parseLong(photographerId),
                description,
                new Date(),
                organisation,
                organisationStreet,
                organisationHouseNumber,
                organisationPostalCode,
                organisationCity,
                allClients
            );
            this.service.insert(photoSession);
        }
        

        String photoSessionId = String.valueOf(photoSession.getPhotoSessionId());

        return "redirect:/photosession/" + photoSessionId;
    }
    ///// END ADD-PHOTOSESSION V2 //////

    @RequestMapping(value = "/management/photosession/edit/{photoSessionId}", method = RequestMethod.GET)
    public String photoSessionEditAction(@PathVariable String photoSessionId, ModelMap model, HttpSession session)
    {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

        Long photographerId = new Long(0);
        Photographer photographer = (Photographer) session.getAttribute("loggedInPhotographer");

        if (photographer != null)
        {
            photographerId = photographer.getPhotographerId();
        }

        PhotoSession photoSession = this.service.get(Integer.parseInt(photoSessionId));

        if (!photoSession.getPhotographerId().equals(photographerId))
        {
            throw new AccessControlException("No rights to view someone elses photo session!");
        }

        model.put("photographerId", photographerId);
        model.put("photosession", photoSession);

        return "photo/sessions/edit";
    }

    @RequestMapping(value = "/management/photosession/edit/{photoSessionId}", method = RequestMethod.POST)
    public String photoSessionEditAction(
            @PathVariable String photoSessionId,
            @RequestParam String description,
            @RequestParam String organisation,
            @RequestParam String organisationStreet,
            @RequestParam String organisationHouseNumber,
            @RequestParam String organisationPostalCode,
            @RequestParam String organisationCity,
            HttpSession session, ModelMap model
    )
    {
        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);

        Long photographerId = new Long(0);
        Photographer photographer = (Photographer) session.getAttribute("loggedInPhotographer");

        if (photographer != null)
        {
            photographerId = photographer.getPhotographerId();
        }

        PhotoSession photoSession = this.service.get(Integer.parseInt(photoSessionId));

        if (!photoSession.getPhotographerId().equals(photographerId))
        {
            throw new AccessControlException("No rights to edit someone elses photo session!");
        }

        photoSession.setDescription(description);
        photoSession.setOrganisation(organisation);
        photoSession.setOrganisationStreet(organisationStreet);
        photoSession.setOrganisationHouseNumber(organisationHouseNumber);
        photoSession.setOrganisationPostalCode(organisationPostalCode);
        photoSession.setOrganisationCity(organisationCity);

        this.service.update(photoSession);

        return "redirect:/photosession";
    }
    
    /**
     * Method for photoshoot overview page (data which fills 'Click here for LoginCodes' modal)
     * @param photoSessionId
     * @param response
     * @return
     * @throws IOException 
     */
    @RequestMapping(
            value = "/photosession/getLoginCodes/{photoSessionId}", 
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE
    )
    public @ResponseBody
        String photoSessionAction(@PathVariable String photoSessionId, HttpServletResponse response) throws IOException {
        
        response.setContentType("text/plain");
        ServletOutputStream out = response.getOutputStream();
        PhotoSession photoSession = this.service.get(Integer.parseInt(photoSessionId));
        
        // Only true if session only contains groupphoto's (no portraitclients)
        if(photoSession.getPortraitClients() == null){
            out.println(photoSession.getLoginCode());
            out.flush();
            out.close();
            return "";
        }
        
        // Session contains portraitclients, get clientCodes instead
        else {
            out.println(photoSession.getPortraitClients() + ":" + photoSession.getLoginCode());
            out.flush();
            out.close();
            return "";
        }
    }
}
