package Photo.Controller;

import Photo.Entity.Photo;
import Photo.Entity.PhotoType;
import Application.Model.FileMeta;
import Application.Model.StorageProperties;
import Application.Service.StorageService;
import Login.Entity.Photographer;
import Photo.Entity.PhotoData;
import Photo.Entity.PhotoSession;
import Photo.Service.PhotoService;
import Photo.Service.PhotoSessionService;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

@Controller
public class PhotoController
{
    /** @todo if user needs to be logged in in order to see photos add validation methods */
    FileMeta fileMeta = null;
    StorageService storageService;

    @Autowired
    private PhotoSessionService photoSessionService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ServletContext context;
    
    @RequestMapping(value = "/photosession/{photoSessionId}", method = RequestMethod.GET)
    public String photoSessionUploadAction(@PathVariable String photoSessionId, ModelMap model)
    {
        try
        {
            PhotoSession photoSession = this.photoSessionService.get(Integer.parseInt(photoSessionId));
            model.put("photoSession", photoSession);
            model.put("photos", this.photoSessionService.findAllPhotos(Long.parseLong(photoSessionId)));
            String portraitClients = photoSession.getPortraitClients();
            
            String portraitClientsOnlyNames = "";
                        
            if(portraitClients != null){    
                
                List<String> clients = Arrays.asList(portraitClients.split(","));
                String previousSeparator = "";
                
                for (String client : clients) {
                    portraitClientsOnlyNames = portraitClientsOnlyNames.concat(previousSeparator + client.split("-")[0]);
                    previousSeparator = ", ";
                    model.put("portraitClients", portraitClientsOnlyNames);
                }
            }
            
        }
        catch (NumberFormatException nfe)
        {
            // Error
        }

        return "photo/upload";
    }
    
    @RequestMapping(value = "/photosession/{photoSessionId}", method = RequestMethod.POST)
    public String photoSessionUploadAction(@PathVariable String photoSessionId, MultipartHttpServletRequest request,  
            HttpServletResponse response, ModelMap model, HttpSession session)
    {        
        String filePath = "C:/Temp/Photos/" + photoSessionId;
        this.storageService = new StorageService(new StorageProperties(filePath), this.context);
        this.storageService.init();
        
        
        List<MultipartFile> files = request.getFiles("files[]");
                
        PhotoSession photoSession = this.photoSessionService.get(Integer.parseInt(photoSessionId));
        Photographer photographer = (Photographer)session.getAttribute("loggedInPhotographer");

        //2. get each file
        if(files != null){
            
            for(MultipartFile mpf : files){
            //2.1 loop through MultipartFiles

            if (!mpf.getContentType().startsWith("image/"))
            {
                continue;
            }

            //2.3 create new fileMeta
            fileMeta = new FileMeta();
            fileMeta.setFileName(mpf.getOriginalFilename());
            fileMeta.setFileSize(mpf.getSize() / 1024 + " Kb");
            fileMeta.setFileType(mpf.getContentType());

                try {
                    Photo photo = new Photo(photoSession);
                    photo.setActive(true);
                    PhotoData photoData = new PhotoData();

                    fileMeta.setBytes(mpf.getBytes());
                    byte[] img = this.storageService.store(fileMeta, true);

                    photo.setFilename(this.storageService.load(mpf.getOriginalFilename()).toString());

                    if (photographer != null) {
                        photo.setPrice(photographer.getGroupPrice());
                    } else {
                        photo.setPrice(new Float(0));
                    }

                    photo.setPhotoType(PhotoType.GROUP);
                    photoData.setData(img);
                    photo.setThumbnail(photoData);

                    this.photoService.insert(photo);
                }
                catch (IOException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        

        return "redirect:" + photoSessionId;
    }
    
    @RequestMapping(
            value = "/photosession/savePortraitClient", 
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE
    )
    public @ResponseBody
    String photoSessionSavePortraitClientAction(@RequestParam("photoSessionId") String photoSessionId,
                                                @RequestParam("photoId") String photoId,
                                                @RequestParam("client") String client,
                                                HttpSession session, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain");
        ServletOutputStream out = response.getOutputStream();
        
        // 1. check if everything is in order
        PhotoSession photoSession = this.photoSessionService.get(Integer.parseInt(photoSessionId));
        Photographer photographer = (Photographer)session.getAttribute("loggedInPhotographer");
        Photo photo = this.photoService.get(Integer.parseInt(photoId));
        
        if(photoSession == null || photographer == null || photo == null){
            System.out.println("Something is terribly wrong");
            
            out.println("FAIL");
            out.flush();
            out.close();
            
            return "";
        }     
        
        // 2. set default photovalues
        String clientName = null;
        PhotoType type = PhotoType.GROUP;
        Float price = photographer.getGroupPrice();
        
        // 2a. Photographer selected groupphoto
        if (!client.equalsIgnoreCase("GROUP")){
            // remove leading/trailing whitespace
            clientName = client.trim();
            type = PhotoType.PORTRAIT;
            price = photographer.getPortraitPrice();
        }        
        
        photo.setClientName(clientName);
        String clientCode = photoSession.getClientCode(clientName);
        photo.setClientCode(clientCode);        
        photo.setPhotoType(type);
        photo.setPrice(price);
        this.photoService.update(photo);
        
        System.out.println("Changes to photo saved");
        
        out.println(price);
        out.flush();
        out.close();
        
        return "";
    }    
    
    ///////// END V2 /////////
    

    @RequestMapping(value = "photosession/getPicture/{photoId}", method = RequestMethod.GET)
    public void getPictue(@PathVariable String photoId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException
    {
        Photo photo = this.photoService.get(Integer.parseInt(photoId));
        Hibernate.initialize(photo.getThumbnail());

        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(photo.getThumbnail().getData());
        response.getOutputStream().close();
    }
    
    @RequestMapping(value = "photosession/deletePicture/{photoId}", method = RequestMethod.POST)
    public void deletePictue(@PathVariable String photoId, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException
    {
        Photo photo = this.photoService.get(Integer.parseInt(photoId));
        photo.setActive(false);
        this.photoService.update(photo);
    }
}
