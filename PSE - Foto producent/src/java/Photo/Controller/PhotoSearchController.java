package Photo.Controller;

import Login.Entity.Photographer;
import Login.Entity.RightValidator;
import Login.Entity.UserType;
import Photo.Service.PhotoService;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PhotoSearchController
{

    @Autowired
    private PhotoService service;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String photoSessionUploadAction(@RequestParam("searchText") String searchText, ModelMap model, HttpSession session)
    {
//        RightValidator.hardValidate(session, UserType.PHOTOGRAPHER);
        
        Long photographerId = new Long(0);
        Photographer photographer = (Photographer)session.getAttribute("loggedInPhotographer");
        if (photographer != null) {
            photographerId = photographer.getPhotographerId();
        }
        
        if (searchText.length() > 0) {
            model.put("results", this.service.findByText(searchText, photographerId));
        }
        
        model.put("searchText", searchText);
        return "photo/search";
    }
}
