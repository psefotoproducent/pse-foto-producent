$(document).ready(function(){
   
    /** 
     * Start Delay so browser won't skip this parts...
     */
    setTimeout(function(){
        /**
         * Set height nav-menu to height of content so no gap 
         * falls as nav-menu has default 100% height
         */
        var nav_menu = $(".nav-side-menu");
        var admin_right_content = $(".admin-right-content-wrapper");
        
        var height = admin_right_content.outerHeight();
        var body_html_height = $("html").outerHeight();
        
        if(admin_right_content.outerHeight() < body_html_height)
        {
            height = '100%';
        }
        nav_menu.css("height", height);
        
    },400);
    /** 
     * End Delay so browser won't skip this parts...
     */
    
});