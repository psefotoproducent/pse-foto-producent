<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<%@tag description="Simple Template" pageEncoding="UTF-8" %>

<%@attribute name="title"%>
<%@attribute name="body" fragment="true" %>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ATeam">
        <meta name="keyword" content="">

        <!-- DEFAULT -->
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<c:url value="/resources/lineicons/style.css" />" rel="stylesheet">
        
        <!-- ATeam Styles -->
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style-responsive.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/dropzone.css" />" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
        <!-- CROPPING -->
        <link rel="stylesheet" href="/resources/crop/assets/css/tether.min.css">
        <link rel="stylesheet" href="/resources/crop/dist/cropper.css">
        <link rel="stylesheet" href="/resources/crop/css/main.css">
        <script type="text/javascript" src="<c:url value="/resources/crop/assets/js/tether.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/crop/dist/cropper.js"/>"></script>
        
        <!-- NIET MEER NODIG -->
        <!--<script type="text/javascript" src="<c:url value="/resources/crop/js/main.js" />"></script>-->

        <!-- SCRIPTS -->
        <script src="<c:url value="/resources/js/jquery.js" />"></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.touch-punch.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.iframe-transport.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.widget.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.fileupload.js" />"></script>
        <script class="include" type="text/javascript" src="<c:url value="/resources/js/jquery.dcjqaccordion.2.7.js" />"></script>
        
        <title>Fotoproducent - ${title}</title>
    </head>
    <body>
        <seciton id="container">
            <!-- **********************************************************************************************************************************************************
            TOP BAR CONTENT & NOTIFICATIONS
            *********************************************************************************************************************************************************** -->
            <!--header start-->
            <header class="header black-bg">
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
                </div>
                  <!--logo start-->
                  <a href="/home" class="logo"><b>FOTOPRODUCENT</b></a>
                  <!--logo end-->
                  <div class="top-menu">
                      <ul class="nav pull-right top-menu">
                          <li><a class="logout" href="/logout"><t:i18n message="Logout" /></a></li>
                      </ul>
                      
                      <% if ((session.getAttribute("loggedInEmployee") != null) || (session.getAttribute("loggedInPhotographer") != null)) { %>
                      
                        <div class="col-sm-2 col-md-2 pull-right">
                            <form action="/search" method="get">                              
                              <div id="custom-search-input">
                                  <div class="input-group col-md-12">
                                      <input type="text" class="form-control" name="searchText" id="searchText" placeholder="<t:i18n message="Search photo..." />" />
                                      <span class="input-group-btn">
                                          <button class="btn" type="submit">
                                              <i class="glyphicon glyphicon-search"></i>
                                          </button>
                                      </span>
                                  </div>
                              </div>
                          </form>
                        </div>
                                    
                      <% } %>
                  </div>
                  <div class="pull-right i18n">
                    <span data-language="en">EN</span>
                    <span data-language="nl">NL</span>
                </div>
              </header>
            <!--header end-->
            
            <!-- **********************************************************************************************************************************************************
            MAIN SIDEBAR MENU
            *********************************************************************************************************************************************************** -->
            <!--sidebar start-->
            <aside>
                <div id="sidebar"  class="nav-collapse ">
                    <jsp:include page="/WEB-INF/views/ui/includes/nav-menu.jsp" />
                </div>
            </aside>
            <!--sidebar end-->
            
            <!-- **********************************************************************************************************************************************************
            MAIN CONTENT
            *********************************************************************************************************************************************************** -->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <jsp:invoke fragment="body"></jsp:invoke>
                </section><!-- /wrapper -->
            </section><!-- /MAIN CONTENT -->
            <!--main content end-->
            
            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2016 - ATeam (Fontys Hogenschool)
                </div>
            </footer>
            <!--footer end-->
        </section>
        
        <!-- COMMON SCRIPT FOR ALL PAGES -->
        <script src="<c:url value="/resources/js/common-scripts.js" />"></script>
        
    </body>
</html>