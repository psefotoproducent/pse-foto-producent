<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@tag description="Simple Template" pageEncoding="UTF-8" %>

<%@attribute name="title"%>
<%@attribute name="body" fragment="true" %>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ATeam">
        <meta name="keyword" content="">

        <!-- DEFAULT -->
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
     
        <link href="<c:url value="/resources/css/omnikassa.css" />" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
        <!-- CROPPING -->
<!--        <link rel="stylesheet" href="resources/crop/assets/css/tether.min.css">
        <link rel="stylesheet" href="resources/crop/dist/cropper.css">
        <link rel="stylesheet" href="resources/crop/css/main.css">
        <script type="text/javascript" src="<c:url value="/resources/crop/assets/js/tether.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/crop/dist/cropper.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/crop/js/main.js" />"></script>-->

        <!-- SCRIPTS -->
        <script src="<c:url value="/resources/js/jquery.js" />"></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.touch-punch.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.iframe-transport.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.widget.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.fileupload.js" />"></script>
        <script class="include" type="text/javascript" src="<c:url value="/resources/js/jquery.dcjqaccordion.2.7.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.scrollTo.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.nicescroll.js" />" type="text/javascript"></script>

        <title>Fotoproducent - ${title}</title>
    </head>
    <body>


    <body>

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->

    <div id="login-page">
        <div class="container">
            <jsp:invoke fragment="body"></jsp:invoke>
        </div>
    </div>

    </body>
</html>