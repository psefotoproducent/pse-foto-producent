<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Products" >    
    <jsp:attribute name="body">



        <body>
            <!--[if lt IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

	
	<style type="text/css">
		img {
		  max-width: 100%; /* This rule is very important, please do not ignore this! */
		}
		.image_container{
			width:600px;
			height: 500px;	
		}
	</style>
        
        <button onclick="initCropper()"> test </button>

	
	<input type="file" name="image" id="image" onchange="readURL(this);"/>
	<div class="image_container">
		<img id="blah" src="resources/images/picture.jpg" alt="your image" />
	</div>
	<div id="cropped_result"></div>
	<button id="crop_button">Crop</button>


	<script type="text/javascript" defer>
           
		function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                setTimeout(initCropper, 1000);
            }
        }
         
	    function initCropper(){
	    	console.log("Came here");
	    	var image = document.getElementById('blah');
			var cropper = new Cropper(image, {
			  aspectRatio: 1 / 1,
			  crop: function(e) {
			    console.log(e.detail.x);
			    console.log(e.detail.y);
			  }
			});

			// On crop button clicked
			document.getElementById('crop_button').addEventListener('click', function(){
	    		var imgurl =  cropper.getCroppedCanvas().toDataURL();
	    		var img = document.createElement("img");
	    		img.src = imgurl;
	    		document.getElementById("cropped_result").appendChild(img);

	    	
					cropper.getCroppedCanvas().toBlob(function (blob) {
						  var formData = new FormData();
						  formData.append('croppedImage', blob);
                                                  var  photoSession = "1";
                                                  
						  // Use `jQuery.ajax` method
						  $.ajax('/cropping/cropped/${photoSession}', {
						    method: "POST",
						    data: formData,
                                                    mimeType:"multipart/form-data",
						    processData: false,
						    contentType: false,
						    success: function () {
						      console.log('Upload success');
						    },
						    error: function () {
						      console.log('Upload error');
						    }
						  });
					});
	    		
	    	});
	    }
	</script>



<!--<form method="POST" action="<c:url value='/uploadd' />"  enctype="multipart/form-data">


    Please select a file to upload : <input type="file" name="file" />
    <div id="cropped_result"></div>
    <input type="submit" value="upload" />

</form>-->
          


        </body>
    </html>


</jsp:attribute>
</t:simple_layout>