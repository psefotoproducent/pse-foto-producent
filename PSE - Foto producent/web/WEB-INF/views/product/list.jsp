<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Products" >    
    <jsp:attribute name="body">
        <script>
            $(function () {
                $('.thumbnail').on('click', function (event) {
                    var id = $(this).data('id');
                    
                    $('.modalImg').attr('src', '/management/product/getPicture/' + id);
                });
            });
        </script>
            
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Products" /></h3>

        <a class="add-btn btn btn-primary" href="/management/product/add" role="button">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <t:i18n message="Add product" />
        </a>
        
        <div class="row mt">
            <div class="col-md-12">
                <div class="content-panel">
                    <form method="post">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                            <tr>
                                <th><t:i18n message="Description" /></th>
                                <th><t:i18n message="Picture" /></th>
                                <th><t:i18n message="Price" /></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${productList}" var="product">
                                    <tr>
                                        <td><a href="/management/product/edit/<c:out value="${product.productId}" />"><c:out value="${product.description}" /></a></td>
                                        <td>
                                            <div class="no-padding col-lg-1 col-md-4 col-xs-6 thumb">
                                                <c:choose>
                                                    <c:when test="${empty product.thumbnail}">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a class="thumbnail" href="#" data-id="${product.productId}" data-toggle="modal" data-target="#modalImage">
                                                            <img class="img-responsive" src="/management/product/getPicture/${product.productId}" alt="">
                                                        </a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </td>
                                        <td>&euro; <c:out value="${product.getPrintPrice()}" /></td>
                                        <td>
                                            <a class="btn btn-primary" href="/management/product/edit/<c:out value="${product.productId}" />" role="button">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                            </a>

                                            <button class="btn btn-danger" type="submit" name="productId" value="<c:out value="${product.productId}" />">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div><!-- /content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- /row -->

        <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <t:i18n message="Photo preview" />
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="modalImg center-block" src="" alt="" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>         
    </jsp:attribute>
</t:simple_layout>