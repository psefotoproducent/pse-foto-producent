<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Add new product" >    
    <jsp:attribute name="body">
        <script>
            $(function () {
                $('#fileupload').fileupload({
                    dataType: 'json',
                    
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progressBar .progress-bar').css(
                            'width',
                            progress + '%'
                        );
                
                        $('#progressBar .progress-text').text(progress + '%');
                
                        if (data.loaded < data.total) {
                            $('#progressBar').css(
                                'display', 'block'
                            );
                        } else {
                            $('#progressBar').css(
                                'display', 'none'
                            );
                        }
                    },
                    
                    dropZone: $('#dropzone')
                });
                
                $('#dropzone').on('dragover dragenter', function() {
                    $('#dropzone').addClass('drop');
                })
                .on('dragleave dragend drop', function() {
                    $('#dropzone').removeClass('drop');
                });
                
                $('.thumbnail').on('click', function (event) {
                    var id = $(this).data('id');
                    
                    $('.modalImg').attr('src', '/management/product/getPicture/' + id);
                });
            });
        </script>
        
        <h3>
            <i class="fa fa-angle-right"></i> 
            <c:choose>
                <c:when test="${product.productId > 0}">
                    <t:i18n message="Edit product" />
                </c:when>    
                <c:otherwise>
                    <t:i18n message="Add new product" />
                </c:otherwise>
            </c:choose>
        </h3>
        
        <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label"><t:i18n message="Description" /></label>
                    <div class="col-sm-8">
                        <input type="text" name="description" class="form-control" id="description" value="${product.description}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label"><t:i18n message="Price" /></label>
                    <div class="col-sm-8">
                        <input type="text" name="price" class="form-control" id="price" value="${product.price}" />
                    </div>
                </div>

                <div class="form-inline" style="display:none;">
                    <div class="form-group">
                        <input id="fileupload" type="file" name="files[]" data-url="${product.productId}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                        <div class="upload-drop-zone" id="dropzone">
                            <t:i18n message="Just drag and drop a file here" />
                        </div>
                    </div>
                </div>

                <div class="progress" id="progressBar" style="display:none;">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                      <span class="progress-text">0%</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="col-xs-3 btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <t:i18n message="Save" />
                        </button>
                    </div>
                </div>
            </form>
        </div>                 

        
        <div class="col-xs-12 admin-right-content"> 
            <h3><i class="fa fa-angle-right"></i> <t:i18n message="Picture" /></h3>
            <div class="col-xs-10 admin-content-wrapper thumbnail-wrapper">
                <div class="col-lg-3 col-md-4 col-xs-6">
                    <c:choose>
                        <c:when test="${empty product.thumbnail}">
                        </c:when>
                        <c:otherwise>
                            <a class="thumbnail" href="#" data-id="${product.productId}" data-toggle="modal" data-target="#modalImage">
                                <img class="img-responsive" src="/management/product/getPicture/${product.productId}" alt="">
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>    
        </div>    
          
        <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <t:i18n message="Photo preview" />
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="modalImg center-block" src="" alt="" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
            
    </jsp:attribute>
</t:simple_layout>