<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Photographer" >    
    <jsp:attribute name="body">
        <h1><t:i18n message="Photographer" /></h1>

        <form method="post">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><t:i18n message="FirstName" /></th>
                        <th><t:i18n message="surName" /></th>
                        <th><t:i18n message="GroupPrice" /></th>
                        <th><t:i18n message="PortraitPrice" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td><c:out value="${photographer.user.firstName}"/> </td>
                        <td><c:out value="${photographer.user.surName}"/> </td>
                        <td><c:out value="${photographer.groupPrice}" /></td>
                        <td><c:out value="${photographer.portraitPrice}" /></td>

                        <td>
                            <a class="btn btn-primary btn-xs" title="<t:i18n message="Edit" />"
                               href="/management/photographerSingle/edit/<c:out value="${sessionScope.loggedInPhotographer.getPhotographerId()}" />">
                                <i class="fa fa-edit" ></i>
                                <t:i18n message="Edit" />
                            </a>
                        </td>
                        <td>
                            <button class="btn btn-danger btn-xs " type="submit" 
                                    name="photographerId" value="<c:out value="${photographer.photographerId}" />">
                                <i class="fa fa-trash-o" ></i>
                                <t:i18n message="Delete" />
                            </button>
                        </td>
                    </tr>

                </tbody>
            </table>
                            
<!--            <a href="/management/photographer/add" class="btn btn-success" title="<t:i18n message="Add photographer" />">
                <i class="fa fa-plus"></i>
                <t:i18n message="Add photographer" />
            </a>-->
        </form>
    </jsp:attribute>

</t:simple_layout>