<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Inloggen" >    
    <jsp:attribute name="body">
        <h1><t:i18n message="Products" /></h1>

        <c:forEach items="${productList}" var="product">
            <div class="col-xs-3">
                <div class="col-xs-12">
                    <img src="/images/products/${product.filename}" alt="${product.filename}" />
                </div>
                <span><c:out value="${product.description}" /></span>
                <span><c:out value="${product.printPrice}" /></span>
            </div>
        </c:forEach>
    </jsp:attribute>
</t:simple_layout>