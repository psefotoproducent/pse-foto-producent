<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Photographers" >    
    <jsp:attribute name="body">
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Photographers" /></h3>

        <a class="add-btn btn btn-primary" href="/management/photographer/add" role="button">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <t:i18n message="Add photographer" />
            </a>
        
        <div class="row mt">
            <div class="col-md-12">
                <div class="content-panel">
                    <form method="post">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                            <tr>
                                <th><t:i18n message="Firstname" /></th>
                                <th><t:i18n message="Insertion" /></th>
                                <th><t:i18n message="Lastname" /></th>
                                <th><t:i18n message="Group price" /></th>
                                <th><t:i18n message="Portrait price" /></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${photographerList}" var="photographer">
                                    <tr>
                                        <td><a href="/management/photographer/edit/<c:out value="${photographer.photographerId}" />"><c:out value="${photographer.user.firstName}" /></a></td>
                                        <td><a href="/management/photographer/edit/<c:out value="${photographer.photographerId}" />"><c:out value="${photographer.user.insertion}" /></a></td>
                                        <td><a href="/management/photographer/edit/<c:out value="${photographer.photographerId}" />"><c:out value="${photographer.user.surName}" /></a></td>
                                        <td><a href="/management/photographer/edit/<c:out value="${photographer.photographerId}" />">&euro; <c:out value="${photographer.groupPrice}" /></a></td>
                                        <td><a href="/management/photographer/edit/<c:out value="${photographer.photographerId}" />">&euro; <c:out value="${photographer.portraitPrice}" /></a></td>
                                        
                                        <td>
                                            <a class="btn btn-primary" href="/management/photographer/edit/<c:out value="${photographer.photographerId}" />" role="button">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                            </a>

                                            <button class="btn btn-danger" type="submit" name="photographerId" value="<c:out value="${photographer.photographerId}" />">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div><!-- /content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- /row -->
     
    </jsp:attribute>

</t:simple_layout>

