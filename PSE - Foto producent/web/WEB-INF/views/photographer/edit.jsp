<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Add a Photographer" >    
    <jsp:attribute name="body">
        
        <h3>
            <i class="fa fa-angle-right"></i> 
            <c:choose>
                <c:when test="${photographer.photographerId > 0}">
                    <t:i18n message="Edit photographer" />
                </c:when>    
                <c:otherwise>
                    <t:i18n message="Add new photographer" />
                </c:otherwise>
            </c:choose>
        </h3>
        
        <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <label for="username" class="col-sm-2 control-label"><t:i18n message="Username" /></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="username" id="username" value="${photographer.user.username}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label"><t:i18n message="Password" /></label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password" id="password" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstname" class="col-sm-2 control-label"><t:i18n message="Firstname" /></label>
                    <div class="col-sm-8">
                        <input type="text"  class="form-control" name="firstname" id="firstName" value="${photographer.user.firstName}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="insertion" class="col-sm-2 control-label"><t:i18n message="Insertion" /></label>
                    <div class="col-sm-8">
                        <input type="text"  class="form-control" name="insertion" id="insertion" value="${photographer.user.insertion}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="surname" class="col-sm-2 control-label"><t:i18n message="Surname" /></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="surname" id="surName" value="${photographer.user.surName}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="housenumber" class="col-sm-2 control-label"><t:i18n message="Housenumber" /></label>
                    <div class="col-sm-8">
                        <input type="number"  class="form-control" name="housenumber" id="housenumber" value="${photographer.user.houseNumber}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="postalcode" class="col-sm-2 control-label"><t:i18n message="Postalcode" /></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="postalcode" id="postalcode" value="${photographer.user.postalCode}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label"><t:i18n message="City" /></label>
                    <div class="col-sm-8">
                        <input type="text"  class="form-control" name="city" id="city" value="${photographer.user.city}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="phonenumber" class="col-sm-2 control-label"><t:i18n message="Phonenumber" /></label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control"  name="phonenumber" id="phonenumber" value="${photographer.user.phoneNumber}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="portraitPrice" class="col-sm-2 control-label"><t:i18n message="portraitPrice" /></label>
                    <div class="col-sm-8">
                        <input type="number"  class="form-control" name="portraitPrice" id="portraitPrice" value="${photographer.portraitPrice}"  required/>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="groupPrice" class="col-sm-2 control-label"><t:i18n message="groupPrice" /></label>
                    <div class="col-sm-8">
                        <input type="number"  class="form-control" name="groupPrice" id="groupPrice" value="${photographer.groupPrice}"  required/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="col-xs-3 btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <t:i18n message="Save" />
                        </button>
                    </div>
                </div>
              
            </form>
        </div>

    </jsp:attribute>
</t:simple_layout>