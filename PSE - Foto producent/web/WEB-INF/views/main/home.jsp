<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%     
    if (session.getAttribute("loggedInEmployee") != null) { %>
    <jsp:include page="/WEB-INF/views/ui/includes/employee-navmenu.jsp" />
    <% }
%>    
<t:simple_layout title="Success!" >    
    <jsp:attribute name="body">
       
            <c:if test="${sessionScope.loggedInPhotographer != null}">
                <h3><i class="fa fa-angle-right"></i> <t:i18n message="Welcome Photographer!" /></h3>
                
                
                <h3></i><i class="fa fa-photo"></i> <t:i18n message="Photosessions" /></h3>
                
                <span><t:i18n message="Use the 'Photosessions' tab to add new photosessions for your clients" /></span><br><br>
                
                <p><img src="/resources/img/uploadExample.PNG" class="img-thumbnail" alt="Cinque Terre" width="500"/></p>
                
                <h3></i><i class="fa fa-cogs"></i> <t:i18n message="Settings" /></h3>
                
                <span><t:i18n message="Use the 'Settings' tab to adjust your personal info such as password, portrait price etc." /></span><br><br>
                                
                <p><img src="/resources/img/editExample.PNG"class="img-thumbnail" alt="Cinque Terre" width="500"/></p>
            </c:if>        
    </jsp:attribute>
</t:simple_layout>