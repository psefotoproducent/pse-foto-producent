<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Language strings">
    <jsp:attribute name="body">
        <div class="row">
            <h3 class="col-xs-8"><i class="fa fa-angle-right"></i> <t:i18n message="Translations" /></h3>
        
            <div class="col-xs-12">
                <div class="btn-group pull-right">
                    <a class="btn btn-primary" href="/i18n-management/en">EN</a>
                    <a class="btn btn-primary" href="/i18n-management/nl">NL</a>
                </div>
            </div>
        </div>
        
        <div class="row mt">
            <div class="col-md-12">
                
                <div class="content-panel">
                    <form method="post">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                            <tr>
                                <th><t:i18n message="Source" /></th>
                                <th class="col-xs-8"><t:i18n message="Translation" /></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${messageList}" var="message">
                                    <tr>
                                        <td><c:out value="${message.getSource()}" /></td>
                                        <td>
                                            <input class="col-xs-12" type="text" value="<c:out value="${message.getTranslation()}" />" data-id="<c:out value="${message.getInternationalizationId()}" />" />
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div><!-- /content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- /row -->
        <script>
            $('form input').on('change', function() {
                $.ajax({
                    headers: { 
                        'Accept': 'application/json',
                        'Content-Type': 'application/json' 
                    },
                    'type': 'POST',
                    'url': '/i18n-management/save',
                    'data': JSON.stringify({
                        id: $(this).data('id'),
                        translation: $(this).val()
                    }),
                    'dataType': 'json',
                    'success': function(result) {console.log(result)}
                });
            });
        </script> 
    </jsp:attribute>
</t:simple_layout>