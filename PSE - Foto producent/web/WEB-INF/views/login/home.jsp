<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:homepage_layout title="Homepage" >    
    <jsp:attribute name="body">

        <form class="form-login" action="login" method="POST">
            <h2 class="form-login-heading">sign in now</h2>
            <div class="pull-right">
                <span onclick="$.ajax({
                    headers: { 
                        'Accept': 'application/json',
                        'Content-Type': 'application/json' 
                    },
                    'type': 'POST',
                    'url': 'i18n/set/en',
                    'data': {},
                    'dataType': 'json',
                    'success': function(result) {location.reload();}
                });">EN</span>&nbsp;
                <span onclick="$.ajax({
                    headers: { 
                        'Accept': 'application/json',
                        'Content-Type': 'application/json' 
                    },
                    'type': 'POST',
                    'url': 'i18n/set/nl',
                    'data': {},
                    'dataType': 'json',
                    'success': function(result) {location.reload();}
                });">NL</span>&nbsp;
            </div>
            <div class="login-wrap">
                <p>
                    <t:i18n message="Sign in using your username and password" />
                    <label for="error"><font color="red">${errorLogin}</font></label> 
                </p>
                <form action="login" method="POST">
                    <input type="text" class="form-control" placeholder="<t:i18n message="Username" />" name="username" autofocus>
                    <br>
                    <input type="password" class="form-control" placeholder="<t:i18n message="Password" />" name="password">
                    <br>
                    <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> <t:i18n message="SIGN IN" /></button>
                </form>
                
                <hr>
                
                <div class="login-social-link centered">
                    <p>
                        <t:i18n message="or you can sign in using a logincode" />
                        <label for="error"><font color="red">${errorLogincode}</font></label> 
                    </p>
                    
                    <form action="loginClient" method="POST">
                        <input type="text" class="form-control" placeholder="<t:i18n message="Logincode" />" name="loginCode" autofocus>
                        <br>
                        <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> <t:i18n message="SIGN IN" /></button>
                    </form>
                </div>
            </div>
        </form>

    </jsp:attribute>
</t:homepage_layout> 