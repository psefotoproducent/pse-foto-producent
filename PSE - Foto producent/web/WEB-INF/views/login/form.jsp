<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<%
    request.setAttribute("testHtml", "<i class='fa fa-refresh fa-spin'></i>");
%>

<t:simple_layout title="Inloggen" >    
      <jsp:attribute name="body">
        
        <p><font color="red">${errorMessage}</font></p>
        <form action="./login" method="POST">
            <label>
                <t:i18n message="Username" />
                ${testHtml}
                <input type="text" name="username" value="${exampleEntity.name}">
            </label>
            <label>
                <t:i18n message="password" />
                <input type="password" name="password" value="${exampleEntity.age}">
            </label>

            <input type="submit">
        </form>

        <table>
            <thead>
                <tr>
                    <td><t:i18n message="name" /></td>
                    <td><t:i18n message="age" /></td>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${exampleEntityList}" var="current">
                    <tr>
                        <td><c:out value="${current.name}" /><td>
                        <td><c:out value="${current.age}" /><td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </jsp:attribute>
</t:simple_layout>