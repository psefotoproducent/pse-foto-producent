<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%     
    if (session.getAttribute("loggedInEmployee") != null) { %>
    <jsp:include page="/WEB-INF/views/ui/includes/employee-navmenu.jsp" />
    <% } else if (session.getAttribute("loggedInPhotographer") != null) { %>
    <jsp:include page="/WEB-INF/views/ui/includes/photographer-navmenu.jsp" />
    <% } else if (session.getAttribute("loggedInClient") != null) { %>
    <jsp:include page="/WEB-INF/views/ui/includes/client-navmenu.jsp" />
    <% } 
%>    