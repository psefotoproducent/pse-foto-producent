<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<ul class="sidebar-menu" id="nav-accordion">

    <p class="centered"><a href="#"><img src="<c:url value="/resources/img/ui-sam.jpg" />" class="img-circle" width="60"></a></p>
    <h5 class="centered"><t:i18n message="Welcome Employee!" /></h5>
    
<!--    <form action="/search" method="get">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" class="form-control input-lg" name="searchText" id="searchText" placeholder="<t:i18n message="Search photo..."/>" />
                <span class="input-group-btn">
                    <button class="btn btn-info btn-lg" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </form>-->

    <li class="sub-menu">
        <a href="/home" class="<c:if test="${requestScope['javax.servlet.forward.request_uri'] == '/home'}">active</c:if>">
            <i class="fa fa-home"></i>
            <span><t:i18n message="Dashboard" /></span>
        </a>
    </li>
    
    <li class="sub-menu">
        <a href="/management/product" class="<c:if test="${requestScope['javax.servlet.forward.request_uri'] == '/management/product'}">active</c:if>">
            <i class="fa fa-shopping-cart"></i>
            <span><t:i18n message="Products" /></span>
        </a>
    </li>
    
    <li class="sub-menu">
        <a href="/order/management/productlist" >
            <i class="fa fa-th-list"></i>
            <span><t:i18n message="Orders" /></span>
        </a>
    </li>
    
    <li class="sub-menu">
        <a href="/employee/management" class="<c:if test="${requestScope['javax.servlet.forward.request_uri'] == '/employee/management'}">active</c:if>">
            <i class="fa fa-building"></i>
            <span><t:i18n message="Employees" /></span>
        </a>
    </li>
    
    <li class="sub-menu">
        <a href="/photographer/management" class="<c:if test="${requestScope['javax.servlet.forward.request_uri'] == '/photographer/management'}">active</c:if>">
            <i class="fa fa-users"></i>
            <span><t:i18n message="Photographers" /></span>
        </a>
    </li>
    
    <li class="sub-menu">
        <a href="/i18n-management/en" class="<c:if test="${requestScope['javax.servlet.forward.request_uri'] == '/i18n-management'}">active</c:if>">
            <i class="fa fa-cogs"></i>
            <span><t:i18n message="Translations" /></span>
        </a>
    </li>

</ul>