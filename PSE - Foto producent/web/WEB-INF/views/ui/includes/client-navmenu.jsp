<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<ul class="sidebar-menu" id="nav-accordion">

    <p class="centered"><a href="#"><img src="<c:url value="/resources/img/ui-sam.jpg" />" class="img-circle" width="60"></a></p>
    <h5 class="centered"><t:i18n message="Welcome Customer!" /></h5>

    <li class="sub-menu">
        
        <% if (session.getAttribute("clientSpecificSession").equals(true)) { %>
            <a href="/order/overview/${sessionScope.photoSessionId}/${sessionScope.clientCode}" class="<c:if test="${requestScope['javax.servlet.forward.request_uri']} == '/order/overview/${sessionScope.photoSessionId}/${sessionScope.clientCode}'">active</c:if>">
                <i class="fa fa-home"></i>
                <span><t:i18n message="Order" /></span>
            </a> 
        <% } else { %>
            <a href="/order/overview/${sessionScope.photoSessionId}" class="<c:if test="${requestScope['javax.servlet.forward.request_uri']} == '/order/overview/${sessionScope.photoSessionId}'">active</c:if>">
                <i class="fa fa-home"></i>
                <span><t:i18n message="Order" /></span>
            </a>
        <% } %>
    </li>
    
    <li class="sub-menu">
        <a href="/order/shoppingcart" class="<c:if test="${requestScope['javax.servlet.forward.request_uri'] == '/order/shoppingcart'}">active</c:if>">
            <i class="fa fa-shopping-cart"></i>
            <span><t:i18n message="Shoppingcart" /></span>
        </a>
    </li>
    
</ul>