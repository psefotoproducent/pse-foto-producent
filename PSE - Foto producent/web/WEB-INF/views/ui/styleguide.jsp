<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Styleguide">    
    <jsp:attribute name="body">
        <div class="col-xs-12 admin-right-content">
            <h3 class="col-xs-10 col-xs-push-1">Typography</h3>
            <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">
                <h1>This is a heading 1.</h1>
                <h2>This is a heading 2.</h2>
                <h3>This is a heading 3.</h3>
                <h4>This is a heading 4.</h4> 
                <h5>This is a heading 5.</h5>
                <h6>This is a heading 6.</h6>
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                    nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
                    dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
                    sunt in culpa qui officia deserunt mollit anim id est laborum."
                </p>
            </div> 
        </div>
        <!-- End Typography -->

        <!-- Buttons -->
        <div class="col-xs-12 admin-right-content">
            <h3 class="col-xs-10 col-xs-push-1">Buttons</h3>
            <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">

                <!-- default bootstrap buttons -->
                <button type="button" class="btn btn-default">Default</button>
                <button type="button" class="btn btn-primary">Primary</button>
                <button type="button" class="btn btn-success">Success</button>
                <button type="button" class="btn btn-info">Info</button>
                <button type="button" class="btn btn-warning">Warning</button>
                <button type="button" class="btn btn-danger">Danger</button>

                <div class="clear"></div>
                <div class="gap"></div>

                <!-- buttons with icons -->
                <button type="button" class="btn btn-labeled btn-success">
                    <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>
                    <span class="btn-text">Success</span>
                </button>
                <button type="button" class="btn btn-labeled btn-danger">
                    <span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>
                    <span class="btn-text">Cancel</span>
                </button>
                <button type="button" class="btn btn-labeled btn-warning">
                    <span class="btn-label"><i class="glyphicon glyphicon-bookmark"></i></span>
                    <span class="btn-text">Bookmark</span>
                </button>
                <button type="button" class="btn btn-labeled btn-primary">
                    <span class="btn-label"><i class="glyphicon glyphicon-camera"></i></span>
                    <span class="btn-text">Camera</span>
                </button>
                <button type="button" class="btn btn-labeled btn-default">
                    <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>
                    <span class="btn-text">Left</span>
                </button>
                <button type="button" class="btn btn-labeled btn-default">
                    <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
                    <span class="btn-text">Right</span>
                </button>

                <div class="clear"></div>
                <div class="gap"></div>    

                <button type="button" class="btn btn-labeled btn-success">
                    <span class="btn-label"><i class="glyphicon glyphicon-thumbs-up"></i></span>
                    <span class="btn-text">Thumbs Up</span>
                </button>
                <button type="button" class="btn btn-labeled btn-danger">
                    <span class="btn-label"><i class="glyphicon glyphicon-thumbs-down"></i></span>
                    <span class="btn-text">Thumbs Down</span>
                </button>
                <button type="button" class="btn btn-labeled btn-info">
                    <span class="btn-label"><i class="glyphicon glyphicon-refresh"></i></span>
                    <span class="btn-text">Refresh</span>
                </button>
                <button type="button" class="btn btn-labeled btn-danger">
                    <span class="btn-label"><i class="glyphicon glyphicon-trash"></i></span>
                    <span class="btn-text">Trash</span>
                </button>

                <div class="clear"></div>
                <div class="gap"></div>    

                <button type="button" class="btn btn-labeled btn-success">
                    <span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>
                    <span class="btn-text">Add</span>
                </button>
                <button type="button" class="btn btn-labeled btn-warning">
                    <span class="btn-label"><i class="glyphicon glyphicon-pencil"></i></span>
                    <span class="btn-text">Edit</span>
                </button>
                <button type="button" class="btn btn-labeled btn-danger">
                    <span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>
                    <span class="btn-text">Remove</span>
                </button>
                <button type="button" class="btn btn-labeled btn-primary">
                    <span class="btn-label"><i class="glyphicon glyphicon-eye-open"></i></span>
                    <span class="btn-text">View</span>
                </button>

            </div>
        </div>
        <!-- END Buttons -->

        <!-- GLYPHICONS -->
        <div class="col-xs-12 admin-right-content">
            <h3 class="col-xs-10 col-xs-push-1">Glyphicons</h3>
            <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">         
                <div class="bt-glyphicons">
                    <ul class="bt-glyphicons-list">

                        <li>
                            <span class="glyphicon glyphicon-asterisk"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-asterisk</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-plus"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-plus</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-euro"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-euro</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-minus"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-minus</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-cloud"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-cloud</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-envelope"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-envelope</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-pencil"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-pencil</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-glass"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-glass</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-music"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-music</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-search"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-search</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-heart"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-heart</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-star"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-star</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-star-empty"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-star-empty</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-user"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-user</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-film"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-film</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-th-large"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-th-large</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-th"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-th</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-th-list"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-th-list</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-ok"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-ok</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-remove"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-remove</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-zoom-in"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-zoom-in</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-zoom-out"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-zoom-out</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-off"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-off</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-signal"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-signal</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-cog"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-cog</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-trash"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-trash</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-home"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-home</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-file"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-file</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-time"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-time</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-road"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-road</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-download-alt"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-download-alt</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-download"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-download</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-upload"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-upload</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-inbox"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-inbox</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-play-circle"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-play-circle</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-repeat"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-repeat</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-refresh"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-refresh</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-list-alt"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-list-alt</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-lock"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-lock</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-flag"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-flag</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-headphones"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-headphones</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-volume-off"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-volume-off</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-volume-down"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-volume-down</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-volume-up"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-volume-up</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-qrcode"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-qrcode</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-barcode"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-barcode</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-tag"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-tag</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-tags"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-tags</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-book"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-book</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-bookmark"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-bookmark</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-print"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-print</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-camera"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-camera</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-font"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-font</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-bold"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-bold</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-italic"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-italic</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-text-height"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-text-height</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-text-width"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-text-width</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-align-left"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-align-left</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-align-center"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-align-center</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-align-right"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-align-right</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-align-justify"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-align-justify</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-list"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-list</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-indent-left"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-indent-left</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-indent-right"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-indent-right</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-facetime-video"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-facetime-video</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-picture"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-picture</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-map-marker"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-map-marker</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-adjust"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-adjust</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-tint"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-tint</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-edit"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-edit</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-share"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-share</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-check"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-check</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-move"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-move</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-step-backward"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-step-backward</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-fast-backward"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-fast-backward</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-backward"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-backward</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-play"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-play</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-pause"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-pause</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-stop"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-stop</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-forward"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-forward</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-fast-forward"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-fast-forward</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-step-forward"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-step-forward</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-eject"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-eject</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-chevron-left"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-chevron-left</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-chevron-right</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-plus-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-plus-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-minus-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-minus-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-remove-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-remove-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-ok-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-ok-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-question-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-question-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-info-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-info-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-screenshot"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-screenshot</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-remove-circle"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-remove-circle</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-ok-circle"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-ok-circle</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-ban-circle"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-ban-circle</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-arrow-left"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-arrow-left</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-arrow-right"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-arrow-right</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-arrow-up"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-arrow-up</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-arrow-down"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-arrow-down</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-share-alt"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-share-alt</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-resize-full"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-resize-full</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-resize-small"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-resize-small</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-exclamation-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-exclamation-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-gift"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-gift</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-leaf"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-leaf</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-fire"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-fire</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-eye-open"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-eye-open</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-eye-close"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-eye-close</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-warning-sign"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-warning-sign</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-plane"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-plane</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-calendar"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-calendar</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-random"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-random</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-comment"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-comment</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-magnet"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-magnet</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-chevron-up"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-chevron-up</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-chevron-down"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-chevron-down</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-retweet"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-retweet</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-shopping-cart"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-shopping-cart</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-folder-close"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-folder-close</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-folder-open"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-folder-open</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-resize-vertical"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-resize-vertical</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-resize-horizontal"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-resize-horizontal</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-hdd"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-hdd</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-bullhorn"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-bullhorn</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-bell"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-bell</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-certificate"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-certificate</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-thumbs-up"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-thumbs-up</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-thumbs-down"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-thumbs-down</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-hand-right"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-hand-right</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-hand-left"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-hand-left</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-hand-up"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-hand-up</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-hand-down"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-hand-down</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-circle-arrow-right"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-right</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-circle-arrow-left"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-left</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-circle-arrow-up"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-up</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-circle-arrow-down"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-down</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-globe"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-globe</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-wrench"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-wrench</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-tasks"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-tasks</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-filter"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-filter</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-briefcase"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-briefcase</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-fullscreen"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-fullscreen</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-dashboard"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-dashboard</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-paperclip"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-paperclip</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-heart-empty"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-heart-empty</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-link"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-link</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-phone"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-phone</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-pushpin"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-pushpin</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-usd"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-usd</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-gbp"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-gbp</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sort"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sort</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sort-by-alphabet"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sort-by-alphabet</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sort-by-alphabet-alt"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sort-by-alphabet-alt</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sort-by-order"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sort-by-order</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sort-by-order-alt"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sort-by-order-alt</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sort-by-attributes"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sort-by-attributes</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sort-by-attributes-alt"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sort-by-attributes-alt</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-unchecked"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-unchecked</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-expand"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-expand</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-collapse-down"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-collapse-down</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-collapse-up"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-collapse-up</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-log-in"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-log-in</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-flash"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-flash</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-log-out"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-log-out</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-new-window"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-new-window</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-record"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-record</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-save"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-save</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-open"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-open</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-saved"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-saved</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-import"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-import</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-export"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-export</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-send"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-send</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-floppy-disk"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-floppy-disk</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-floppy-saved"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-floppy-saved</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-floppy-remove"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-floppy-remove</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-floppy-save"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-floppy-save</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-floppy-open"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-floppy-open</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-credit-card"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-credit-card</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-transfer"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-transfer</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-cutlery"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-cutlery</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-header"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-header</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-compressed"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-compressed</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-earphone"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-earphone</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-phone-alt"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-phone-alt</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-tower"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-tower</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-stats"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-stats</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sd-video"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sd-video</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-hd-video"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-hd-video</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-subtitles"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-subtitles</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sound-stereo"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sound-stereo</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sound-dolby"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sound-dolby</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sound-5-1"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sound-5-1</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sound-6-1"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sound-6-1</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-sound-7-1"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-sound-7-1</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-copyright-mark"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-copyright-mark</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-registration-mark"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-registration-mark</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-cloud-download"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-cloud-download</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-cloud-upload"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-cloud-upload</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-tree-conifer"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-tree-conifer</span>
                        </li>

                        <li>
                            <span class="glyphicon glyphicon-tree-deciduous"></span><br/>
                            <span class="glyphicon-class">glyphicon glyphicon-tree-deciduous</span>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- END GLYPHICONS -->

        <!-- FA-ICONS -->
        <div class="col-xs-12 admin-right-content">
            <h3 class="col-xs-10 col-xs-push-1">FA Icons</h3>
            <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">         
                <div class="bt-glyphicons">
                    <ul class="bt-glyphicons-list">

                        TODO : REST
                        <li>
                            <span class="fa fa-dashboard"></span>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- END FA-ICONS -->

        <!-- INPUT FIELDS -->
        <div class="col-xs-12 admin-right-content">
            <h3 class="col-xs-10 col-xs-push-1">Input Fields</h3>
            <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">         
                <form>

                    <!-- input field -->
                    <div class="form-group">
                        <label for="example">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    </div>

                    <!-- checkbox -->
                    <label for="example">Checkbox</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Check me out
                        </label>
                    </div>

                    <!-- textarea -->
                    <label for="example">Textarea</label>
                    <textarea class="form-control" rows="3"></textarea>

                    <!-- radio buttons -->
                    <label for="example">Radio Buttons</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                            Option one is this and that&mdash;be sure to include why it's great
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            Option two can be something else and selecting it will deselect option one
                        </label>
                    </div>

                    <!-- dropdown -->
                    <label for="example">Dropdown List</label>
                    <select class="form-control">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>

                    <label for="example">Validation Text</label>
                    <p class="text-muted">Enter your message...</p>
                    <p class="text-primary">Enter your message...</p>
                    <p class="text-success">Enter your message...</p>
                    <p class="text-info">Enter your message...</p>
                    <p class="text-warning">Enter your message...</p>
                    <p class="text-danger">Enter your message...</p>

                    <p class="bg-primary">Enter your message...</p>
                    <p class="bg-success">Enter your message...</p>
                    <p class="bg-info">Enter your message...</p>
                    <p class="bg-warning">Enter your message...</p>
                    <p class="bg-danger">Enter your message...</p>

                </form>
            </div>
        </div>
        <!-- END INPUT FIELDS -->

        <!-- EXTRA -->
        <div class="col-xs-12 admin-right-content">
            <h3 class="col-xs-10 col-xs-push-1">Extra</h3>
            <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">         
                ... todo     
            </div>
        </div>
        <!-- END EXTRA -->
    </jsp:attribute>
        
</t:simple_layout>