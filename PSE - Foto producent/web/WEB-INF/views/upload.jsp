<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Photo upload" >    
    <jsp:attribute name="body">     
        <script>
            $(function () {
                $('#fileupload').fileupload({
                    dataType: 'json',
                    
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progressBar .progress-bar').css(
                            'width',
                            progress + '%'
                        );
                
                        $('#progressBar .progress-text').text(progress + '%');
                
                        if (data.loaded < data.total) {
                            $('#progressBar').css(
                                'display', 'block'
                            );
                        } else {
                            $('#progressBar').css(
                                'display', 'none'
                            );
                        }
                    },
                    
                    done: function (e, data) {
//                        $.each(data.result, function (index, file) {
//                            $(".list-group").append(
//                                "<a href='getPicture/"+file.fileName+"' class='list-group-item list-group-item-success'><span class='badge alert-success pull-right'>Success</span></a>"
//                            );
//                        }); 

                        location.reload();
                    },
                    
                    dropZone: $('#dropzone')
                });
                
                $('#dropzone').on('dragover dragenter', function() {
                    $('#dropzone').addClass('drop');
                })
                .on('dragleave dragend drop', function() {
                    $('#dropzone').removeClass('drop');
                });
                
                $('.thumbnail').on('click', function (event) {
                    var id = $(this).data('id');
                    
                    $('.modalImg').attr('src', '/photosession/getPicture/' + id);
                    $('.printImgGray').attr('href', '/print/grayscale/' + id);
                    $('.printImgSepia').attr('href', '/print/sepia/' + id);
                    
                    $('.deleteImg').on('click', function() {
                       $.post('/photosession/deletePicture/' + id, function( data ) {
                           $('.img' + id).remove();
                           $('#modalImage').modal('toggle');
                      }); 
                    });
                });
            });
        </script>

        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Upload photos" /> ${photoSession.description}</h3>
        
        <h4><t:i18n message="Drag and drop files below" /></h4>
        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="form-inline" style="display:none;">
                <div class="form-group">
                    <input id="fileupload" type="file" name="files[]" data-url="${photoSession.photoSessionId}" multiple>
                </div>
            </div>
                
            <div class="form-group">
                <label for="photoType" class="col-sm-offset-1 col-sm-1 control-label"><t:i18n message="Phototype" /></label>
                <div class="col-sm-2">
                    <select class="form-control" id="photoType" name="photoType">
                        <option value="1"><t:i18n message="Group" /></option>
                        <option value="2"><t:i18n message="Portrait" /></option>
                    </select>
                </div>
            </div>
                
            <div class="form-group">
                <label for="price" class="col-sm-offset-1 col-sm-1 control-label"><t:i18n message="Price" /></label>
                <div class="col-sm-2">
                    <input type="number" name="price" class="form-control" id="price" value="0" />
                </div>
            </div>
                
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="upload-drop-zone" id="dropzone">
                        <t:i18n message="Just drag and drop a file here" />
                    </div>
                </div>
            </div>
        </form>

        <div class="progress" id="progressBar" style="display:none;">
            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">
              <span class="progress-text">0%</span>
            </div>
        </div>
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Processed files" /></h3>
        <div class="row">
            <c:forEach items="${photos}" var="photo">
                <div class="col-lg-1 col-md-4 col-xs-6 thumb img${photo.photoId}">
                    <a class="thumbnail" href="#" data-id="${photo.photoId}" data-toggle="modal" data-target="#modalImage">
                        <img class="img-responsive" src="getPicture/${photo.photoId}" alt="">
                    </a>
                </div>
            </c:forEach>
        </div>
        
        <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <t:i18n message="Photo preview" />
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="modalImg center-block" src="" alt="" />
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-success printImgGray" href="#" target="_blank">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print grayscale
                        </a>
                        
                        <a class="btn btn-success printImgSepia" href="#" target="_blank">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print sepia
                        </a>
                        
                        <a class="btn btn-danger deleteImg" href="#">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                        </a>
                        
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
            
    </jsp:attribute>
</t:simple_layout>