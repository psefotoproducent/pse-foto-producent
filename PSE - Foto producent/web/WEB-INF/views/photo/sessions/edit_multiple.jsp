<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Photo upload" >    
    <jsp:attribute name="body">   
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Add multiple photosessions" /></h3>

        <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">
            <form method="post" class="form-horizontal">
                <input type="hidden" name="photographerId" id="photographerId" value ="${photographerId}" />

                <div class="form-group">
                    <label for="sessioncount" class="col-sm-2 control-label"><t:i18n message="Sessioncount" /></label>
                    <div class="col-sm-2">
                        <input type="number" name="sessioncount" class="form-control" id="sessioncount" value="10" />
                    </div>
                    <label class="control-label"><t:i18n message="sessions" /></label>
                </div>
                
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label"><t:i18n message="Description" /></label>
                    <div class="col-sm-10">
                        <input type="text" name="description" class="form-control" id="description" value="${photosession.description}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="organisation" class="col-sm-2 control-label"><t:i18n message="Organisation" /></label>
                    <div class="col-sm-10">
                        <input type="text" name="organisation" class="form-control" id="organisation" value="${photosession.organisation}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="organisationStreet" class="col-sm-2 control-label"><t:i18n message="Address" /></label>
                    <div class="col-sm-8">
                        <input type="text" name="organisationStreet" class="form-control" id="organisationStreet" value="${photosession.organisationStreet}" />
                    </div>

                    <div class="col-sm-2">
                        <input type="text" name="organisationHouseNumber" class="form-control" id="organisationHouseNumber" value="${photosession.organisationHouseNumber}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="organisationPostalCode" class="col-sm-2 control-label"><t:i18n message="Postal code and city" /></label>
                    <div class="col-sm-3">
                        <input type="text" name="organisationPostalCode" class="form-control" id="organisationPostalCode" value="${photosession.organisationPostalCode}" />
                    </div>

                    <div class="col-sm-7">
                        <input type="text" name="organisationCity" class="form-control" id="organisationCity" value="${photosession.organisationCity}" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <t:i18n message="Generate sessions" />
                        </button>
                        <a class="btn btn-danger" href="/photosession" role="button">
                            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> <t:i18n message="Go back" />
                        </a>
                    </div>                    
                </div>
            </form>
        </div>
    </jsp:attribute>
</t:simple_layout>