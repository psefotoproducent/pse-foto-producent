<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Photosessions" >    
    <jsp:attribute name="body">   
        <script>
            $(function () {
                
                $('.modalImgLink').on('click', function () {
                    var id = $(this).data('id');
                    
                    $.ajax({                        
                        type: "POST",
                        url: "/photosession/getLoginCodes/" + id,
                        success: function (data) {
                            
                            // Only true if clientCodes are being used
                            if (data.indexOf('-') > -1) { 
                                
                                var clientsAndLoginCode = data.split(":");
                                var loginCode = clientsAndLoginCode[1].trim();
                                
                                var clientsAndCodes = clientsAndLoginCode[0].split(",");
                                
                                var modalContent = "";
                                
                                for (i = 0; i < clientsAndCodes.length; i++) { 
                                    var clientAndCode = clientsAndCodes[i].split("-");
                                    modalContent = modalContent.concat("Logincode for <b>" + clientAndCode[0] + "</b>: " + loginCode + "-" + clientAndCode[1] + "<br>");
                                }
                                
                                $(".modal-body").html(modalContent);
                            } else {
                                $(".modal-body").html("Logincode for this session: <b>" + data + "</b>");
                            }
                        }
                    });
                });
                
            });
                
        </script>
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Photosessions" /></h3>
        
        <a class="btn btn-primary" href="/management/photosession/add" role="button">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <t:i18n message="Add photosession" />
        </a>
        
        <a class="btn btn-warning" href="/management/photosession/addBuffer" role="button">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <t:i18n message="Add multiple photosessions" />
        </a>
        
        <div class="row mt">
            <div class="col-md-12">
                <div class="content-panel">
                    <form method="post" action="/management/photosession/delete">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                                <tr>
                                    <th style="width:300px;"><t:i18n message="Description" /></th>
                                    <th style="width:300px;"><t:i18n message="Logincode" /></th>                        
                                    <th style="width:100px;"><t:i18n message="Date" /></th>
                                    <th style="width:300px;"><t:i18n message="Organisation" /></th>
                                    <th><t:i18n message="Address" /></th>
                                    <th style="width:150px;"><t:i18n message="Actions" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${sessionlist}" var="photoSession">
                                    <tr>
                                        <td>
                                            <a href="/photosession/${photoSession.photoSessionId}"><c:out value="${photoSession.description}" /></a>
                                        </td>
                                        <td>                                           
                                            
                                            <a class="modalImgLink" href="#" data-id="${photoSession.photoSessionId}" data-toggle="modal" data-target="#modalCodes">
                                                <span class="btn btn-primary btn-sm">
                                                    Click here for logincodes <input data-url="${photoSession.photoSessionId}" style="display: none;" >
                                                </span>
                                            </a>
                                        </td>                            
                                        <td>
                                            <a href="/photosession/${photoSession.photoSessionId}"><c:out value="${photoSession.date}" /></a>
                                        </td>
                                        <td>
                                            <a href="/photosession/${photoSession.photoSessionId}"><c:out value="${photoSession.organisation}" /></a>
                                        </td>
                                        <td>
                                            <a href="/photosession/${photoSession.photoSessionId}"><c:out value="${photoSession.getAddress()}" /></a>
                                        </td>
                                        <td>
                                            <a class="btn btn-info" href="/photosession/${photoSession.photoSessionId}" role="button">
                                                <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                                            </a>

                                            <a class="btn btn-primary" href="/management/photosession/edit/${photoSession.photoSessionId}" role="button">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                            </a>

                                            <button class="btn btn-danger" type="submit" name="photoSessionId" value="<c:out value="${photoSession.photoSessionId}" />">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <!--NEEDS MORE WORK FOR CLIENTLOGINCODES-->
                                    
                                    <!--Enlarged pop-up screen with loginCodes-->

                                    <div class="modal fade" id="modalCodes" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">

                                                <!--header-->
                                                <div class="modal-header">
                                                    <t:i18n message="Available logincodes" />
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                
                                                <!--content-->
                                                <div class="modal-body">
<!--                                                    <t:i18n message="Logincode for all photo's:&nbsp;" />
                                                    <b>
                                                        <c:out value="${photoSession.loginCode}" />
                                                    </b>
                                                    <c:choose>
                                                        <c:when test="${not empty $clientCodes}">
                                                            <t:i18n message="Logincode for $client&nbsp;" />
                                                            <b>${clientCodes}</b>
                                                        </c:when>
                                                    </c:choose>        -->
                                                </div>

                                                <!--footer-->
                                                <div class="modal-footer">                        
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>     
                                    
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
                                
        
                                
    </jsp:attribute>
</t:simple_layout>