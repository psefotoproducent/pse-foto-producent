<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Photo upload" >    
    <jsp:attribute name="body">   
        <script>
            $(document).ready(function () {

                // Method for dynamic portraitClient input
                var next = 1;
                $(".add-more").click(function (e) {
                    e.preventDefault();
                    var addto = "#client" + next;
                    var addRemove = "#client" + (next);
                    next = next + 1;
                    
                    var newIn = '<input autocomplete="off" class="form-control" id="client' + next + '" name="client' + next + '" type="text" placeholder="Insert client\'s name...">';
                    var newInput = $(newIn);
                    var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-xs btn-danger remove-me" >Remove this client</button></div><div id="client"></br>';
                    var removeButton = $(removeBtn);
                    $(addto).after(newInput);
                    $(addRemove).after(removeButton);
                    $("#field" + next).attr('data-source', $(addto).attr('data-source'));
                    $("#count").val(next);

                    $('.remove-me').click(function (e) {
                        e.preventDefault();
                        var fieldNum = this.id.charAt(this.id.length - 1);
                        var clientID = "#client" + fieldNum;
                        $(this).remove();
                        $(clientID).remove();
                    });
                });

                // Method to fill array with all (dynamically added) input #client (that are not empty)
                $( "form" ).on( "submit", function() {     
                    
                    var clients = [];
                    
                    $('[id^=client]').each(function() {
                        if($(this).val().length !== 0){
                            var inputValue = $(this).val();
                            clients.push(inputValue);
                        }                        
                    });
                    
                    $("#allClients").val(clients);
                    
                });                                    
            });
        </script>


        <h3>
            <i class="fa fa-angle-right"></i> 
            <c:choose>
                <c:when test="${photosession.photoSessionId > 0}">
                    <t:i18n message="Edit photosession" />
                </c:when>    
                <c:otherwise>
                    <t:i18n message="Add new photosession" />
                </c:otherwise>
            </c:choose>
        </h3>

        <div class="col-xs-10 admin-content-wrapper">
            <form method="post" class="form-horizontal">
                <!--<form class="form-horizontal">-->

                <input type="hidden" name="photographerId" id="photographerId" value ="${photographerId}" />

                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label"><t:i18n message="Description" /></label>
                    <div class="col-sm-10">
                        <input type="text" name="description" class="form-control" id="description" value="${photosession.description}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="organisation" class="col-sm-2 control-label"><t:i18n message="Organisation" /></label>
                    <div class="col-sm-10">
                        <input type="text" name="organisation" class="form-control" id="organisation" value="${photosession.organisation}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="organisationStreet" class="col-sm-2 control-label"><t:i18n message="Address" /></label>
                    <div class="col-sm-8">
                        <input type="text" name="organisationStreet" class="form-control" id="organisationStreet" value="${photosession.organisationStreet}" />
                    </div>

                    <div class="col-sm-2">
                        <input type="text" name="organisationHouseNumber" class="form-control" id="organisationHouseNumber" value="${photosession.organisationHouseNumber}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="organisationPostalCode" class="col-sm-2 control-label"><t:i18n message="Postal code and city" /></label>
                    <div class="col-sm-3">
                        <input type="text" name="organisationPostalCode" class="form-control" id="organisationPostalCode" value="${photosession.organisationPostalCode}" />
                    </div>

                    <div class="col-sm-7">
                        <input type="text" name="organisationCity" class="form-control" id="organisationCity" value="${photosession.organisationCity}" />
                    </div>
                </div>


                <div class="form-group col-sm-off">                    
                    <h3>
                        <i class="fa fa-angle-right"></i> Add portrait clients below
                    </h3>    
                    <label class="col-sm-6 control-label">For every portrait in this photosession, please add the Client's name</label>
                </div>

                <!--START DYNAMIC PORTRAIT CLIENT INPUTFIELDS-->
                <div class="form-group ">
                    <input type="hidden" name="count" value="1" />

                    <div class="form-group" id="profs"> 
                        <div class="col-sm-4" id="field">
                            <input autocomplete="off" class="form-control" id="client1" name="client1" type="text" data-items="8" placeholder="Insert client's name...">
                            </br>
                            <button id="b1" class="btn add-more" type="button">Add more clients..</button>
                        </div>
                    </div>
                </div>                    

                <!--hidden input for storing client array in post-->
                <input type="hidden" id="allClients" name="allClients"/>
                
                </br>

                <div class="form-group">
                    <div >
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <t:i18n message="Save" />
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </jsp:attribute>
</t:simple_layout>