<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<t:simple_layout title="Photo upload" >    
    <jsp:attribute name="body">     
        <script>
            $(function () {
                
                //Set owner of portraitPhoto in Photocontroller
                $(document).on('change','[id^=portraitClientSelector]',function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    
                    var id = $(this).val();
                    
                    var photoSessionId = ${photoSession.photoSessionId};
                    var split = id.split(":");
                    var photoId = split[0];
                    var client = split[1]; 
                                        
                    var clientData = {
                        "photoSessionId" : photoSessionId, 
                        "photoId" : photoId, 
                        "client" : client, 
                    };
                    
                    console.log(clientData);
                    
                    $.ajax({                        
                        type: "POST",
                        url: "/photosession/savePortraitClient",
                        data: clientData,
                        success: function (data) {
                            $("#price"+photoId).html("&euro; " + data);
                            $("#changesSaved").fadeIn("fast").delay(1200).fadeOut("slow");
                        }
                    }); 
                });
                
                
                //Fileupload field bootstrap
                $(document).on('change', ':file', function() {
                    var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });

                $(document).ready( function() {
                    $(':file').on('fileselect', function(event, numFiles, label) {

                        var input = $(this).parents('.input-group').find(':text'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;

                        if( input.length ) {
                            input.val(log);
                        } else {
                            if( log ) alert(log);
                        }
                        
                    });
                });  
                                
                $('.modalImgLink').on('click', function (event) {
                    var id = $(this).data('id');
                    
                    $('.modalImg').attr('src', '/photosession/getPicture/' + id);
                    $('.printImgGray').attr('href', '/print/grayscale/' + id);
                    $('.printImgSepia').attr('href', '/print/sepia/' + id);
                    
                    $('.deleteImg').on('click', function() {
                       $.post('/photosession/deletePicture/' + id, function( data ) {
                           $('.img' + id).remove();
                           $('#modalImage').modal('toggle');
                      }); 
                    });
                });
            });
        </script>

        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Upload photos" /> <b>${photoSession.description}</b></h3>
        
        <br>
            <c:choose>
                <c:when test="${empty portraitClients}">
                <%--do nothing--%>
                </c:when>
                <%--ELSE show names of portraitclient--%>
                <c:otherwise>
                    <h4> <t:i18n message="The following clients are linked to this photosession:&nbsp;" /><b>${portraitClients}</b></h4>
                </c:otherwise>
            </c:choose>
        <br>
        
        <h4><t:i18n message="Add photos below" /></h4>
        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                                 
            <div class="form-group">
                <label for="selectPhotos" class="col-sm-1 control-label"><t:i18n message="Select photos:" /></label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <label class="input-group-btn">
                            <span class="btn btn-default">
                                Browse&hellip; <input type="file" name="files[]" data-url="${photoSession.photoSessionId}" style="display: none;" multiple>
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
            </div>
                    
            <div class="form-group">
                <label for="price" class="col-sm-1 control-label"></label>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>&nbsp;<t:i18n message="Add photo(s) to session" />
                    </button>
                </div>
            </div>
        </form>
                    
        <br>
                    
        <!--End form, start atributes-->            
        
        <!--Photopreviews (on photosessionpage)-->
        
        <h3>
            <i class="fa fa-angle-right"></i> <t:i18n message="Processed files" />
            <label id="changesSaved" style="display: none; font-size: 14px;"><font color="green"><b>*Changes saved</b></font></label> 
        </h3>
                        
        <%--IF there are portraitphoto's, show selectbox and create form--%>
            
        <c:if test="${not empty portraitClients}">
            <form method="post" action="/photosession/${photoSession.photoSessionId}/savePortraitClients">
        </c:if>

        <c:forEach items="${photos}" var="photo" varStatus="loop">

            <c:if test="${(loop.index % 3) == 0}">
                <c:set var="count" value="0" scope="page" />

                <div class="row mt">
            </c:if>

            <c:set var="count" value="${count + 1}" scope="page"/>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc img${photo.photoId}">
                <div class="project-wrapper">
                    <div class="project">
                        <div class="photo-wrapper">
                            <div class="photo pull-left">
                                <a class="modalImgLink" href="#" data-id="${photo.photoId}" data-toggle="modal" data-target="#modalImage">
                                    <img class="img-responsive" src="getPicture/${photo.photoId}" alt="" />
                                </a>
                            </div>

                            <div class="col-md-5 col-sm-5">
                                <h4 style="color: white;" id="price${photo.photoId}">&euro; ${photo.price}</h4>
                                <p>
                                    <c:if test="${not empty portraitClients}">
                                        <t:i18n message="Owner:" />
                                        <select id="portraitClientSelector" class="form-control photoType">
                                            <option value="${photo.photoId}:GROUP">Group photo</option>
                                            
                                            <c:forEach items="${portraitClients}" var="client">   
                                                <c:set var="clientpart" value="${fn:split(client, '-')}" />
                                                <option value="${photo.photoId}:${client}" <c:if test="${photo.clientName == client}">selected</c:if>>${clientpart[0]}</option>
                                            </c:forEach>
                                        </select>
                                    </c:if>
                                </p>
                            </div>

                            <div class="overlay"></div>
                        </div>
                    </div>
                </div>
            </div>

            <c:if test="${count == 3}">
                </div>
            </c:if>

        </c:forEach>

        <c:if test="${not empty portraitClients}">
            </form>
        </c:if>
        
        <!--Enlarged photo's with print/delete options-->
        
        <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <t:i18n message="Photo preview" />
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="modalImg center-block" src="" alt="" />
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-success printImgGray" href="#" target="_blank">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print grayscale
                        </a>
                        
                        <a class="btn btn-success printImgSepia" href="#" target="_blank">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print sepia
                        </a>
                        
                        <a class="btn btn-danger deleteImg" href="#">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                        </a>
                        
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
            
    </jsp:attribute>
</t:simple_layout>