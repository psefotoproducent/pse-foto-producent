<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Search photo" >    
    <jsp:attribute name="body">
        <script>
            $(function () {
                $('.modalImgLink').on('click', function (event) {
                    var id = $(this).data('id');
                    
                    $('.modalImg').attr('src', '/photosession/getPicture/' + id);
                    $('.printImgGray').attr('href', '/print/grayscale/' + id);
                    $('.printImgSepia').attr('href', '/print/sepia/' + id);
                    
                    $('.deleteImg').on('click', function() {
                       $.post('/photosession/deletePicture/' + id, function( data ) {
                           $('.img' + id).remove();
                           $('#modalImage').modal('toggle');
                      }); 
                    });
                });
            });
        </script>
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Search results" /></h3>
        
        <c:choose>
            <c:when test="${empty results}">
                <t:i18n message="No results found..." />
            </c:when>    
            <c:otherwise>
                
                <c:forEach items="${results}" var="photo" varStatus="loop">

                    <c:if test="${(loop.index % 3) == 0}">
                        <c:set var="count" value="0" scope="page" />

                        <div class="row mt">
                    </c:if>

                    <c:set var="count" value="${count + 1}" scope="page"/>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc img${photo.photoId}">
                        <div class="project-wrapper">
                            <div class="project">
                                <div class="photo-wrapper">
                                    <div class="photo pull-left">
                                        <a class="modalImgLink" href="#" data-id="${photo.photoId}" data-toggle="modal" data-target="#modalImage">
                                            <img class="img-responsive" src="/photosession/getPicture/${photo.photoId}" alt="" />
                                        </a>
                                    </div>
                                    
                                    <div class="col-md-5 col-sm-5">
                                        <h4 style="color: white;">&euro; ${photo.price}</h4>
                                        <p>
                                            <c:if test="${photo.clientName != null}">
                                                <t:i18n message="Owner:" /> ${photo.clientName}<br/>
                                            </c:if>
                                            <t:i18n message="Session:" /> ${photo.photoSession.loginCode}
                                        </p>
                                    </div>
                                    
                                    <div class="overlay"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <c:if test="${count == 3}">
                        </div>
                    </c:if>

                </c:forEach>
                
            </c:otherwise>
        </c:choose>
        
        <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <t:i18n message="Photo preview" />
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="modalImg center-block" src="" alt="" />
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-success printImgGray" href="#" target="_blank">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print grayscale
                        </a>
                        
                        <a class="btn btn-success printImgSepia" href="#" target="_blank">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print sepia
                        </a>
                        
                        <a class="btn btn-danger deleteImg" href="#">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                        </a>
                        
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
            
    </jsp:attribute>
</t:simple_layout>