<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Management" >    
    <jsp:attribute name="body">

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="http://highcharts.github.io/export-csv/export-csv.js"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/custom-chart.js" />"></script>

        <script>
            $(function () {
                $('.setPaid').on('click', function () {
                    var btn = $(this);
                    var id = btn.data('id');

                    if (btn.hasClass('btn-info')) {
                        $.ajax({
                            url: '/order/management/setPaid/' + id,
                            method: 'POST',
                            beforeSend: function (xhr) {
                                
                            },
                            success: function (data, textStatus, jqXHR) {
                                $(btn).attr('class', 'btn  btn-success');
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $(btn).attr('class', 'btn  btn-danger');
                            },
                        });
                    }
                });
            });
        </script>

        <div class="row">
            <div class="row mtbox">
                <div class="col-md-3 col-sm-3 col-sm-offset-1 box0">
                    <div class="box1">
                        <span class="li_banknote"></span>
                        <h3>Invoiced &euro; ${paidSum + unpaidSum}</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 box0">
                    <div class="box1">
                        <span class="li_vallet"></span>
                        <h3>Paid &euro; ${paidSum}</h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 box0">
                    <div class="box1">
                        <span class="li_news"></span>
                        <h3>Unpaid &euro; ${unpaidSum}</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt">            
            <div class="col-md-6 col-sm-6 mb">
                <div class="grey-panel pn">
                    <div class="grey-header">
                        <h5><t:i18n message="10 LATEST INVOICES" /></h5>
                    </div>
                    <div class="col-md-12 col-sm-12 mb">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Firstname</th>
                                    <th>Insertion</th>
                                    <th>Lastname</th>
                                    <th>Created</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${latestInvoices}" var="invoice">
                                    <tr>
                                        <td>${invoice.invoiceId}</td>
                                        <td>${invoice.firstName}</td>
                                        <td>${invoice.insertion}</td>
                                        <td>${invoice.surName}</td>
                                        <td>${invoice.created}</td>
                                        <td>
                                            <a class="btn <c:choose><c:when test="${invoice.payed == true}">btn-success</c:when><c:otherwise>btn-info</c:otherwise></c:choose> setPaid" href="#" data-id="${invoice.invoiceId}">
                                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Paid
                                                    </a>
                                                </td>
                                            </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 mb">
                <div class="grey-panel pn">
                    <div class="grey-header">
                        <h5><t:i18n message="10 LONGEST STANDING INVOICES" /></h5>
                    </div>
                    <div class="col-md-12 col-sm-12 mb">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Firstname</th>
                                    <th>Insertion</th>
                                    <th>Lastname</th>
                                    <th>Created</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${longestStandingInvoices}" var="invoice">
                                    <tr>
                                        <td>${invoice.invoiceId}</td>
                                        <td>${invoice.firstName}</td>
                                        <td>${invoice.insertion}</td>
                                        <td>${invoice.surName}</td>
                                        <td>${invoice.created}</td>
                                        <td>
                                            <a class="btn <c:choose><c:when test="${invoice.payed == true}">btn-success</c:when><c:otherwise>btn-info</c:otherwise></c:choose> setPaid" href="#" data-id="${invoice.invoiceId}">
                                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Paid
                                                    </a>
                                                </td>
                                            </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt">            
            <div class="col-md-6 col-sm-6 mb">
                <div class="darkblue-panel pn">
                    <div class="darkblue-header">
                        <h5><t:i18n message="PAID VS UNPAID INVOICES" /></h5>
                    </div>
                    <div id="paid-unpaid-chart"></div>

                </div>
            </div>

            <div class="col-md-6 col-sm-6 mb">
                <div class="darkblue-panel pn">
                    <div class="darkblue-header">
                        <h5><t:i18n message="PAID VS UNPAID INVOICES" /> (&euro;)</h5>
                    </div>
                    <div id="paid-unpaid-price-chart"></div>

                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                getRemoteDataDrawChart('/management/charts/getPaidUnpaidChart', createNewLineChart('paid-unpaid-chart'));
                getRemoteDataDrawChart('/management/charts/getPaidUnpaidPriceChart', createNewLineChart('paid-unpaid-price-chart'));
            });
        </script>

    </jsp:attribute>
</t:simple_layout>