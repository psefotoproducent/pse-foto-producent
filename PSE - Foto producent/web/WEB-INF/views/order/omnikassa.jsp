<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:omnikassa title="Orderpage" >    
    <jsp:attribute name="body">       
       <div id="container">
		<header>
			<p id="headerlogo"><img src="<c:url value="/resources/images/logo_IDEAL.png"/>" alt="IDEAL logo" /></p>
			<h1>Welkom op de IDeal simulatie</h1>
		</header>
		
		<p class="k-intro">Druk op onderstaande knop om verder te gaan</p>
		
		<form id="issuer_sim" name="issuer_sim" method="post" action="/order/checkPayment/${invoice.invoiceId}" >	
			<ul>
				
					<li>
                                            <button type="submit" class="accepted" name="action" type="submit" value="OK">Transactie OK
[RC 00]</button>
					</li>
				
					<li><button class="refused" name="action" type="submit" value="notOK" >Transactie niet OK
[RC 05]</button>
					</li>
				
					<li><button class="cancelled" name="action" type="submit" value="notOK" >Transactie niet OK
[RC 17]</button>
					</li>
				
					<li><button class="pending" name="action" type="submit" value="notOK" >Transactie niet OK
[RC 60]</button>
					</li>
				
					<li><button class="timeout" name="action" type="submit" value="notOK" >Transactie niet OK
[RC 97]</button>
					</li>
				
			</ul>
		</form>
		
	</div>
    </jsp:attribute>
</t:omnikassa>