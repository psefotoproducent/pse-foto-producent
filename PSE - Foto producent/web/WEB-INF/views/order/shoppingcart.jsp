<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Shoppingcart" >    
    <jsp:attribute name="body">
        <script>
            
            function goBack() {
                window.history.back();
            }
                
            $(function () { 
                                
                // Remove shoppingcartline from shoppingcart                
                $('.deleteBtn').on('click', function(e) {
                    // get clicked div id (for confirmmsg)
                    var id = $(this).data('id');
//                    alert(id);
                    dataType: 'json',
                    e.preventDefault();
                    e.stopPropagation();
                    
                    var shoppingCartLineId = { 
                        "shoppingCartLineId" : id
                    };
//                    alert(JSON.stringify(shoppingCartLineId));
                
                    $.ajax({
                        type: "POST",
                        url: "/order/remove",                    
                        data: JSON.stringify(shoppingCartLineId),
                        contentType: 'application/json',
                        success: function(data) {
                            if(data !== "FAIL")
//                                alert('Shoppingcartline removed'),
                                $("#shoppingCartTable").load(location.href + " #shoppingCartTable"),
                                // reload mandatory to delete more than one shopingcartline?
                                location.reload();
                            else alert('Failed removing shoppingcartline');
                        }
                    });
                });
            });
        </script>
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Shoppingcart" /></h3>
            
        <!--SHOW CURRENT SHOPPINGCARTLINES-->
        <div class="row mt">
            <div class="col-md-12">
                <div class="content-panel">
                    <table id="shoppingCartTable" class="table table-striped table-advance table-hover">
                        <thead>
                            <tr>
                                <th><t:i18n message="Preview" /></th>
                                <th><t:i18n message="Product" /></th>
                                <th><t:i18n message="Color" /></th>
                                <th><t:i18n message="Amount" /></th>
                                <th><t:i18n message="Price" /></th>
                                <th><t:i18n message="Delete" /></th>
                            </tr>
                        </thead>

                        <c:forEach items="${shoppingCart}" var="shoppingCartLine">
                            <form id="shoppingCartLineForm${shoppingCartLine.shoppingCartLineId}" data-id="${shoppingCartLine.shoppingCartLineId}" method="post">
                                <form:input type="hidden" id="shoppingCartLineId${shoppingCartLine.shoppingCartLineId}"  path="shoppingCartLineId"></form:input>
                                <tr>                                
                                    <td class="col-lg-1 thumbnail">
                                        <img class="img-responsive img-rounded" src="overview/getPreviewThumb/${shoppingCartLine.photoId}/${shoppingCartLine.color}" alt="">
                                    </td>                                
                                    <td><c:out value="${shoppingCartLine.productDescription}" /></td>
                                    <td><c:out value="${shoppingCartLine.color}" /></td>
                                    <td><c:out value="${shoppingCartLine.amount}" /></td>
                                    <td><c:out value="${shoppingCartLine.price}" /></td>
                                    <td>
                                        <button class="btn btn-danger deleteBtn" 
                                                id="deleteBtn"
                                                data-id="${shoppingCartLine.shoppingCartLineId}" 
                                                type="submit" name="shoppingCartLineId" 
                                                value="<c:out value="${shoppingCartLine.shoppingCartLineId}" />">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            </form>
                        </c:forEach> 
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Total</b></td>
                            <td><b>&euro; ${totalPrice}</b></td>
                            <td></td>

                        </tr>
                    </table>
                </div>                         
            </div>
        </div>
         
        <div class="row mt">
            <div class="col-md-12">
                <form method="get" action="confirmOrder">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btnSubmit"><t:i18n message="Proceed to billing information"></t:i18n></button>
                        <button class="btn btn-warning" onclick="goBack()"><t:i18n message="Back to orderpage"></t:i18n></button>
                    </div>
                </form>
            </div>
        </div>
            
    </jsp:attribute>
</t:simple_layout>