<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Order confirmed" >    
    <jsp:attribute name="body">
        <script>
            function goBack() {
                window.history.back();
            }
            
            function printTable(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();

                document.body.innerHTML = originalContents;
            }
        </script>
        
            
            <!--CONFIRMATION MESSAGE AFTER FORM SUBMIT-->
            <h3 style="color: green;">${confirmMsg}</h3>
            <h3 style="color: red;">${failMsg}</h3>
                        
            <c:choose>
                <c:when test="${not empty confirmMsg}">
                    <div class="row">
                    <table id="confirmationTable" class="table table-striped">                        
                        <tr>
                            <th class="col-sm-2 control-label">
                                InvoiceID
                                <%--<t:i18n message="InvoiceID" />--%>
                            </th>
                            <td>${invoice.invoiceId}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Date
                                <%--<t:i18n message="Date" />--%>
                            </th>
                            <td>${invoice.created}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Name
                                <%--<t:i18n message="Name" />--%>
                            </th>
                            <td>${invoice.firstName}</td>
                        </tr>                        
                        <tr>
                            <th class="col-sm-2 control-label">
                                Surname
                                <%--<t:i18n message="Surname" />--%>
                            </th>
                            <td>${invoice.surName}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Street
                                <%--<t:i18n message="Street " />--%>
                            </th>
                            <td>${invoice.street}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Housenumber
                                <%--<t:i18n message="Housenumber" />--%>
                            </th>
                            <td>${invoice.houseNumber}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Postalcode
                                <%--<t:i18n message="Postalcode" />--%>
                            </th>
                            <td>${invoice.postalCode}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                City
                                <%--<t:i18n message="City" />--%>
                            </th>
                            <td>${invoice.city}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Email adress
                                <%--<t:i18n message="Email adress" />--%>
                            </th>
                            <td>${invoice.email}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Phonenumber
                                <%--<t:i18n message="Phonenumber" />--%>
                            </th>
                            <td>${invoice.phoneNumber}</td>
                        </tr>
                        <tr>
                            <th class="col-sm-2 control-label">
                                Total price of order
                                <%--<t:i18n message="Total price of order" />--%>
                            </th>
                            <td>&euro;${totalprice}</td>
                        </tr>
                    </table>
                </div>                         
                                    
            <div class="form-group">
                <a class="btn btn-info" href="/order/print/${invoice.invoiceId}" role="button">
                    <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                    Download invoice
                    <%--<t:i18n message="Download invoice" />--%>
                </a>
                <form action="/logout" style="display: inline;">
                    <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Click here to logout
                            <%--<t:i18n message="Click here to logout" />--%>
                    </button>
                </form>
            </div>
                </c:when>    
                <c:otherwise>
                    <form action="/logout" style="display: inline;">
                    <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Click here to logout
                            <%--<t:i18n message="Click here to logout" />--%>
                    </button>
                     </form>
                </c:otherwise>
            </c:choose>
            
    </jsp:attribute>
</t:simple_layout>
                    
                   