<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Shoppingcart" >    
    <jsp:attribute name="body">
        <script>
            
            function goBack() {
                window.history.back();
            }
            
        </script>
            <!--CLIENTINFORMATION FORM FOR CONFIRMATION-->
            <h3><i class="fa fa-angle-right"></i> <t:i18n message="Fill in the form below to confirm this order" /></h3>
            
            <h2>${errorMsg}</h2>
            
            <form id="orderForm" method="post" class="form-horizontal" action="confirmOrder">
                
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label"><t:i18n message="First name" /></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="firstname" id="firstName" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="insertion" class="col-sm-2 control-label"><t:i18n message="Insertion" /></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="insertion" id="insertion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label"><t:i18n message="Surname" /></label>
                        <div class="col-sm-8">
                            <input type="text"  class="form-control" name="surname" id="surName" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="insertion" class="col-sm-2 control-label"><t:i18n message="Street" /></label>
                        <div class="col-sm-8">
                            <input type="text"  class="form-control" name="street" id="street" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="surname" class="col-sm-2 control-label"><t:i18n message="Housenumber" /></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="housenumber" id="houseNumber" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="housenumber" class="col-sm-2 control-label"><t:i18n message="Postalcode" /></label>
                        <div class="col-sm-8">
                            <input type="number"  class="form-control" name="postalcode" id="postalCode" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="postalcode" class="col-sm-2 control-label"><t:i18n message="City" /></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"  name="city" id="city" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-sm-2 control-label"><t:i18n message="Email adress" /></label>
                        <div class="col-sm-8">
                            <input type="email"  class="form-control" name="email" id="email" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phonenumber" class="col-sm-2 control-label"><t:i18n message="Phonenumber" /></label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control"  name="phonenumber" id="phonenumber" required/>
                        </div>
                    </div>
                        
                    <!--dummy option for payment (not implemented yet in anything)-->
                    <div class="form-group">
                        <label for="payment" class="col-sm-2 control-label"><t:i18n message="Payment method" /></label>
                        <div class="col-sm-8">
                            <select class="form-control">
                                <option value="ABN Amro">ABN Amro</option>
                                <option value="Rabobank">Rabobank</option>
                                <option value="ING">ING</option>
                            </select>
                        </div>
                    </div>
                    <!--end dummy-->
                        
                    <div class="form-group">
                        <div class="col-sm-12 col-sm-offset-2">
                            <button type="submit" class="btn btn-success btnSubmit"><t:i18n message="Confirm order"></t:i18n></button>
                            <button class="btn btn-warning" onclick="goBack()"><t:i18n message="Back to shoppingcart"></t:i18n></button>
                        </div>
                    </div>
            </form>
    </jsp:attribute>
</t:simple_layout>