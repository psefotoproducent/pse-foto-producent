<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Productionlist" >    
    <jsp:attribute name="body">
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Productionlist" /></h3>

        <div class="row mt">
            <div class="col-md-12">
                <div class="content-panel">
                    <form method="post">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                                <tr>
                                    <th><t:i18n message="Surname" /></th>
                                    <th><t:i18n message="Shipping address" /></th>
                                    <th><t:i18n message="Product description" /></th>
                                    <th><t:i18n message="Amount" /></th>
                                    <th><t:i18n message="Status" /></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${invoiceList}" var="list">
                                    <tr>
                                        <td><c:out value="${list.invoice.surName}" /></td>
                                        <td><c:out value="${list.invoice.getAddress()}" /></td>
                                        <td><c:out value="${list.product.description}" /></td>
                                        <td><c:out value="${list.amount}" /></td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${list.done == 0}">
                                                    <a href="edit/<c:out value="${list.invoiceLineId}" />/<c:out value="${list.done}" />" class="btn btn-danger btn-sm" >
                                                        <t:i18n message="Not in production" />
                                                    </a>  
                                                </c:when>
                                                <c:when test="${list.done == 1}">
                                                    <a href="edit/<c:out value="${list.invoiceLineId}" />/<c:out value="${list.done}" />" class="btn btn-warning btn-sm" >
                                                        <t:i18n message="In Production" />
                                                    </a>  
                                                </c:when>
                                                <c:when test="${list.done == 2}">
                                                    <a href="edit/<c:out value="${list.invoiceLineId}" />/<c:out value="${list.done}" />" class="btn btn-info btn-sm" >
                                                        <t:i18n message="Awating shipment" />
                                                    </a>      
                                                </c:when>
                                                <c:when test="${list.done == 3}">
                                                    <a href="edit/<c:out value="${list.invoiceLineId}" />/<c:out value="${list.done}" />" class="btn btn-success btn-sm" >
                                                        <t:i18n message="Shipped" />
                                                    </a>     
                                                </c:when>
                                            </c:choose>
                                        </td>
                                        <td>
                                            <a href="decrease/<c:out value="${list.invoiceLineId}" />/<c:out value="${list.done}" />" class="btn btn-primary btn-sm" onclick="check()" title="<t:i18n message="Add photographer" />" >
                                                <t:i18n message="Revert production " />
                                            </a>  
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </jsp:attribute>

</t:simple_layout>