<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Orderpage" >    
    <jsp:attribute name="body">       
        <style type="text/css">
            img {
                max-width: 100%; /* This rule is very important, please do not ignore this! */
            }
            .image_container{
                width:500px;
                height: 400px;	
            }
        </style>

        <script type="text/javascript" defer>

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                    setTimeout(initCropper, 1000);
                }
            }

            var cropper;

            function initCropper(id) {
                var ID = id;
                console.log("Came here");

                var image = document.getElementById('img');
                cropper = new Cropper(image, {
                    scalable: true,
                    crop: function (e) {
                        console.log(e.detail.x);
                        console.log(e.detail.y);
                    }
                });
                document.getElementById('rotateOne').onclick = function () {
                    cropper.rotate(90);
                };
                document.getElementById('rotateTwo').onclick = function () {
                    cropper.rotate(180);
                };
                document.getElementById('zoomIn').onclick = function () {
                    cropper.zoom(0.1);
                };
                document.getElementById('zoomOut').onclick = function () {
                    cropper.zoom(-0.1);
                };
                document.getElementById('clear').onclick = function () {
                    cropper.reset();
                };
                document.getElementById('flipVer').onclick = function () {
                    cropper.scale(-1, 1);
                };
                document.getElementById('flipHor').onclick = function () {
                    cropper.scale(1, -1);
                };

                // On crop button clicked
                document.getElementById('crop_button').addEventListener('click', function () {
                    var imgurl = cropper.getCroppedCanvas().toDataURL();
                    var img = document.createElement("img");
                    img.src = imgurl;
                    document.getElementById("cropped_result").appendChild(img);


                    cropper.getCroppedCanvas().toBlob(function (blob) {
                        var formData = new FormData();
                        formData.append('croppedImage', blob);



                        // Use `jQuery.ajax`         method
                        $.ajax('/cropping/cropped/' + document.getElementById("checkID").value, {
                            method: "POST",
                            data: formData,
                            mimeType: "multipart/form-data",
                            processData: false,
                            contentType: false,
                            success: function () {
                                console.log('Upload success');
                                location.reload();
                            },
                            error: function () {
                                console.log('Upload error');
                            }
                        });
                    });

                });
            }

            function destroy()
            {
                cropper.destroy();

            }
        </script>

        <script>
            $(function () {
                // Shows preview in relation to selected color
                $('.colorPicker').on('change', function () {
                    var id = $(this).data('id');
                    $('.crop' + id).attr('src', '/order/overview/getPreview/' + id + '/' + $(this).val());
                });

                // Shows larger image of clicked product thumbnail
                $('.thumbnail').on('click', function () {
                    var id = $(this).data('id');
                    $('.modalImg').attr('src', '/order/overview/getProduct/' + id);
                });


                $('.crop${photo.photoId}').on('click', function () {
                    var id = $(this).data('id');
                    $('.modalCrop').attr('src', '/order/overview/getPreview/' + id + '/Normal');
                    document.getElementById("checkID").value = id;
                });

                $('.close').on('click', function () {
                    console.log(ID + "  TEST")
                    destroy();
                });

                // Submit ajax shoppingcartline to shoppingcartcontroller

                $('*').on('submit', function (e) {
                    // get clicked div id (for confirmmsg)
                    var id = $(this).data('id');
                    dataType: 'json',
                            e.preventDefault();
                    e.stopPropagation();

                    var photoId = $('#photoId' + id).val();
                    var price = $('#price' + id).val();

                    var productIdAndDescription = $('#productId' + id).val();
                    var split = productIdAndDescription.split(":");
                    var productId = split[0];
                    var productDescription = split[1];

                    var color = $('#color' + id).val();
                    var amount = $('#amount' + id).val();

                    var formdata = {
                        "photoId": photoId,
                        "price": price,
                        "productId": productId,
                        "productDescription": productDescription,
                        "color": color,
                        "amount": amount
                    };
//                    alert(JSON.stringify(formdata));

                    $.ajax({
                        type: "POST",
                        url: "/order/create",
                        data: JSON.stringify(formdata),
                        contentType: 'application/json',
                        success: function (data) {
                            if (data !== "FAIL")
                                // adjust shoppingcartSize on orderpage
                                $("#shoppingcartSize").html(data),
                                        // show fast confirmation on success
                                        $("#confirmMsg" + id).fadeIn("fast").delay(1200).fadeOut("slow");
                            else
                                alert('Failed adding shoppingcartline');
                        }
                    });
                });
            });
        </script>

        <!--start Shoppingcartoverview area-->
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Items in Shoppingcart -&nbsp; "/>
            <div id="shoppingcartSize" style="display:inline-block;"> ${shoppingCart.size}</div> 
        </h3>

        <a href="/order/shoppingcart" class="btn btn-primary" role="button"><t:i18n message="View shoppingcart"></t:i18n></a>
            <!--end Shoppingcartoverview area-->

            <hr class="hrstyle">

            <h4><t:i18n message="Available products" /></h4>

        <!-- START PRODUCT LOOP (ONLY PICTURES) -->
        <div class="row">
            <c:forEach items="${products}" var="product">
                <c:if test="${product.thumbnail != null}">
                    <div class="col-lg-1 col-md-4 col-xs-6 thumb">
                        <a class="thumbnail" href="#" data-id="${product.productId}" data-toggle="modal" data-target="#modalImage">
                            <img class="img-responsive" src="/order/overview/getProduct/${product.productId}" alt="">
                        </a>
                    </div>
                </c:if>
            </c:forEach>
        </div>

        <hr class="hrstyle">

        <h4><t:i18n message="Your photo's" /></h4>

        <!-- START PHOTOLOOP INCLUDING ORDEROPTIONS -->
        <c:forEach items="${photos}" var="photo">
            <div class="row mt">
                <div class="col-md-6 col-sm-6 mb">
                    <div class="grey-panel pn-simple">
                        <div class="grey-header">
                            <h5><t:i18n message="Photo" /> ${photo.photoId}</h5>
                        </div>

                        <form:form method="post" id="photoForm" data-id="${photo.photoId}" class="form-horizontal" modelAttribute="shoppingcart">  
                            <form:input type="hidden" id="photoId${photo.photoId}" value="${photo.photoId}" path="photoId"></form:input>

                                <!-- Price area -->
                                <div class="form-group">
                                    <label class="col-sm-1 col-sm-offset-1 control-label">Price</label>
                                    <div class="col-sm-2 inputGroupContainer">
                                        <div class="input-group">
                                            <!-- Show portrait/group price? -->
                                            <!-- value = price -->                                    
                                        <form:input type="text" class="form-control" id="price${photo.photoId}" value="${photo.price}" path="price" readonly="true" ></form:input>
                                            <span class="input-group-addon">&euro;</span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Product combobox -->
                                <div class="form-group">
                                    <label class="col-sm-1 col-sm-offset-1 control-label">Product</label>
                                    <div class="col-sm-4 selectContainer">
                                        <!-- value = productId -->
                                    <form:select class="form-control" id="productId${photo.photoId}" path="productId">
                                        <!--<option value="">Choose a product</option>-->
                                        <c:forEach items="${products}" var="product">               
                                            <!--use both productId and description as value (description is for shoppingcart overview)-->
                                            <form:option value="${product.productId}:${product.description}">${product.description} - &euro; <b>${product.price}</b></form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </div>

                            <!-- Color combobox -->
                            <div class="form-group">
                                <label class="col-sm-1 col-sm-offset-1 control-label">Color</label>
                                <div class="col-sm-4 selectContainer">
                                    <!-- value = color -->
                                    <form:select class="form-control colorPicker" id="color${photo.photoId}" data-id="${photo.photoId}" path="color">
                                        <!--<option value="">Choose a color</option>-->
                                        <c:forEach items="${colors}" var="color">
                                            <form:option value="${color}">${color}</form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </div>

                            <!-- amount -->
                            <div class="form-group">
                                <label class="col-sm-1 col-sm-offset-1 control-label">Amount</label>
                                <div class="col-sm-2 selectContainer">
                                    <!-- value = amount -->
                                    <form:select class="form-control" id="amount${photo.photoId}" path="amount">
                                        <!--<option>Choose an amount</option>-->
                                        <form:option value="1">1</form:option>
                                        <form:option value="2">2</form:option>
                                        <form:option value="3">3</form:option>
                                        <form:option value="4">4</form:option>
                                        <form:option value="5">5</form:option>
                                    </form:select>
                                </div>
                            </div>

                            <!-- Place in shoppingcart button -->
                            <div class="form-group btn${photo.photoId}">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <button type="submit" class="btn btn-success btnSubmit"><t:i18n message="Place in shoppingcart"></t:i18n></button>
                                    </div>
                                    <br>
                                    <div class="col-sm-5">
                                        <div id="confirmMsg${photo.photoId}" style="display: none; font-size: 14px;">
                                        <label class="col-sm-12 col-sm-offset-1 control-label"><font color="Green"><b>Added to shoppingcart</b></font></label>
                                    </div> 
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>

                <!--Photopreview container-->
                <div class="col-sm-4 productbox">
                    <div class="white-panel pn-simple pn-simple-hover">
                        <!--<img class="photoPreview${photo.photoId} img-responsive" src="/order/overview/getPreview/${photo.photoId}/Normal" />-->
                        <a class="crop" href="#" data-id="${photo.photoId}" data-toggle="modal" data-target="#modalCrop">
                            <img class="crop${photo.photoId} img-responsive" src="/order/overview/getPreview/${photo.photoId}/Normal" alt="">
                        </a>
                    </div>

                </div>
            </div>
        </c:forEach>

        <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <t:i18n message="Product preview" />
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="modalImg center-block" src="" alt="" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalCrop" tabindex="-1" role="dialog" aria-labelledby="modalImageLable" aria-hidden="true">
            <div class="modal-dialog modal-xs" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <t:i18n message="Crop preview" />

                    </div>
                    <div class="modal-body">
                        <div class="row mt">
                            <div class="image-container">
                                <img id="img"  class="modalCrop center-block" src="" alt="your image"/>
                            </div>
                        </div>

                        <div class="row mt">
                            <div class="col-sm-8">
                                <button onclick="initCropper()" class="btn btn-primary"><t:i18n message="Start cropping" /></button>
                            </div>
                        </div>
                        <div class="row mt">
                            <div class="col-sm-8">  
                                <button type="button" class="btn btn-warning" id="rotateOne"><t:i18n message="Rotate 90�" /></button>
                                <button type="button" class="btn btn-warning" id="rotateTwo"><t:i18n message="Rotate 180�" /></button>

                                <button type="button" class="btn btn-warning" id="zoomIn"><span class="fa fa-search-plus"></span></button>
                                <button type="button" class="btn btn-warning" id="zoomOut"><span class="fa fa-search-minus"></span></button>

                                <button type="button" class="btn btn-warning" id="flipVer"><span class="fa fa-arrows-h"></span></button>

                                <button type="button" class="btn btn-warning" id="flipHor"><span class="fa fa-arrows-v"></span></button>

                            </div>
                        </div>

                        <input type="hidden" name="checkID" id="checkID" value="">

                        <div class="row mt">
                            <div class="col-sm-8">
                                <div id="cropped_result"></div>
                            </div>
                        </div>

                        <div class="row mt">
                            <div class="col-sm-8">
                                <button id="crop_button" class="btn btn-success">Opslaan</button>
                                <button type="button" class="btn btn-danger" id="clear"><t:i18n message="Reset" /></button>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button onclick="destroy()" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </jsp:attribute>
</t:simple_layout>