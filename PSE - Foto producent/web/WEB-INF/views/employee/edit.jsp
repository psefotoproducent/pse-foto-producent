<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Add a Employee" >    
    <jsp:attribute name="body">
     
        <h3>
            <i class="fa fa-angle-right"></i> 
            <c:choose>
                <c:when test="${employee.employeeId > 0}">
                    <t:i18n message="Edit employee" />
                </c:when>    
                <c:otherwise>
                    <t:i18n message="Add new employee" />
                </c:otherwise>
            </c:choose>
        </h3>
        
        <div class="col-xs-10 col-xs-push-1 admin-content-wrapper">
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <label for="username" class="col-sm-2 control-label"><t:i18n message="Username" /></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="username" id="username" value="${employee.user.username}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label"><t:i18n message="Password" /></label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password" id="password" value="${employee.user.password}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstname" class="col-sm-2 control-label"><t:i18n message="Firstname" /></label>
                    <div class="col-sm-8">
                        <input type="text"  class="form-control" name="firstname" id="firstName" value="${employee.user.firstName}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="insertion" class="col-sm-2 control-label"><t:i18n message="Insertion" /></label>
                    <div class="col-sm-8">
                        <input type="text"  class="form-control" name="insertion" id="insertion" value="${employee.user.insertion}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="surname" class="col-sm-2 control-label"><t:i18n message="Surname" /></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="surname" id="surName" value="${employee.user.surName}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="housenumber" class="col-sm-2 control-label"><t:i18n message="Housenumber" /></label>
                    <div class="col-sm-8">
                        <input type="number"  class="form-control" name="housenumber" id="housenumber" value="${employee.user.houseNumber}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="postalcode" class="col-sm-2 control-label"><t:i18n message="Postalcode" /></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control"  name="postalcode" id="postalcode" value="${employee.user.postalCode}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label"><t:i18n message="City" /></label>
                    <div class="col-sm-8">
                        <input type="text"  class="form-control" name="city" id="city" value="${employee.user.city}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="phonenumber" class="col-sm-2 control-label"><t:i18n message="Phonenumber" /></label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control"  name="phonenumber" id="phonenumber" value="${employee.user.phoneNumber}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="col-xs-3 btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <t:i18n message="Save" />
                        </button>
                    </div>
                </div>
            </form>
        </div>

    </jsp:attribute>
</t:simple_layout>