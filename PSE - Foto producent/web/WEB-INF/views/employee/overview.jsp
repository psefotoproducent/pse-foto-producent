<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Employees" >    
    <jsp:attribute name="body">
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Employees" /></h3>
        
        <div class="row mt">
            <div class="col-md-12">
                <div class="content-panel">
                    <form method="post">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                            <tr>
                                <th><t:i18n message="Username" /></th>
                                <th><t:i18n message="Firstname" /></th>
                                <th><t:i18n message="Insertion" /></th>
                                <th><t:i18n message="Lastname" /></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${employeeList}" var="employee">
                                    <tr>
                                        <td><c:out value="${employee.user.username}" /></td>
                                        <td><c:out value="${employee.user.firstName}" /></td>
                                        <td><c:out value="${employee.user.insertion}" /></td>
                                        <td><c:out value="${employee.user.surName}" /></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div><!-- /content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- /row -->
     
    </jsp:attribute>

</t:simple_layout>