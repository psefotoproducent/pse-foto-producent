<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Employees" >    
    <jsp:attribute name="body">
        
        <h3><i class="fa fa-angle-right"></i> <t:i18n message="Employees" /></h3>

        <a class="add-btn btn btn-primary" href="/management/employee/add" role="button">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <t:i18n message="Add employee" />
        </a>
        
        <div class="row mt">
            <div class="col-md-12">
                <div class="content-panel">
                    <form method="post">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                            <tr>
                                <th><t:i18n message="Username" /></th>
                                <th><t:i18n message="Firstname" /></th>
                                <th><t:i18n message="Insertion" /></th>
                                <th><t:i18n message="Lastname" /></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${employeeList}" var="employee">
                                    <tr>
                                        <td><a href="/management/employee/edit/<c:out value="${employee.employeeId}" />"><c:out value="${employee.user.username}" /></a></td>
                                        <td><a href="/management/employee/edit/<c:out value="${employee.employeeId}" />"><c:out value="${employee.user.firstName}" /></a></td>
                                        <td><a href="/management/employee/edit/<c:out value="${employee.employeeId}" />"><c:out value="${employee.user.insertion}" /></a></td>
                                        <td><a href="/management/employee/edit/<c:out value="${employee.employeeId}" />"><c:out value="${employee.user.surName}" /></a></td>
                                        
                                        <td>
                                            <a class="btn btn-primary" href="/management/employee/edit/<c:out value="${employee.employeeId}" />" role="button">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                            </a>

                                            <button class="btn btn-danger" type="submit" name="employeeId" value="<c:out value="${employee.employeeId}" />">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div><!-- /content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- /row -->
     
    </jsp:attribute>

</t:simple_layout>