<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Internationalization.controller.I18n"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:simple_layout title="Medewerkers" >    
    <jsp:attribute name="body">
        <h1><t:i18n message="Employee" /></h1>

        <form method="post">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><t:i18n message="Username" /></th>
                        <th><t:i18n message="Firstname" /></th>
                        <th><t:i18n message="Insertion" /></th>
                        <th><t:i18n message="Lastname" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td><c:out value="${employee.user.username}" /></td>
                        <td><c:out value="${employee.user.firstName}" /></td>
                        <td><c:out value="${employee.user.insertion}" /></td>
                        <td><c:out value="${employee.user.surName}" /></td>
                        <td>
                            <a class="btn btn-primary btn-xs" title="<t:i18n message="Edit" />"
                               href="/employee/single/edit/<c:out value="${employee.employeeId}" />">
                                <i class="fa fa-edit" ></i>
                                <t:i18n message="Edit" />
                            </a>
                        </td>
                        <td>
                            <button class="btn btn-danger btn-xs " type="submit" 
                                    name="employeeId" value="<c:out value="${employee.employeeId}" />">
                                <i class="fa fa-trash-o" ></i>
                                <t:i18n message="Delete" />
                            </button>
                        </td>
                    </tr>

                </tbody>
            </table>
            
        </form>
    </jsp:attribute>

</t:simple_layout>