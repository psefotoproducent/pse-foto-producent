<%@ page import="i18n.I18n"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <p>Hello! This is the test page for a Spring Web MVC project.</p>
        <p><i>To display a different welcome page for this project, modify</i>
            <tt>test.jsp</tt>.
        </p>
        <p>
            <% out.print(I18n.l("Welcome")); %>
            <% I18n.setLanguage("nl"); %>
            <% out.print(I18n.l("Welcome")); %>
        </p>
    </body>
</html>
